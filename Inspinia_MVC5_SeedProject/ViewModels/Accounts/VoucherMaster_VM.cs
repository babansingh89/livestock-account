﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.ViewModels.Accounts
{
    public class VoucherMaster_VM
    {
        public string GL_ID { get; set; }
        public string GL_NAME { get; set; }
        public string GL_TYPE { get; set; }
        public string OLD_SL_ID { get; set; }

        public int? VoucherTypeID { get; set; }
        public string VoucherDescription { get; set; }

        public string InstrumentTypeID { get; set; }
        public string Instrument { get; set; }

        public int? VoucherNumber { get; set; }
        public string RefVoucherSlNo { get; set; }

        public string SubLedgerTypeID { get; set; }
        public string SubLedgerType { get; set; }

        public string Code { get; set; }
        public string Desccription { get; set; }
    }
}