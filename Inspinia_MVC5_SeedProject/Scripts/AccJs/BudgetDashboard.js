﻿

$(document).ready(function () {

    Populate();

    $("#txtDashReceiptType").autocomplete({

        source: function (request, response) {

            var budgetTypeID = $('#ddlDashBudgetType').val();
            var catID = $('#ddlDashCategory').val();
            var subcatID = $('#ddlDashSubCategory').val();
            var SectorID = $('#ddlSector').val();
            if (SectorID == '') { alert('Please Select Sector.'); $('#ddlSector').focus(); return false; }

            var E = "{TrasactionType: '" + "Particulars" + "', BudgetTypeID: '" + budgetTypeID + "', CategoryID: '" + catID + "', SubCategoryID: '" + subcatID + "', SectorID: '" + $('#ddlSector').val() + "',  Desc: '" + $.trim($('#txtDashReceiptType').val()) + "'}";// alert(E);

            $.ajax({
                type: "POST",
                url: '/Accounts_Form/BudgetMaster/Get_Particulars',
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {

                            AutoComplete.push({
                                label: item.BudgetDescription,
                                BudgetID: item.BudgetID
                            });
                        });

                        response(AutoComplete);
                        //PopulateGrid(serverResponse.Data);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#txtDashReceiptType').val(i.item.label);
            Populate();
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
        });
    $('#txtDashReceiptType').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtDashReceiptType").val('');
            Populate();
        }
        if (iKeyCode == 46) {
            $("#txtDashReceiptType").val('');
            Populate();
        }
    });

});
function Get_DashCondition() {
    $('#ddlDashCategory').val('');
    var budgetTypeID = $('#ddlDashBudgetType').val(); 
    if (budgetTypeID == 'R') {
        $('.clsDashCatSubCat').css('display', '');
        $('#ddlDashCategory').val('');
        $('#ddlDashSubCategory').val('');
        $('#txtDashReceiptType').val('');
    }
    else {
        $('.clsDashCatSubCat').css('display', 'none');
       // $('#ddlDashCategory').val('E');
        $('#ddlDashSubCategory').val('');
        $('#txtDashReceiptType').val('');
    }
    Populate();
}
function Populate() {

    var budgetTypeID = $('#ddlDashBudgetType').val();
    var catID = $('#ddlDashCategory').val();
    var subcatID = $('#ddlDashSubCategory').val();
    var SectorID = $('#ddlSector').val();
    if (SectorID == '') { alert('Please Select Sector.'); $('#ddlSector').focus(); return false; }

    var E = "{TrasactionType: '" + "Dashboard" + "', BudgetTypeID: '" + budgetTypeID + "', CategoryID: '" + catID + "', SubCategoryID: '" + subcatID + "', SectorID: '" + $('#ddlSector').val() + "',  Desc: '" + $.trim($('#txtDashReceiptType').val()) + "'}";// alert(E);

    if (budgetTypeID != "") {
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BudgetMaster/Get_Dashboard',
            data: E,
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                var t = data.Data;
                $('#page-wrapper').toggleClass('sk-loading');
                BindGrid(t);
                $('#page-wrapper').removeClass('sk-loading');
            }
        });
    }
}
function BindGrid(detail)
{
  
    var table = $("#myDashboard");
    table.find('thead').empty();
    table.find('tbody').empty();

    if (detail.length > 0)
    {
        var Hhtml = ""; var Ihtml = "", ITotal = 0, ETotal = 0;

        Hhtml += "<tr><td></td>" +
                 "<td colspan='7' style='text-align:center;'>CORPORATE</td>" +
                 "</tr > ";
        Hhtml += "<tr><td colspan='1'>Particulars</td>" +
            "<td style='text-align:center'>Income/Expense</td>" +
            "<td style='text-align:center'>Budget Amt</td>" +
            "<td style='text-align:center'>FC Amt</td>" +
            "<td style='text-align:center'>Balance after FC</td>" +
            "<td style='text-align:center'>Actual Expense</td>" +
            "<td style='text-align:center'>Balance after actual Expense</td>" +
            "<td style='text-align:center'></td>" +
            "</tr > ";

        table.find('thead').append(Hhtml);

        for (var i = 0; i < detail.length; i++) {

            var bAmt = parseFloat(detail[i].BudgetAmount);
            var fcAmt = parseFloat(detail[i].ConcurranceAmount);
            var baeAmt = parseFloat(detail[i].AfterActualExp);
            var a = (fcAmt * 100) / bAmt; //alert(a);
            var b = (baeAmt * 100) / bAmt; //alert(b);
            var color1 = "", color2 = "", color3 = "", color4 = "", color5 = "";
            var color6 = "", color7 = "", color8 = "", color9 = "", color10 = "";

            if (parseFloat(fcAmt) > 0) {

                if (a <= 20 && a <= 20)
                { color1 = "progress-bar-success"; color2 = ""; color3 = ""; color4 = ""; color5 = ""; }

                if (a > 20 && a <= 40)
                { color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = ""; color4 = ""; color5 = ""; }

                if (a > 40 && a <= 60) {
                    color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = ""; color5 = "";
                }
                if (a > 60 && a <= 80) {
                    color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-warning"; color5 = "progress-bar-warning";
                }
                if (a > 80 && a < 100) {
                    color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-success"; color5 = "progress-bar-danger";
                }
                if (a == 100) {
                    color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-success"; color5 = "progress-bar-success";
                }
                if (a > 100) {
                    color1 = "progress-bar-danger"; color2 = "progress-bar-danger"; color3 = "progress-bar-danger"; color4 = "progress-bar-danger"; color5 = "progress-bar-danger";
                }
            }
            else
            {
                color1 = ""; color2 = ""; color3 = ""; color4 = ""; color5 = "";
            }
            //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

            if (parseFloat(baeAmt) > 0) {

                if (b > 20 && b <= 40)
                { color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = ""; color9 = ""; color10 = ""; }

                if (b > 40 && b <= 60) {
                    color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = ""; color10 = "";
                }
                if (b > 60 && b <= 80) {
                    color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-warning"; color10 = "progress-bar-warning";
                }
                if (b > 80 && b < 100) {
                    color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-success"; color10 = "progress-bar-danger";
                }
                if (b == 100) {
                    color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-success"; color10 = "progress-bar-success";
                }
                if (b > 100) {
                    color6 = "progress-bar-danger"; color7 = "progress-bar-danger"; color8 = "progress-bar-danger"; color9 = "progress-bar-danger"; color10 = "progress-bar-danger";
                }
            }
            else {
                color6 = ""; color7 = ""; color8 = ""; color9 = ""; color10 = "";
            }


            var BudgetID = detail[i].BudgetID; var row_color = "";
            if (BudgetID == 0)
            {
                row_color = '#C8CDCC';
            }
          

            Ihtml += "<tr class='myData_I' style='background-color:" + row_color + "'>"
                    + "<td style='text-align:right; display:none;' class='BudgetID' >" + detail[i].BudgetID + "</td>"
                    + "<td style='text-align:left; '>" + detail[i].BudgetDescription + "</td>"
                    + "<td style='text-align:center; '>" + detail[i].IE + "</td>"
                    + "<td style='text-align:right;' class='clsBudgetAmt' >" + (parseFloat(detail[i].BudgetAmount)).toFixed(2) + "</td>"
                    + "<td style='text-align:right;' >" + (parseFloat(detail[i].ConcurranceAmount)).toFixed(2) + "</td>"
                    + "<td style='text-align:right;' class='clsBalAmt' >" + (parseFloat(detail[i].BalanceAmount)).toFixed(2) + "</td>"
                    + "<td style='text-align:right;' class='clsBalAmt' >" + (parseFloat(detail[i].ActualExpense)).toFixed(2) + "</td>"
                    + "<td style='text-align:right;' class='clsBalAmt' >" + (parseFloat(detail[i].AfterActualExp)).toFixed(2) + "</td>"

                    + "<td style='width:12%'><div ><div class='progress' style='height:10px !important; margin-bottom: 2px; float:left; width:80%'>" +
                    "<div class='progress-bar " + color1 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color2 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color3 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color4 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color5 + "' role='progressbar' style='width:20%;'></div></div > <div style='float:right; margin-top:-2px; font-size:10px; width:20px'>FC</div></div>" +

                    "<div><div ><div class='progress' style='height:10px !important; margin-bottom: 2px; float:left; width:80%'>" +
                    "<div class='progress-bar " + color6 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color7 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color8 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color9 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color10 + "' role='progressbar' style='width:20%;'></div></div > <div style='float:right; margin-top:-2px; font-size:10px; width:20px'>Exp</div></div>" +
                    "</td > "

                Ihtml + "</tr>";
            

        }
       table.find('tbody').append(Ihtml);
    }
}