﻿var master = []; var k = 0;

$(document).ready(function () {

    Set_DefaultDate();

    $("#txtReceiptType").autocomplete({

        source: function (request, response) {

            var budgetTypeID = $('#ddlBudgetType').val();
            var catID = $('#ddlCategory').val();
            var subcatID = $('#ddlSubCategory').val();
            var SectorID = $('#ddlSector').val();
            if (SectorID == '') { alert('Please Select Sector.'); $('#ddlSector').focus(); return false; }

            var E = "{TrasactionType: '" + "Particulars" + "', BudgetTypeID: '" + budgetTypeID + "', CategoryID: '" + catID + "', SubCategoryID: '" + subcatID + "', SectorID: '" + $('#ddlSector').val() + "',  Desc: '" + $.trim($('#txtReceiptType').val()) + "'}";// alert(E);
           
            $.ajax({
                type: "POST",
                url: '/Accounts_Form/BudgetMaster/Get_Particulars',
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (serverResponse) {
                    var AutoComplete = []; 
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                           
                            AutoComplete.push({
                                label: item.BudgetDescription, 
                                BudgetID: item.BudgetID
                            });
                        });

                        response(AutoComplete);
                        PopulateGrid(serverResponse.Data);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#txtReceiptType').val(i.item.label);
             //$('#txtReceiptType').val(''); $('#myTable tbody tr.myData').remove();
            Get_Particulars();
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });

    //ALLOWING WITH DECIMAL VALUE IN ALL AMOUNT FIELD
    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
});


function Set_DefaultDate() {
    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtEffectiveDate').val(output);

    $('#txtEffectiveDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        todayHighlight: true
    });
}
function Get_Condition()
{
    var budgetTypeID = $('#ddlBudgetType').val();
    if (budgetTypeID == 'R') {
        $('.clsCatSubCat').css('display', '');
        $('#ddlCategory').val('');
        $('#ddlSubCategory').val('');
        $('#txtReceiptType').val('');
    }
    else {
        $('.clsCatSubCat').css('display', 'none');
        $('#ddlCategory').val('E');
        $('#ddlSubCategory').val('');
        $('#txtReceiptType').val('');
    }
        Get_Particulars();
}
function Get_Particulars()
{
    var budgetTypeID = $('#ddlBudgetType').val();
    var catID = $('#ddlCategory').val();
    var subcatID = $('#ddlSubCategory').val();
    var SectorID = $('#ddlSector').val();
    if (SectorID == '') { alert('Please Select Sector.'); $('#ddlSector').focus(); return false;}

    if (budgetTypeID != "")
    {
        var E = "{TrasactionType: '" + "Particulars" + "', BudgetTypeID: '" + budgetTypeID + "', CategoryID: '" + catID + "', SubCategoryID: '" + subcatID + "', SectorID: '" + $('#ddlSector').val() + "',  Desc: '" + $.trim($('#txtReceiptType').val()) + "'}";// alert(E);
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BudgetMaster/Get_Particulars',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data; 
                $('#page-wrapper').toggleClass('sk-loading');
                PopulateGrid(t);
                $('#page-wrapper').removeClass('sk-loading');
            }
        });
    }
}
function PopulateGrid(detail)
{
    var html = "";

    var table = $('#myTable');
    $('#myTable tbody tr.myData').remove();

    if (detail.length > 0)
    {
        for (var i = 0; i < detail.length; i++) {
            html += "<tr class='myData'>"
                + "<td style='text-align:right; display:none;' class='BudgetID' >" + detail[i].BudgetID + "</td>"
                + "<td style='text-align:left; '>" + detail[i].BudgetDescription + "</td>"
                + "<td style='text-align:center'><input type='text' class='clsBudgetCode' style='width:200px' value='" + (detail[i].BudgetCode == null ? "" : detail[i].BudgetCode) +"' ></td>"
                + "<td style='text-align:right;' class='clsBudgetAmt' >" + (parseFloat(detail[i].BudgetAmount)).toFixed(2) + "</td>"
                + "<td style='text-align:center;' class='clsTaxNetAmount' ><img src='/Content/Images/add.png' title='Add' style='height:20px; width: 20px; cursor:pointer;' onclick='PopUP_BudgetDetail(this)' /></td>"
            html + "</tr>";
        }
       
        table.append(html);
    }
}


//POPUP DATA CALCULALTION
function PopUP_BudgetDetail(ID) {

    var BudgetID = $(ID).closest('tr').find('.BudgetID').text(); 
    var BudgetCode = $(ID).closest('tr').find('.clsBudgetCode').val(); 
    var BudgetAmount = $(ID).closest('tr').find('.clsBudgetAmt').text(); 
    $('#hdnBudgetID').val(BudgetID);
    $('#hdnBudgetCode').val(BudgetCode);
    $('#hdnBudgetAmount').val(BudgetAmount);

    Bind_BudgetDetail(BudgetID);
    $("#dialog-Budgetdetail").modal("show");
}
function Show_BudgetDetail(data)
{
    var html = ""; 

    $("#tbl_BudgetDetail tbody tr.myData").remove();

    if (data.length > 0) {
     
        var $this = $('#tbl_BudgetDetail .test_0');
        $parentTR = $this.closest('tr');

        for (var i = 0; i < data.length; i++) {
            html += "<tr class='myData' >"
                + "<td  class='bugtAmt' style='text-align:right;'>" + data[i].BudgetAmount + "</td>"
                + "<td class='effDate' style='text-align:center;'>" + data[i].EffectiveDate + "</td>"
                //+ "<td class='fundType' style='display:none;'>" + data[i].FundTypeID + "</td>"
                //+ "<td class='fundTypeDesc' style='text-align:center;'>" + data[i].FundDescription + "</td>"
                + "<td class='remarks'>" + (data[i].Remarks == null ? "" : data[i].Remarks) + "</td>"
                + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='DeleteBudgetDetail(this);' /></td>"
            html + "</tr>";
        }
        $parentTR.after(html);
    }
}
function DeleteBudgetDetail(ID)
{
    $(ID).closest('tr').remove();

}
function Bind_BudgetDetail(BudgetID) {
    if (BudgetID != "") {
        var E = "{TrasactionType: '" + "Select" + "', BudgetID: '" + BudgetID + "', SectorID: '" + $('#ddlSector').val() + "'}";// alert(E);
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BudgetMaster/Get_BudgetDetails',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data;
                $('#page-wrapper').toggleClass('sk-loading');
                Show_BudgetDetail(t);
                $('#page-wrapper').removeClass('sk-loading');
            }
        });
    }

}
function AddBudgetDetail()
{
    var bugtAmt = $('#txtBudgetAmt').val();
    var effDate = $('#txtEffectiveDate').val();
    var fundType = $('#ddlFundType').val();
    var fundTypeDesc = $('#ddlFundType').find('option:selected').text();
    var remarks = $('#txtRemarks').val();

    if (bugtAmt == "" && bugtAmt <= 0) {
        $("#txtBudgetAmt").attr("placeholder", "Enter Amount"); $("#txtBudgetAmt").addClass("Red"); $('#txtBudgetAmt').focus(); return false;
    }
    if (effDate == "") {
        $("#txtEffectiveDate").attr("placeholder", "Enter Date"); $("#txtEffectiveDate").addClass("Red"); $('#txtEffectiveDate').focus(); return false;
    }
    if (fundType == "") {
        $("#ddlFundType").attr("placeholder", "Select"); $("#ddlFundType").addClass("Red"); $('#ddlFundType').focus(); return false;
    }

    var html = "";

    var $this = $('#tbl_BudgetDetail .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"
        + "<td  class='bugtAmt' style='text-align:right;'>" + bugtAmt + "</td>"
        + "<td class='effDate' style='text-align:center;'>" + effDate + "</td>"
        //+ "<td class='fundType' style='display:none;'>" + fundType + "</td>"
        //+ "<td class='fundTypeDesc' style='text-align:center;'>" + fundTypeDesc + "</td>"
        + "<td class='remarks'>" + remarks + "</td>"
        + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='DeleteBudgetDetail(this);' /></td>"
    html + "</tr>";

    $parentTR.after(html);

    $("#txtBudgetAmt").val(''); Set_DefaultDate(); $("#ddlFundType").val('1'); $("#txtRemarks").val(''); $("#txtBudgetAmt").focus();

}
function Save() {

    var grdLen = $('#tbl_BudgetDetail tbody tr.myData').length; var ArrList = []; var master = {}; var TotalAmount = 0;
    if (grdLen > 0) {
        $('#tbl_BudgetDetail tbody tr.myData').each(function () {

            var bugtAmt = $(this).find('.bugtAmt').text();
            var effDate = $(this).find('.effDate').text();
            var fundTypeID = '';//$(this).find('.fundType').text();
            var remarks = $(this).find('.remarks').text();

            if (effDate != "") {
                var a = effDate.split('/');
                effDate = a[2] + "-" + a[1] + "-" + a[0];
            }

            TotalAmount = parseFloat(TotalAmount) + parseFloat(bugtAmt);

            ArrList.push({
                'BudgetID': $('#hdnBudgetID').val(), 'BudgetAmount': bugtAmt, 'EffectiveDate': effDate, 'FundTypeID': fundTypeID, 'Remarks': remarks
            });
        });
    }

    master = {
        BudgetID: $('#hdnBudgetID').val(),
        BudgetCode: $('#hdnBudgetCode').val(),
        BudgetAmount: TotalAmount,
        SectorID: $('#ddlSector').val(),
        BudgetDetail: ArrList
    }

    var E = "{Master: " + JSON.stringify(master) + "}";// alert(E); //return false;

    $.ajax({
        url: '/Accounts_Form/BudgetMaster/InsertUpdate',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (t == "success" || t == "updated") {

                swal({
                    title: "Success",
                    text: 'Budget Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#tab-1').removeClass('active');
                        $('#tab-2').addClass('active');
                        $("#dialog-Budgetdetail").modal("hide");
                        Get_Particulars();
                            //location.reload();
                            return false;
                        }
                    });
            }
        }
    });
}



