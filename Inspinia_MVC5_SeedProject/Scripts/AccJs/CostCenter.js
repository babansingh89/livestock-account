﻿$(document).ready(function () {
    cC.setDate();
    cC.getVoucherType();

    cC.getcostCenterDDL();
    cC.getDeptDDL();

    $('#btnsearch').click(function () {
        cC.getVoucherTypedate();
    });
    

    //$(document).on('click', '.trVDetails', function () {
    //    //var obj = {};
    //    //obj.VCH_No = $(ref).find('.VCH_No').html();
    //    //obj.AccountCode = $(ref).find('.AccountCode').html();

        

    //    //cC.getCostCenterList(this);
    //    //cC.getCostCenterList(this);
    //});
    $(document).on('blur', '#txtAmount', function () {
        //var totalamt = cC.objVouAccount.Amt;
        //var gridAmt = cC.gridSum;
        //var txtval = $(this).val();
        

        if ((parseFloat(cC.gridSum) + parseFloat($(this).val())) > parseFloat(cC.objVouAccount.Amt)) {
            alert('Amount is not greater than Voucher detail Amount.');
            $('#txtAmount').val('');
            $('#txtAmount').focus();
        }
       
    })

    $(document).on('keypress', '#txtAmount', function (evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;

        return true;
    });

    $(document).on('click', '#btnedit', function () {
        cC.rowEdit(this);
    });

    $('#btnAdd').click(function () {
        //Inventory.Buttonval = $(this).attr('data-attrval');
        //if (!frm.valid()) {
        //    return false;
        //} else {
        //cC.save(function (res) {
        //    cC.getCostCenterList(null,cC.objVouAccount);
        //    //cC.getCostCenterList(this);
        //        //Inventory.getAllInvDetails();
        //        cC.reset();
        //    });
        //}

        //var d = $('#ddlCostcenter option:selected').val();
        //alert(d);
        //var d1 = $('#ddldept option:selected').val();
        //alert(d1);

        if (($('#ddlCostcenter').val() == 0) && ($('#ddldept').val() == 0)) {
            alert('please Select Either Cost Center or Dept');
            return false;
        }
        if ($('#txtAmount').val() == '') {
            alert('please Enter Amount.');
            return false;
        } 
        //alert(11111);
        cC.save(function (res) {
            cC.getCostCenterList(null, cC.objVouAccount);
            //cC.getCostCenterList(this);
            //Inventory.getAllInvDetails();
            cC.reset();
        });
    });




});

var cC = {
isNumberKey: function(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
    ,
    checkObjectNull: function (obj) {
        var c = 0;
        //var arr = $.map(obj, function (v, i) {
        //    return [v];
        //});
        //if (arr.length == 0) {
        //    result = null;
        //}
        //return result;

        for (var k in obj) {
            c++;
        }
        if (c == 0) obj = null;
        return obj;
    },
    Id: '',
    gridSum:0,
    objVouAccount:{},
    setDate: function () {
        $('#dtfrom,#dtto').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow',
            dateFormat: 'dd/mm/yy'
        });


    },
    getVoucherType: function () {
        $.ajax({
            url: '/Accounts_Form/VoucherApproval/GetAllVoucherType',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                cC.popVoucher(response.Data)
                //console.log(response.Data);
            },
            error: function () { }
        });
    },
    popVoucher: function (data) {
        var ctlr_ddlVoucher = $('#ddlVoucherType');
        ctlr_ddlVoucher.empty();
        ctlr_ddlVoucher.append('<option value=0>--select--</option');
        $(data).each(function (index, item) {
            //ctlr_ddlYear.append('<option value=' + item.Year + '>' + item.YearDesc + '</option>');
            ctlr_ddlVoucher.append('<option value=' + item.VoucherTypeID + '>' + item.VoucherDescription + '</option>');
        })
    },
    getVoucherTypedate: function () {
        var obj = {};
        //obj.OffdayID = Offday1.Id;
        obj.Fromdate = $('#dtfrom').val();
        obj.ToDate = $('#dtto').val();
        obj.VCH_Type = $('#ddlVoucherType option:selected').val();

        $.ajax({
            url: '/Accounts_Form/CostCenter/GetVoucherByTypeDate',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(obj),
            dataType: "json",
            success: function (response) {
                console.log(response.Data);
                cC.popAllVoucherList(response.Data);
            },
            error: function () { }
        });


    },
    popAllVoucherList: function (data) {
        var DataHtml = [];
        $(data).each(function (i, element) {
            DataHtml.push('<tr class="trVList" style="cursor:pointer" onclick="cC.getVocherById(this);">');
            DataHtml.push('<td class="VCH_No" style=display:none;>' + element.VCH_No + '</td>');
            DataHtml.push('<td class="VCH_Date">' + element.VCH_Date + '</td>');
            DataHtml.push('<td class="VCH_Ref">' + element.VCH_Ref + '</td>');
            DataHtml.push('<td class="Narration">' + element.Narration + '</td>');
            DataHtml.push('</tr>');
        });
        
        $('#tblVoucherList tbody').html(DataHtml.join(''));
    },
    getVocherById: function (ref) {
       
        var obj = {};
        obj.VCH_No = $(ref).find('.VCH_No').html();
      
        $.ajax({
            url: '/Accounts_Form/CostCenter/getVoucherByID',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(obj),
            dataType: "json",
            success: function (response) {
                console.log(response.Data);
                cC.popAllVoucherDetails(response.Data);
            },
            error: function () { }
        });
    },
    popAllVoucherDetails: function (data) {
        $('#tblVoucherDetails tbody').html('');
        $('#tblCostCenter tbody').html('');
        cC.objVouAccount = {};

        var DataHtml = [];
        $(data).each(function (i, element) {
            DataHtml.push('<tr class="trVDetails" style="cursor:pointer" onclick="cC.getCostCenterList(this,null);">');
            DataHtml.push('<td class="VCH_No" style=display:none;>' + element.VCH_No + '</td>');
            DataHtml.push('<td class="DRCR" style=display:none;>' + element.DRCR + '</td>');
            DataHtml.push('<td class="VoucherDetailID" style=display:none;>' + element.VoucherDetailID + '</td>');
            DataHtml.push('<td class="AccountCode" style=display:none;>' + element.AccountCode + '</td>');
            DataHtml.push('<td class="VCH_Date">' + element.OldAccountHead + '</td>');
            DataHtml.push('<td class="VCH_Ref">' + element.AccountDescription + '</td>');
            DataHtml.push('<td class="DR_AMT" style="text-align: right;">' + parseFloat(element.DR_AMT).toFixed(2) + '</td>');
            DataHtml.push('<td class="CR_AMT" style="text-align: right;">' + parseFloat(element.CR_AMT).toFixed(2) + '</td>');
            DataHtml.push('</tr>');
        });

        $('#tblVoucherDetails tbody').html(DataHtml.join(''));
    },
    
    getcostCenterDDL: function () {
        $.ajax({
            url: '/Accounts_Form/CostCenter/GetCostCenter',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                cC.popcostCenter(response.Data)
            },
            error: function () { }
        });
    },
    popcostCenter: function (data) {
        var ctlr_ddlloffday = $('#ddlCostcenter');
        ctlr_ddlloffday.empty();
        ctlr_ddlloffday.append('<option value=0>--select--</option');
        $(data).each(function (index, item) {
            ctlr_ddlloffday.append('<option value=' + item.CostCenterCode + '>' + item.CostCenterDesc + '</option>');
        })
    },

    getDeptDDL: function () {
        $.ajax({
            url: '/Accounts_Form/CostCenter/Getdept',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                cC.popDept(response.Data)
            },
            error: function () { }
        });
    },
    popDept: function (data) {
        var ctlr_ddlloffday = $('#ddldept');
        ctlr_ddlloffday.empty();
        ctlr_ddlloffday.append('<option value=0>--select--</option');
        $(data).each(function (index, item) {
            ctlr_ddlloffday.append('<option value=' + item.DeptCode + '>' + item.DeptDesc + '</option>');
        })
    },
    getCostCenterList: function (ref,obj) {
        //var obj = {};
        //obj.VCH_No = $(ref).find('.VCH_No').html();
        //obj.AccountCode = $(ref).find('.AccountCode').html();
        if (ref == null) {
            cC.objVouAccount = obj;
        } else {
            cC.objVouAccount.VCH_No = $(ref).find('.VCH_No').html();
            cC.objVouAccount.AccountCode = $(ref).find('.AccountCode').html();
            cC.objVouAccount.DRCR = $(ref).find('.DRCR').html();
            cC.objVouAccount.VoucherDetailID = $(ref).find('.VoucherDetailID').html();
            cC.objVouAccount.Amt = $(ref).find('.CR_AMT').html() == 0 ? $(ref).find('.DR_AMT').html() : $(ref).find('.CR_AMT').html();
        }

        $.ajax({
            url: '/Accounts_Form/CostCenter/GetCostCenterList',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(cC.objVouAccount),
            dataType: "json",
            success: function (response) {
                //console.log(response.Data);
                cC.popCostCenter(response.Data);
            },
            error: function () { }
        });
    },
    popCostCenter(data) {
        cC.gridSum = 0;
        $('#tblCostCenter tbody').html('');
        var DataHtml = [];
        $(data).each(function (i, element) {
            DataHtml.push('<tr class="">');
            DataHtml.push('<td class="BillId" style=display:none;>' + element.BillId + '</td>');
            DataHtml.push('<td class="CostCenterCode" style=display:none;>' + element.CostCenterCode + '</td>');
            DataHtml.push('<td class="CostCenterDesc">' + element.CostCenterDesc + '</td>');
            DataHtml.push('<td class="DeptCode" style=display:none;>' + element.DeptCode + '</td>');
            DataHtml.push('<td class="DeptDesc">' + element.DeptDesc + '</td>');
            DataHtml.push('<td class="AdjustmentAmount" style="text-align: right;">' + (parseFloat(element.AdjustmentAmount)).toFixed(2) + '</td>');
            DataHtml.push('<td class="InstrumentNumber">' + element.InstrumentNumber + '</td>');
            DataHtml.push('<td class="InstrumentDate">' + element.InstrumentDate + '</td>');
            DataHtml.push('<td class="AdjustmentNumber">' + element.AdjustmentNumber + '</td>');
            DataHtml.push('<td class="InFavour">' + element.InFavour + '</td>');
            DataHtml.push('<td class="WardNo" style=display:none;>' + element.WardNo + '</td>');
            DataHtml.push('<td class="radioVal" style=display:none;>' + element.radioVal + '</td>');
            DataHtml.push('<td><input type="button" id="btnedit" class="ibtnDel btn btn-primary"  value="Edit"></td>');
            //DataHtml.push('<td><input type="button" id="btnDel" class="ibtnDel btn btn-md btn-danger"  value="Delete"></td>');
            DataHtml.push('</tr>');
            cC.gridSum += element.AdjustmentAmount;
        });

        $('#tblCostCenter tbody').html(DataHtml.join(''));
        //alert(cC.gridSum);
    },
    rowEdit: function (ref) {
        var ctrl_ddlCostCenter = $('#ddlCostcenter');
        var ctrl_ddldept = $('#ddldept');
        var crtl_txtAmount = $('#txtAmount');
        var ctrl_txtWard = $('#txtWard');
        var ctrl_optradio = $('input:radio[name=optradio]:checked').val();

        cC.Id = $(ref).closest('tr').find('.BillId').html();
        ctrl_ddlCostCenter.val($(ref).closest('tr').find('.CostCenterCode').html());
        ctrl_ddldept.val($(ref).closest('tr').find('.DeptCode').html());
        crtl_txtAmount.val($(ref).closest('tr').find('.AdjustmentAmount').html());
        ctrl_txtWard.val($(ref).closest('tr').find('.WardNo').html());
        var rv = $(ref).closest('tr').find('.radioVal').html()

        if (rv == "0") {
            $("#radioGeneral").prop("checked", true);
        } else if (rv == "1") {
            $("#radioPoor").prop("checked", true);
        } else {
            $("#radioNon").prop("checked", true);
        }
        $('#btnAdd').val('Update');
    },
    getFormdata: function () {
        var ctrl_InvDate = $("#dtStockDate");
        var ctrl_txtAccountDesc = $('#txtAccountDesc');
        var ctrl_hdnAuto = $('#hdnAuto');
        var ctrl_txtAmount = $('#txtAmount');


        return {
            Id: Inventory.Id,
            InvDate: ctrl_InvDate.val(),
            AccountDescription: ctrl_txtAccountDesc.val(),
            AccountCode: ctrl_hdnAuto.val(),
            Amount: ctrl_txtAmount.val()
        }
    },
    save: function (callback) {
        if (cC.checkObjectNull(cC.objVouAccount) == null) {
            alert('pleae click on voucher List ');
            return false;
        }
         var obj = {};
         obj.BillId = cC.Id;
         obj.VoucherDetailID = cC.objVouAccount.VoucherDetailID;
         obj.VCH_No = cC.objVouAccount.VCH_No;
         obj.AccountCode = cC.objVouAccount.AccountCode;
         obj.DRCR = cC.objVouAccount.DRCR;
         obj.CostCenterCode = $('#ddlCostcenter').val();
         obj.DeptCode = $('#ddldept').val();
         obj.AdjustmentAmount = $('#txtAmount').val();
         obj.WardNo = $('#txtWard').val();
         obj.radioVal = $('input:radio[name=optradio]:checked').val();


        $.ajax({
            url: '/Accounts_Form/CostCenter/InsUpdCostCenterdetails',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(obj),
            dataType: "json",
            success: function (response) {
                alert(response.Message);
                if (callback) {
                    callback(response);
                }
            },
            error: function () { }
        });
    },
    reset: function () {
        $('#btnAdd').val('Add');
        cC.Id = '';
        $('#ddlCostcenter').val('0');
        $('#ddldept').val('0')
        $('#txtAmount').val('');
        $('#txtWard').val('');
        //$('input:radio[name=optradio]:checked').val();
        $('input[name="optradio"]').prop('checked', false);

    }
       
      

}