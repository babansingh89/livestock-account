﻿var pathString = ""; var oldNetAmt = 0; var TranType = "", IncExp = "", TranTypeValue = "";

$(document).ready(function () {

    Set_Payment_billDate();
    Set_DefaultDate_payment();

    //    VENDOR BIILL AUTOCOMPLETE
    $('#txtVendorBillNo_payment').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType:'" + "Payment_BillNo" + "' , Desc:'" + $('#txtVendorBillNo_payment').val() + "', MND:'" + "I" + "', SetorID:'" + $('#ddlSector').val() + "', VendorCode:'" + $('#hdntxtExpVendorID_payment').val() + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/Item/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnVendorBillNo_payment').val(i.item.AccountCode);
            Bind_BillDetail(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtVendorBillNo_payment').keydown(function (evt) {
        
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtVendorBillNo_payment").val('');
            $("#hdnVendorBillNo_payment").val('');
           
        }
        if (iKeyCode == 46) {
            $("#txtVendorBillNo_payment").val('');
            $("#hdnVendorBillNo_payment").val('');
          
        }
    });

    //    VENDOR AUTOCOMPLETE
    $('#txtExpVendorName_payment').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType:'" + "Vendor_Bill" + "' ,Desc:'" + $('#txtExpVendorName_payment').val() + "', MND:'" + "L" + "', SetorID:'" + $('#ddlSector').val() + "', VendorCode:'" + $('#hdntxtExpVendorID_payment').val() + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/Item/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdntxtExpVendorID_payment').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtExpVendorName_payment').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtExpVendorName_payment").val('');
            $("#hdntxtExpVendorID_payment").val('');
            blank();
        }
        if (iKeyCode == 46) {
            $("#hdntxtExpVendorID_bill").val('');
            $("#hdntxtExpVendorID_payment").val('');
            blank();
        }
    });

    //    BANK AUTOCOMPLETE
    $('#txtPaymentBank').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType:'" + "Vendor_Bank" + "' ,Desc:'" + $('#txtPaymentBank').val() + "', MND:'" + "B" + "', SetorID:'" + $('#ddlSector').val() + "', VendorCode:'" + $('#hdntxtExpVendorID_payment').val() + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/Item/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnpaymentBank').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtPaymentBank').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtPaymentBank").val('');
            $("#hdnpaymentBank").val('');
        }
        if (iKeyCode == 46) {
            $("#txtPaymentBank").val('');
            $("#hdnpaymentBank").val('');
        }
    });

    //    DASHBOARD BIILL AUTOCOMPLETE
    $('#txtDashBillNo').autocomplete({
        source: function (request, response) {

            var S = "{TransType:'" + "Dashboard_AutoBillNo" + "' , Desc:'" + $('#txtDashBillNo').val() + "', FromDate:'" + $('#txtFromDate').val() + "', ToDate:'" + $('#txtToDate').val() + "', SectorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/Item/Dashboard',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.BillDesc,
                                BillId: item.BillId,
                                BillNo: item.BillNo
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnDashBillNo').val(i.item.BillNo);
        },
        minLength: 0
    }).click(function () {
        if ($('#txtFromDate').val() == "") { $('#txtFromDate').focus(); return false; }
        if ($('#txtToDate').val() == "") { $('#txtToDate').focus(); return false; }
        $(this).autocomplete('search', ($(this).val()));
        });
    $('#txtDashBillNo').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtDashBillNo").val('');
            $("#hdnDashBillNo").val('');
        }
        if (iKeyCode == 46) {
            $("#txtDashBillNo").val('');
            $("#hdnDashBillNo").val('');
        }
    });

    //    DASHBOARD SERVICE AUTOCOMPLETE
    $('#txtServiceName').autocomplete({
        source: function (request, response) {
            var S = "{TransType:'" + "Service" + "' , Desc:'" + $('#txtServiceName').val() + "', FromDate:'" + $('#txtFromDate').val() + "', ToDate:'" + $('#txtToDate').val() + "', SectorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/Item/Dashboard',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnServiceName').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtServiceName').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtServiceName").val('');
            $("#hdnServiceName").val('');
        }
        if (iKeyCode == 46) {
            $("#txtServiceName").val('');
            $("#hdnServiceName").val('');
        }
    });

    $('#btnExpRefresh_payment').click(Blank_payment);
    $('#btnBillAdd_payment').click(Add_bill_payment);
    $('#btnExpSave_payment').click(Save_Payment);


    $.getJSON("https://www.encodedna.com/library/sample.json", function (data) {
            alert(JSON.stringify(data));
        });
   
});

function GetType_Payment(serviceType, shortSType, ID, ExpInc, TranValue) {
    TranTypeValue = TranValue; 
    TranType = shortSType;
    Populate_PaymentAdjHead(shortSType);
    $('#ddlExIncomePaymentType').prop('disabled', true); $('#ddlExIncomePaymentType').val(ExpInc);
    $('.banksection').css('display', '');
}
function Set_Payment_billDate() {
    $('#txtpaymentInstDate, #txtPaymentDate_payment, #txtToDate, #txtFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function Set_DefaultDate_payment() {
    var a = localStorage.getItem("FinYear");
        var fy = a.split('-');
        var validDate = '01/04/' + fy[0];
        $('#txtFromDate').val(validDate);

    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtPaymentDate_payment').val(output);
    $('#txtToDate').val(output);
    
}
function img_payment_delete() {

    $('#ExpIframe_payment').attr('src', '');
    $("#ExpenseflPic_payment").val('');

    var tableName = "", ID = "", urls = "";
    transactionType = "Payment";
    ID = $('#hdnPaymentBillID').val();
    urls = "~/UploadItemImage/Payment/";

    if (ID != "") {
        var E = "{transactionType: '" + transactionType + "', ID: '" + ID + "', urls: '" + urls + "'}";
        $.ajax({
            url: '/Accounts_Form/Item/Delete_Picture',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {

            }
        });
    }
}
function Add_bill_payment() {
   
    if ($('#hdnVendorBillNo_payment').val() == "") { $('#txtVendorBillNo_payment').focus(); return false; }  
    if ($('#txtVendorBillDate_payment').val() == "") { $('#txtVendorBillDate_payment').focus(); return false; }
    if ($('#txtPayGrossAmt_payment').val() == "") { $('#txtPayGrossAmt_payment').focus(); return false; }
    if ($('#txtPayNetAmt_payment').val() == "") { $('#txtPayNetAmt_payment').focus(); return false; }
   
    var chkAccCode = $("#hdnVendorBillNo_payment").val();

    var l = $('#tbl_Expense_payment tr.myData_payment').find("td[chk-data='" + chkAccCode + "']").length;

    if (l > 0) {
        alert('Sorry ! This Vendor Bill No. is Already Available.'); $("#txtVendorBillNo_payment").val(''); $("#hdnVendorBillNo_payment").val('');
        $("#txtVendorBillDate_payment").val(''); $("#txtPayGrossAmt_payment").val(''); $("#txtPayNetAmt_payment").val('');
        $('#txtVendorBillNo_payment').focus();
        return false;
    }
   
    var html = ""

    var $this = $('#tbl_Expense_payment .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData_payment'>"
        + '<td colspan="5" chk-data=' + $("#hdnVendorBillNo_payment").val() + ' chk-tax=' + $("#hdnVendorBillNo_payment").val() + '>'
        + '<div class="forum-item cls_1">'
        + '<div class="row">'
        + '<div class="col-md-5">'
        + '<div class="forum-icon">'
        + '<i class="fa fa-trash-o del_img"  onclick="Delete_payment(this, \'' + $("#hdnVendorBillNo_payment").val() + '\')" style="cursor:pointer; color:red" title="Delete"></i>'
        + '</div>'
        + '<div class="forum-sub-title cls_ExpItemDesc" style="padding-top:15px">' + $("#txtVendorBillNo_payment").val() + '</div>'
        + '</div>'

        + '<div class="col-md-1 forum-info" style="display:none">'
        + '<span class="views-number cls_ItemID" >'
        + $("#hdnVendorBillNo_payment").val()
        + '</span>'
        + '<div>'
        + '<small>ItemID</small>'
        + '</div>'
        + '</div>'

        + '<div class="col-md-2 forum-info">'
        + '<span class="views-number cls_ExpItemQty">'
        + $("#txtVendorBillDate_payment").val()
        + '</span>'
        + '<div>'
        + '<small>Bill Date</small>'
        + '</div>'
        + '</div>'

        + '<div class="col-md-2 forum-info">'
        + '<span class="views-number cls_GrossAmount">'
        + $("#txtPayGrossAmt_payment").val()
        + '</span>'
        + '<div>'
        + '<small>Gross Amt</small>'
        + '</div>'
        + '</div>'

        + '<div class="col-md-2 forum-info">'
        + '<span class="views-number cls_Amount">'
        + $("#txtPayNetAmt_payment").val()
        + '</span>'
        + '<div>'
        + '<small>Net Amount</small>'
        + '</div>'
        + '</div>'


        + '</div>'
        + '</div>'
        + '</td>'
    html + "</tr>";

    $parentTR.after(html);

    $("#hdnVendorBillNo_payment").val(''); $("#txtVendorBillNo_payment").val(''); $("#txtVendorBillDate_payment").val('');
    $("#txtPayGrossAmt_payment").val(''); $("#txtPayNetAmt_payment").val(''); 
    Calculate_Total_payment();

}
function Calculate_Total_payment() {

    var Total = 0;
    var html = ""

    if ($("#tbl_Expense_payment tbody tr.myData_payment").length > 0) {
        $("#tbl_Expense_payment tbody tr.myData_payment").each(function (index, value) {
            var Amount = $(this).find('.cls_Amount').text();
            Total = parseFloat(Total) + parseFloat(Amount);
            $('#hdnExpGrossAmt_payment').val(Total);
        });
    }
  
    $('#tbl_Expense_payment .trfooter_payment').remove();

    html += "<tr class='trfooter_payment' style='background-color:#F5F5F6' >"
        + "<td style='text-align:right; font-weight:bold;' colspan='3'>SubTotal : </td>"
        + "<td style='text-align:right; font-weight:bold; width:20%' colspan='2'><span class='clsSubTotal' > &#x20b9; " + (Total).toFixed(2) + "</span></td>"
    html + "</tr>";

    $('#tbl_Expense_payment tr:last').after(html);

    if ($("#tbl_Expense_payment tbody tr.myData_payment").length > 0)
        Calculate_AdjCalPayment();

    if ($("#ddlExpAdjType_payment").val() != "")
        Calculate_RoundNetPayment();
}
function Calculate_AdjCalPayment() {

    //var adjtype = $('#ddlExpAdjType_payment').val() == "" ? 0 : $('#ddlExpAdjType_payment').val();
    //var adjamt = $("#txtExpAdjAmount_payment").val() == "" ? 0 : $("#txtExpAdjAmount_payment").val();
    //var adjbillHead = $('#ddlExpAdjHead_payment').val();

    //if (adjtype == "") { $('#ddlExpAdjType_payment').focus(); $("#txtExpAdjAmount_payment").val(''); return false; }
    //if (adjbillHead == "") { $('#ddlExpAdjHead_payment').focus(); $("#txtExpAdjAmount_payment").val(''); return false; }

    var Total = 0;
    if ($("#tbl_Expense_payment tbody tr.myData_payment").length > 0) {
        $("#tbl_Expense_payment tbody tr.myData_payment").each(function (index, value) {
            var Amount = $(this).find('.cls_Amount').text();
            Total = parseFloat(Total) + parseFloat(Amount);
        });
    }
    $('#txtExpNetAmount_payment').val(Total);
    var a = $('#txtExpNetAmount_payment').val() == "" ? 0 : $('#txtExpNetAmount_payment').val();    //Detail Total
    var e = $('#txtExpRoundOff_payment').val() == "" ? 0 : $('#txtExpRoundOff_payment').val();     //round off
    var f = ($('#txtExpAdjAmount_payment').val() == "" || $('#txtExpAdjAmount_payment').val() == ".") ? 0 : $('#txtExpAdjAmount_payment').val();   //adjamount
    var adjtype = $('#ddlExpAdjType_payment').val() == "" ? 0 : $('#ddlExpAdjType_payment').val();

    
    var c = 0, s = 0, k = 0;
    if (adjtype != "") {
        if (adjtype == "A")
            c = (parseFloat(a)  + parseFloat(f)).toFixed(2);
        else
            c = (parseFloat(a) - parseFloat(f)).toFixed(2);

       

        s = Math.round(parseFloat(c)).toFixed(2);
        k = parseFloat(s) - parseFloat(c);

       
    }
    else {
        c = (parseFloat(a) + parseFloat(f)).toFixed(2);
        s = Math.round(parseFloat(c)).toFixed(2);
        k = parseFloat(s) - parseFloat(c);
    }

    $('#txtExpRoundOff_payment').val(parseFloat(k).toFixed(2));
    $('#txtExpNetAmount_payment').val(Math.round(s).toFixed(2));

}
function Calculate_RoundNetPayment() {

    var adjtype = $('#ddlExpAdjType_payment').val() == "" ? 0 : $('#ddlExpAdjType_payment').val();
    var adjamt = $("#txtExpAdjAmount_payment").val() == "" ? 0 : $("#txtExpAdjAmount_payment").val();
    var adjbillHead = $('#ddlExpAdjHead_payment').val();

    if (adjtype == "") { $('#ddlExpAdjType_payment').focus(); $("#txtExpAdjAmount_payment").val(''); return false; }
    if (adjbillHead == "") { $('#ddlExpAdjHead_payment').focus(); $("#txtExpAdjAmount_payment").val(''); return false; }

    var Total = 0;
    if ($("#tbl_Expense_payment tbody tr.myData_payment").length > 0) {
        $("#tbl_Expense_payment tbody tr.myData_payment").each(function (index, value) {
            var Amount = $(this).find('.cls_Amount').text();
            Total = parseFloat(Total) + parseFloat(Amount);
        });
    }
    $('#txtExpNetAmount_payment').val(Total);
    var a = $('#txtExpNetAmount_payment').val() == "" ? 0 : $('#txtExpNetAmount_payment').val();

    var c = 0, s = 0, k = 0;

   
    if (adjamt > 0) {

        if (adjtype == "A")
            c = (parseFloat(a)  + parseFloat(adjamt)).toFixed(2);
        else
            c = (parseFloat(a)  - parseFloat(adjamt)).toFixed(2);

        s = Math.round(parseFloat(c)).toFixed(2);
        k = parseFloat(s) - parseFloat(c);
       
    }
    else {

        Calculate_AdjCalPayment(); return false;
    }

    $('#txtExpRoundOff_payment').val(parseFloat(k).toFixed(2));
    $('#txtExpNetAmount_payment').val(Math.round(s).toFixed(2));
}
function Bind_BillDetail(BillID) {
    var E = "{POID: '" + BillID + "', TransactionType: '" + "Bill_Detail" + "', TransType: '" + "B" + "', SectorID: '" + $('#ddlSector').val() + "', FinYear: '" + localStorage.getItem("FinYear") + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/Load_PO',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {
            var tt = data.Data; 

            var t1 = tt["Table"]; 

            if (t1.length > 0) {
               
                $('#txtVendorBillDate_payment').val(t1[0].VendorBillDate); $('#txtPayGrossAmt_payment').val(t1[0].GrossAmt); $('#txtPayNetAmt_payment').val(t1[0].NetAmtAfterTcs);

            }
        }
    });
}
function Delete_payment(ID, BillID) {
    $(ID).closest('tr').remove(); Calculate_Total_payment();
}
function Populate_PaymentAdjHead(shortSType) {
    var E = "{TransType: '" + shortSType + "', SectorID: '" + $('#ddlSector').val() + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/Load_BillAccHead',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        async: false,
        success: function (data, status) {
            var t = data.Data;
            if (t.length > 0) {
                $('#ddlExpAdjHead_payment').empty();
                $('#ddlExpAdjHead_payment').append('<option value="" selected>Select</option');
                $(t).each(function (index, item) {
                    $('#ddlExpAdjHead_payment').append('<option value=' + item.AccountCode + '>' + item.AccountDescription + '</option>');
                });
            }
        }
    });
}
function Blank_payment() {
    $('#ddlExIncomePaymentType').val(''); 
    $('.payment').val(''); $('#lblPaymentBillNo').text(''); Set_DefaultDate_payment();
    $("#tbl_Expense_payment tbody tr.myData_payment").remove(); $('#tbl_Expense_payment tbody tr.trfooter_payment').remove();
    $('#ExpIframe').attr('src', '');
    $('#txtVendorBillNo_payment').val(''); $('#hdnVendorBillNo_payment').val('');
    $('#txtVendorBillDate_payment').val(''); $('#txtPayGrossAmt_payment').val(''); $('#txtPayNetAmt_payment').val('');
}
function Expense_Preview_payment(input) {

    var a = input.files[0].name;
    if (a != "") {
        var validExtensions = ['pdf']; //array of valid extensions
        var fileName = a;
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
        if ($.inArray(fileNameExt, validExtensions) == -1) {
            alert("Invalid file type. Please Select PDF File only.");
            $("#ExpenseflPic_payment").val('');
            return false;
        }
    }

    if (input.files && input.files[0]) {
        path = $('#ExpenseflPic_payment').val().substring(12);
        pathString = '/UploadItemImage/Payment/' + path;
    }
}
function Save_Payment() {

    if ($("#ddlExIncomePaymentType").val() == "") {
        toastr.error('Please, Select Either Income or Expense !!', 'Warning')
        $("#ddlExIncomePaymentType").focus(); return false;
    }
    if ($("#hdntxtExpVendorID_payment").val() == "") {
        toastr.error('Please, Select Vendor Name !!', 'Warning')
        $("#txtExpVendorName_payment").focus(); return false;
    }
    if ($("#txtPaymentDate_payment").val() == "") {
        toastr.error('Please, Enter Payment Date !!', 'Warning')
        $("#txtPaymentDate_payment").focus(); return false;
    }
    if ($("#txtExpRemark_payment").val() == "") {
        toastr.error('Please, Enter Remarks !!', 'Warning')
        $("#txtExpRemark_payment").focus(); return false;
    }

    var master = {};
    master = {
        BillId: $('#hdnPaymentBillID').val(),
        VendorCode: $('#hdntxtExpVendorID_payment').val(),
        VendEmail: $('#txtEmail_payment').val(),
        BillStatus: TranTypeValue, //TranType,
        IncomeExpense: $('#ddlExIncomePaymentType').val(),
        BillNo: $('#lblPaymentBillNo').text(),
        BillDate: $('#txtPaymentDate_payment').val(), //Payment Date

        TransType: TranType,
        TaxType: 'E',

        BankID: $('#hdnpaymentBank').val(),
        InstrumentType: $('#ddlpaymentInstType').val(),
        InstrumentNo: $('#txtpaymentInstNo').val(),
        InstrumentDate: $('#txtpaymentInstDate').val(),

        AdjType: $('#ddlExpAdjType_payment').val(),
        AdjAmt: $('#txtExpAdjAmount_payment').val(),
        BillAdjHead: $('#ddlExpAdjHead_payment').val(),
        RoundOff: $('#txtExpRoundOff_payment').val(),
        GrossAmt: $('#hdnExpGrossAmt_payment').val(),
        NetAmt: $('#txtExpNetAmount_payment').val(),
        Remarks: $('#txtExpRemark_payment').val(),
        DocFilePath: pathString,
        Sectorid: $('#ddlSector').val()
    }

    var grdLen = $('#tbl_Expense_payment tbody tr.myData_payment').length; var ArrList = [];
    if (grdLen > 0) {
        $('#tbl_Expense_payment tbody tr.myData_payment').each(function () {

            var ItemID = $(this).find('.cls_ItemID').text();
            var GrossAmount = $(this).find('.cls_GrossAmount').text();
            var NetAmount = $(this).find('.cls_Amount').text();

            ArrList.push({
                'ItemId': ItemID, 'GrossAmt': GrossAmount, 'NetAmt': NetAmount, 'sectorid': $('#ddlSector').val()
            });
        });
    }

    var ItemDesc = JSON.stringify(ArrList);
    var E = "{BillMaster: " + JSON.stringify(master) + ", BillDetail: " + ItemDesc + "}";

    //return false;
    $.ajax({
        url: '/Accounts_Form/Item/Payment_InsertUpdate',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200 && t != "") {
                Upload_payment('ExpenseflPic_payment', t);

                swal({
                    title: "Success",
                    text: 'Payment Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });
            }
        }
    });

}
function Upload_payment(flPic, PONO) {

    var formData = new FormData();
    var totalFiles = document.getElementById(flPic).files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById(flPic).files[i];
        fileName = $('#ExpenseflPic_payment').val().substring(12);
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.'));
        formData.append(flPic, file, PONO + fileNameExt);
    }

    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/UploadPicture_payment',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        async: false,
        success: function (response) {
        }
    });
}
function Execute_Action_T(ID) {

    var POID = $(ID).closest('tr').find('.PoId').text(); 
    var ActionName = $(ID).closest('tr').find('.cls_Action').val(); 
    var TransType = $(ID).closest('tr').find('.TransType').text(); 
    $('.cls_Action').val('');


    if (ActionName != "") {
        if (ActionName == "E")  //Edit
            Action_Edit_T(POID, TransType);
        if (ActionName == "D")  //Delete
            Action_Delete(POID, TransType);

    }

    $(ID).closest('tr').find('.cls_Action').val(ActionName);
}
function Action_Edit_T(POID, TransType) {
    var E = "{POID: '" + POID + "', TransactionType: '" + "Bill_Detail" + "', TransType: '" + TransType + "', SectorID: '" + $('#ddlSector').val() + "', FinYear: '" + localStorage.getItem("FinYear") + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/Load_PO',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {
            var tt = data.Data; //alert(JSON.stringify(t));

            var t1 = tt["Table"];
            var t2 = tt["Table1"];
            var t3 = tt["Table2"];

            if (t1.length > 0) {
                $('#lblPaymentBillNo').text(t1[0].PoNo);
                $('#txtExpVendorName_payment').val(t1[0].VendorName); $('#hdntxtExpVendorID_payment').val(t1[0].VendorCode); $('#txtEmail_payment').val(t1[0].VendEmail);

                $('#txtExpRemark_payment').val(t1[0].Remarks); 
                $('#ddlExpAdjType_payment').val(t1[0].AdjType); $('#txtExpAdjAmount_payment').val(t1[0].AdjAmt.toFixed(2)); $('#txtExpRoundOff_payment').val(t1[0].RoundOff.toFixed(2)); $('#txtExpNetAmount_payment').val(t1[0].NetAmt.toFixed(2));
                Populate_PaymentAdjHead(t1[0].TransType); $('#ddlExpAdjHead_payment').val(t1[0].BillAdjHead);

                $('#hdnExpGrossAmt_payment').val(t1[0].GrossAmt); $('#txtPaymentBank').val(t1[0].BankName); $('#hdnpaymentBank').val(t1[0].BankID);
                $('#ddlpaymentInstType').val(t1[0].InstrumentType); $('#txtpaymentInstNo').val(t1[0].InstrumentNo); $('#txtpaymentInstDate').val(t1[0].InstrumentDate); $('#txtPaymentDate_payment').val(t1[0].PoDate);
                $('#hdnPaymentBillID').val(POID); TranType = t1[0].TransType; TranTypeValue = t1[0].BillStatus;

                $('#ddlExIncomePaymentType').val(t1[0].IncomeExpense); IncExp = t1[0].IncomeExpense;

                $('#ExpIframe_payment').attr('src', '');
                $('#ExpIframe_payment').attr('src', t1[0].DocFilePath);

                $('#lblPaymentVoucherNo').text(t1[0].RefVoucherSlNO == null ? "" : (" - " + t1[0].RefVoucherSlNO));
                $('#hdnPaymentVoucherNo').val(t1[0].VoucherNo);
                $('#lblPaymentVoucherNo1').text(t1[0].RefVoucherSlNO1 == null ? "" : ("  " + t1[0].RefVoucherSlNO1));
                $('#hdnPaymentVoucherNo1').val(t1[0].VoucherNo1);
                t1[0].VoucherNo == "" ? ($('#btnPrintPaymentVch').css('display', 'none')) : ($('#btnPrintPaymentVch').css('display', ''));

                Populate_Expense_payment(t2);

                $('.payment').toggleClass('toggled');
            }
        }
    });
}
function Populate_Expense_payment(detail) {
   
    $('#tbl_Expense_payment tbody tr.myData_payment').remove();
    $('#tbl_Expense_payment tbody tr.trfooter_payment').remove();

    if (detail.length > 0) {

        var html = ""

        var $this = $('#tbl_Expense_payment .test_0');
        $parentTR = $this.closest('tr');
        var totalGross = 0;


        for (var i = 0; i < detail.length; i++) {

            html += "<tr class='myData_payment'>"
                + '<td colspan="5" chk-data=' + detail[i].BillId + ' chk-tax=' + detail[i].BillId + '>'
                + '<div class="forum-item cls_1">'
                + '<div class="row">'
                + '<div class="col-md-5">'
                + '<div class="forum-icon">'
                + '<i class="fa fa-trash-o del_img"  onclick="Delete_payment(this, \'' + detail[i].BillId + '\')" style="cursor:pointer; color:red" title="Delete"></i>'
                + '</div>'
                + '<div class="forum-sub-title cls_ExpItemDesc" style="padding-top:15px">' + detail[i].ItemDescription + '</div>'
                + '</div>'

                + '<div class="col-md-1 forum-info" style="display:none">'
                + '<span class="views-number cls_ItemID" >'
                + detail[i].ItemId
                + '</span>'
                + '<div>'
                + '<small>ItemID</small>'
                + '</div>'
                + '</div>'

                + '<div class="col-md-2 forum-info">'
                + '<span class="views-number cls_ExpItemQty">'
                + detail[i].BillDate
                + '</span>'
                + '<div>'
                + '<small>Bill Date</small>'
                + '</div>'
                + '</div>'

                + '<div class="col-md-2 forum-info">'
                + '<span class="views-number cls_ExpItemRate">'
                + detail[i].GrossAmt
                + '</span>'
                + '<div>'
                + '<small>Gross Amt</small>'
                + '</div>'
                + '</div>'

                + '<div class="col-md-2 forum-info">'
                + '<span class="views-number cls_Amount">'
                + detail[i].NetAmt
                + '</span>'
                + '<div>'
                + '<small>Net Amount</small>'
                + '</div>'
                + '</div>'


                + '</div>'
                + '</div>'
                + '</td>'
            html + "</tr>";

            totalGross = parseFloat(totalGross) + parseFloat(detail[i].NetAmt);

        }

        html += "<tr class='trfooter_payment' style='background-color:#F5F5F6' >"
            + "<td style='text-align:right; font-weight:bold;' colspan='3'>SubTotal : </td>"
            + "<td style='text-align:right; font-weight:bold; width:20%' colspan='2'><span class='clsSubTotal' > &#x20b9; " + (parseFloat(totalGross).toFixed(2)) + "</span></td>"
        html + "</tr>";

        $parentTR.after(html);
        Calculate_AdjCalPayment();
    }
}
function blank()
{
  
    $("#tbl_Expense_payment tbody tr.myData_payment").remove();
    Calculate_Total_payment();
    $('#txtExpAdjAmount_payment').val(''); $('#txtExpRoundOff_payment').val(''); $('#txtExpNetAmount_payment').val(''); 
    $('#lblPaymentVoucherNo').text(''); $('#btnPrintPaymentVch').css('display', 'none');
}
function PrintPaymentVouchers(con) {

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "VoucherCash.rpt",
        FileName: "Voucher",
        Database:''
    });

    detail.push({
        VoucherNumber: con == 1 ? $('#hdnPaymentVoucherNo').val() : $('#hdnPaymentVoucherNo1').val(),
        sectorid: $("#ddlSector").val()
    });

    final = {
        Master: master,
        Detail: detail
    }


    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();

}



//*******************************************************************************************************************
//                                                      DASHBOARD
//*******************************************************************************************************************
function Dashboard()
{
    var E = "{TransType: '" + "Dashboard" + "', Desc: '" + $('#hdnDashBillNo').val() + "', Service: '" + $('#hdnServiceName').val() + "', FromDate:'" + $('#txtFromDate').val() + "', ToDate:'" + $('#txtToDate').val() + "', SectorID: '" + $('#ddlSector').val() + "'}";
    //alert(E);
    //return false;
    $.ajax({
        url: '/Accounts_Form/Item/Dashboard',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; //alert(JSON.stringify(t));

            //$('#tbldashboard').DataTable({
            //    destroy: true,
            //});
            //if (data.Status == 200 && t != "") {
                Bind_Dashboard(t);
           // }
        }
    });
}
function Bind_Dashboard(data)
{
   
    //if (data.length > 0)
    //{
            $('#tbldashboard tbody tr').remove();
       
            var columnData = [{ "mDataProp": "BillId" },
            { "mDataProp": "BillNo" },
            { "mDataProp": "BillDate" },
            { "mDataProp": "BuyerName" },
            { "mDataProp": "NetAmt" },
            { "mDataProp": "ReceiptAmt" },
            { "mDataProp": "DueAmt" }];

            var columnDataHide = [0];

            var oTable = $('#tbldashboard').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [],

                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aaData": data,
                "aoColumns": columnData,
                "aoColumnDefs": [
                    {
                        "targets": columnDataHide,
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [0],
                        "className": "BillId"
                    },
                    {
                        "targets": [1],
                        "className": "text-center BillNo"
                    },
                    {
                        "targets": [2],
                        "className": "text-center BillDate"
                    },
                    {
                        "targets": [3],
                        "className": "acccode"
                    },
                    {
                        "targets": [4,5,6],
                        "className": "text-right amount"
                    }
                ],
                'iDisplayLength': 10,
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;

                    // converting to interger to find total
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var netTotal = api
                        .column(4)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    var receiptTotal = api
                        .column(5)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    var dueTotal = api
                        .column(6)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    $(api.column(3).footer()).html('Total');
                    $(api.column(4).footer()).html('(&#x20b9;) ' + netTotal);
                    $(api.column(5).footer()).html('(&#x20b9;) ' + receiptTotal);
                    $(api.column(6).footer()).html('(&#x20b9;) ' + dueTotal);
                },
                destroy: true,
            });
        
    //}
}





