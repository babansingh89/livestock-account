﻿var master = []; var k = 0;

$(document).ready(function () {

    Set_ConDefaultDate();

    $("#txtConReceiptType").autocomplete({

        source: function (request, response) {

            var budgetTypeID = $('#ddlConBudgetType').val();
            var E = "{TrasactionType: '" + "ConParticulars" + "', BudgetTypeID: '" + budgetTypeID + "',  Desc: '" + $.trim($('#txtConReceiptType').val()) + "'}";

            $.ajax({
                type: "POST",
                url: '/Accounts_Form/BudgetMaster/Get_ConParticulars',
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {

                            AutoComplete.push({
                                label: item.BudgetDescription,
                                BudgetID: item.BudgetID
                            });
                        });

                        response(AutoComplete);
                        PopulateConGrid(serverResponse.Data);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#txtConReceiptType').val(i.item.label);
            Get_ConParticulars();
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });

});


function Set_ConDefaultDate() {
    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtConEffectiveDate').val(output);

    $('#txtConEffectiveDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        todayHighlight: true
    });
}
function Get_ConCondition() {
    var budgetTypeID = $('#ddlConBudgetType').val();
    if (budgetTypeID == 'R') {
        $('.clsConCatSubCat').css('display', '');
        $('#ddlConCategory').val('');
        $('#ddlConSubCategory').val('');
        $('#txtConReceiptType').val('');
    }
    else {
        $('.clsConCatSubCat').css('display', 'none');
        $('#ddlConCategory').val('E');
        $('#ddlConSubCategory').val('');
        $('#txtConReceiptType').val('');
    }
    Get_ConParticulars();
}
function Get_ConParticulars() {

    var budgetTypeID = $('#ddlConBudgetType').val();
    var catID = $('#ddlConCategory').val();
    var subcatID = $('#ddlConSubCategory').val();
    var SectorID = $('#ddlSector').val();
    if (SectorID == '') { alert('Please Select Sector.'); $('#ddlSector').focus(); return false; }

    if (budgetTypeID != "") {
        var E = "{TrasactionType: '" + "ConParticulars" + "', BudgetTypeID: '" + budgetTypeID + "', CategoryID: '" + catID + "', SubCategoryID: '" + subcatID + "', SectorID: '" + $('#ddlSector').val() + "',  Desc: '" + $.trim($('#txtConReceiptType').val()) + "'}";// alert(E);
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BudgetMaster/Get_ConParticulars',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data;
                $('#page-wrapper').toggleClass('sk-loading');
                PopulateConGrid(t);
                $('#page-wrapper').removeClass('sk-loading');
            }
        });
    }
}
function PopulateConGrid(detail) {
    var html = "";

    var table = $('#myConTable');
    $('#myConTable tbody tr.myData').remove();

    if (detail.length > 0) {
        for (var i = 0; i < detail.length; i++) {
            html += "<tr class='myData'>"
                + "<td style='text-align:right; display:none;' class='BudgetID' >" + detail[i].BudgetID + "</td>"
                + "<td style='text-align:left; '>" + detail[i].BudgetDescription + "</td>"
                + "<td style='text-align:center'>" + (detail[i].BudgetCode == null ? "" : detail[i].BudgetCode) + "</td>"
                + "<td style='text-align:right;' class='clsBudgetAmt' >" + (parseFloat(detail[i].BudgetAmount)).toFixed(2) + "</td>"
                + "<td style='text-align:center;' ><div style='clear:both;'><div style='float:left; text-align:right; width:80%'><span  class='clsConAmount'>" + (parseFloat(detail[i].ConcurranceAmount)).toFixed(2) +  "</span></div><div style='float:right; text-align:right; width:20%'><img src='/Content/Images/add.png' title='Add' style='margin-left:20px;height:20px; width: 20px; cursor:pointer;' onclick='PopUP_ConBudgetDetail(this)' /></div></div></td>"
                + "<td style='text-align:right;' class='clsBalAmt' >" + (parseFloat(detail[i].BalanceAmount)).toFixed(2) + "</td>"
            html + "</tr>";
        }

        table.append(html);
    }
}


//POPUP DATA CALCULALTION
function PopUP_ConBudgetDetail(ID) {

    var BudgetID = $(ID).closest('tr').find('.BudgetID').text();
    var BudgetCode = $(ID).closest('tr').find('.clsBudgetCode').val();
    var BudgetAmount = $(ID).closest('tr').find('.clsBudgetAmt').text();
    var ConAmount = $(ID).closest('tr').find('.clsConAmount').text();
    var BalAmount = $(ID).closest('tr').find('.clsBalAmt').text();

    $('#hdnConBudgetID').val(BudgetID);
    $('#hdnConBudgetCode').val(BudgetCode);
    $('#hdnConBudgetAmount').val(BudgetAmount);
    $('#hdnConAmount').val(ConAmount);
    $('#hdnConBalAmount').val(BalAmount);

    Bind_ConBudgetDetail(BudgetID);
    $("#dialog-ConBudgetdetail").modal("show");
}
function Show_ConBudgetDetail(data) {
    var html = "";

    $("#tbl_ConBudgetDetail tbody tr.myData").remove();

    if (data.length > 0) {

        
        var $this = $('#tbl_ConBudgetDetail .test_0');
        $parentTR = $this.closest('tr');

        for (var i = 0; i < data.length; i++) {
            html += "<tr class='myData' >"
                + "<td  class='bugtAmt' style='text-align:right;'>" + data[i].ConcurranceAmount + "</td>"
                + "<td class='effDate' style='text-align:center;'>" + data[i].ConcurranceDate + "</td>"
                + "<td class='refNo' style='text-align:center;'>" + data[i].ReferanceNo + "</td>"
                + "<td class='remarks'>" + (data[i].Remarks == null ? "" : data[i].Remarks) + "</td>"
                + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='DeleteConBudgetDetail(this);' /></td>"
            html + "</tr>";
        }
        $parentTR.after(html);
    }
}
function DeleteConBudgetDetail(ID) {
    $(ID).closest('tr').remove(); $("#txtConBudgetAmt").focus();

    var grdLen = $('#tbl_ConBudgetDetail tbody tr.myData').length; var TotalAmount = 0;
    if (grdLen > 0) {
        $('#tbl_ConBudgetDetail tbody tr.myData').each(function () {

            var bugtAmt = $(this).find('.bugtAmt').text();

            TotalAmount = parseFloat(TotalAmount) + parseFloat(bugtAmt);

        });
    }

    var BudgetAmt = $('#hdnConBudgetAmount').val(); 
    if (parseFloat(TotalAmount) > parseFloat(BudgetAmt)) {
        $('.dvErrorMsg').css('display', '');
        $('#errorMsg').text('Sorry ! Total Concurrance amount is greater than ' + BudgetAmt); return false;
    }
    else {
        $('.dvErrorMsg').css('display', 'none');
        $('#errorMsg').text('');
    }
    return false;
}
function Bind_ConBudgetDetail(BudgetID) {
    if (BudgetID != "") {
        var E = "{TrasactionType: '" + "ConSelect" + "', BudgetID: '" + BudgetID + "', SectorID: '" + $('#ddlSector').val() + "'}";// alert(E);
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BudgetMaster/Get_BudgetDetails',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data;
                $('#page-wrapper').toggleClass('sk-loading');
                Show_ConBudgetDetail(t);
                $('#page-wrapper').removeClass('sk-loading');
            }
        });
    }

}
function AddConBudgetDetail() {

    var bugtAmt = $('#txtConBudgetAmt').val();
    var effDate = $('#txtConEffectiveDate').val();
    var refNo = $('#txtConReferanceNo').val();
    var remarks = $('#txtConRemarks').val();

    if (bugtAmt == "" && bugtAmt <= 0) {
        $("#txtConBudgetAmt").attr("placeholder", "Enter amount"); $("#txtConBudgetAmt").addClass("Red"); $('#txtConBudgetAmt').focus(); return false;
    }
    if (effDate == "") {
        $("#txtConEffectiveDate").attr("placeholder", "Enter date"); $("#txtConEffectiveDate").addClass("Red"); $('#txtConEffectiveDate').focus(); return false;
    }
    //if (refNo == "") {
    //    $("#txtConReferanceNo").attr("placeholder", "Enter refNo."); $("#txtConReferanceNo").addClass("Red"); $('#txtConReferanceNo').focus(); return false;
    //}

    var html = "";

    var $this = $('#tbl_ConBudgetDetail .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"
        + "<td  class='bugtAmt' style='text-align:right;'>" + bugtAmt + "</td>"
        + "<td class='effDate' style='text-align:center;'>" + effDate + "</td>"
        + "<td class='refNo' style='text-align:center;'>" + refNo + "</td>"
        + "<td class='remarks'>" + remarks + "</td>"
        + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='DeleteConBudgetDetail(this);' /></td>"
    html + "</tr>";

    $parentTR.after(html);

    $("#txtConBudgetAmt").val(''); Set_ConDefaultDate(); $("#txtConReferanceNo").val(''); $("#txtConRemarks").val(''); $("#txtConBudgetAmt").focus();
    return false;
}
function ConSave() {

    var grdLen = $('#tbl_ConBudgetDetail tbody tr.myData').length; var ArrList = []; var master = {}; var TotalAmount = 0;
    if (grdLen > 0) {
        $('#tbl_ConBudgetDetail tbody tr.myData').each(function () {

            var bugtAmt = $(this).find('.bugtAmt').text();
            var effDate = $(this).find('.effDate').text();
            var refNo = $(this).find('.refNo').text();
            var remarks = $(this).find('.remarks').text();

            if (effDate != "") {
                var a = effDate.split('/');
                effDate = a[2] + "-" + a[1] + "-" + a[0];
            }

            TotalAmount = parseFloat(TotalAmount) + parseFloat(bugtAmt);

            ArrList.push({
                'BudgetID': $('#hdnConBudgetID').val(), 'ConcurranceAmount': bugtAmt, 'ConcurranceDate': effDate, 'ReferanceNo': refNo, 'Remarks': remarks
            });
        });
    }

    var BudgetAmt = $('#hdnConBudgetAmount').val(); //alert(BudgetAmt); alert(TotalAmount);
    if (parseFloat(TotalAmount) > parseFloat(BudgetAmt)) 
    {
        $('.dvErrorMsg').css('display', '');
        $('#errorMsg').text('Sorry ! Total Concurrance amount is greater than ' + BudgetAmt); return false;
    }
    else
    {
        $('.dvErrorMsg').css('display', 'none');
        $('#errorMsg').text('');
    }

    var E = "{BudgetID: '" + $('#hdnConBudgetID').val() + "', SectorID: '" + $('#ddlSector').val() + "',  Master: " + JSON.stringify(ArrList) + "}";
    //alert(E);
    //return false;

    $.ajax({
        url: '/Accounts_Form/BudgetMaster/InsertUpdate_Concurrance',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (t == "success" || t == "updated") {

                swal({
                    title: "Success",
                    text: 'Concurrance Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            $('#tab-1').removeClass('active');
                            $('#tab-2').removeClass('active');
                            $('#tab-3').addClass('active');
                            $("#dialog-ConBudgetdetail").modal("hide");
                            Get_ConParticulars();
                            return false;
                        }
                    });
            }
        }
    });
}



