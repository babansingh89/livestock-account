﻿var MNDOldCode = "";
var selectors = {
    'tree': '#treeview, #subtree',
    'input': '#input-search',
    'reset': '#btn-clear-search'
};
var lastPattern = '';



$(function () {
    var formhead = $('#Acheadtree');
    var sub = $('#Subledgertree');
    app.AccountRule();
 
    if ($(".pageName").val() === "AccountMasterPage") {
        app.populateAccount();
    } else if ($(".pageName").val() === "SubLedgerPage") {
    app.subledger();
    }
    app.validation();
    app.sublegerValidation();

    app.getAllbank();
    app.getState();
    app.nodeClick();
    app.getAllSector();



    $('#ddlbank').change(function () {
        var id = $(this).val();
        app.getbranchByBankId(id);
    });
    $('#ddlstate').change(function () {
        app.getDistrict($('#ddlstate').val());
    });
    app.IsChecked();
    $(document).on('click','input[type="checkbox"]',function () {
        //app.onlyonecheck();
        //app.IsChecked();
    });

    $(document).on('click', '.tt', function () {
        $('.tt').not(this).prop('checked', false); 
        app.IsChecked();
    })

    //$('#chkbank').prop('disabled', true);
    //$('#chkIsSubLedger').prop('disabled', true);
    //$('#ddlunit').prop('disabled', true);

    
    $('input:radio[name=optradio]').click(function () {
        app.radiochange(this);
    });
   
    $('#btnsubmit').click(function () {
        
        if (app.UnitType != "C") {
            if ($('#btnsubmit').text() == "Save") {

                if (!formhead.valid()) {
                    return false //stop submitting
                }

                app.save();
            } else {

                if (!formhead.valid()) {
                    return false //stop submitting
                }

                app.update();
            }
        } else {
            //alert("Insert and update are not allowed for " + app.SectorName + ".");
            swal({
                title: "Warning",
                text: "Insert and update are not allowed for " + app.SectorName + ".",
                type: "warning",
                confirmButtonColor: "#AEDEF4",
                confirmButtonText: "OK",
                closeOnConfirm: true,
            });
            return false;
        }

        
        app.populateAccount();
        app.reset();
        $('#txtacAuto').val('');
    });
    $('#btnsubsubmit').click(function () {
        if (!sub.valid()) {
            return false;
        }
        if ($('#btnsubsubmit').text() == "Save") {
            app.sublegerSave();
        } else {
            app.updateSubledger();
        }
            app.subledger();
            app.subReset();
            $('#txtAutoSub').val('');
        
    });
    app.accountHeadAutoComplete();
    
	app.test111(); //FOR ACCOUNT MASTER AUTOCOMPLETE
    app.test222(); //FOR SUBLEDGER MASTER AUTOCOMPLETE

    app.subledgerAutoComplete();
    app.subledgerAuto();
    app.EffectiveLedgerAuto();
    

    $('#txtparent').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtparent').val('');
            app.ParentId = "";
        }
        if (iKeyCode == 46) {
            $('#txtparent').val('');
            app.ParentId = "";
        }
    });
    $('#txtsubparent').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtsubparent').val(''); $('#hdnParentID').val('');
            app.subParentId = "";
        }
        if (iKeyCode == 46) {
            $('#txtsubparent').val(''); $('#hdnParentID').val('');
            app.subParentId = "";
        }
    });
    $('#txtIFSC').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 38 || iKeyCode != 40 || iKeyCode != 13 || iKeyCode != 9) {
            $('#txtIFSC').val('');
            $('#hdnIFSC').val('');
            $('#txtbank').val('');
            $('#txtbranch').val('');
            $('#txtMICR').val('');
            app.branchId = "";
        }
    });
    $('#btncancel').click(function () {
        app.reset();
    });
    $('#btnsubcancel').click(function () {
        app.subReset();
    });
    $('#ddlSector').change(function () {
        app.AccountRule();
    });
    $('#btnEffectAdd').click(function () {
        app.EffectAdd();
    });

    $('form input:text, input:radio,input:checkbox,select,button').bind("keydown", function (e) {
        var n = $('form input:text, input:radio,input:checkbox,select,button').length;
        //
        var c = $('form input:text, input:radio,input:checkbox,select,button').length;
        $('form input:text, input:radio,input:checkbox,button,select').each(function (index, element) {
            if ($(element).prop('readonly') || $(element).prop('disabled') || $(element).prop('type') == "button") {
            }
            
        });

        var keyCode = (window.event) ? e.which : e.keyCode;
        //
        if (keyCode == 13)
        { //Enter key
            e.preventDefault(); //Skip default behavior of the enter key
            var nextIndex = $('form input:text, input:radio,input:checkbox,select,button').not(":disabled").index(this) + 1;
            if (nextIndex < n) {
                //$('form input:text, input:radio,input:checkbox,select')[nextIndex].focus();
                $('form input:text, input:radio,input:checkbox,select,button').not(":disabled")[nextIndex].focus();
            }
      else
            {
                $('form input:text, input:radio,input:checkbox,select').not(":disabled")[nextIndex - 1].focus();
                //$('input:text')[nextIndex - 1].blur();
                $('#btnSubmit').click();
            }
        }
    });

    app.getAccConfigBySectorId();
   
    app.RightClick();
   $(selectors.input).on('keyup', search);
});

var app = {
    IELA: '',
    ParentId: '',
    subParentId: '',
    branchId: '',
    childCount: '',
    group: '',
    UnitType: '',
    SectorName: '',
    MNDOldCode:'',

    populateAccount: function () {
 $('#page-wrapper').toggleClass('sk-loading');
        $.ajax({
            url: '/Accounts_Form/AccountMaster/GetAccountHead',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
               
                app.treeview1(response.Data);
                  $('#page-wrapper').removeClass('sk-loading');
            },
            error: function () { }
        });
    },
    AccountRule: function () {
        var V = "{SectorID : '" + $("#ddlSector").val() + "'}";
        $.ajax({
            url: '/Accounts_Form/AccountMaster/AccountRule',
            data: V,
            type: 'POST',
            async:false,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (D) {
                var t = D.Data;
                if (t.length > 0 && D.Status == 200) {


                    MNDOldCode = app.MNDOldCode = t[0].MNDOldcode; 
                    if (MNDOldCode == "Y") { $("#panel-1").hide(); $("#panel-2").show(); $('#lblLedger').html('Detail'); }
                    else { $("#panel-1").show(); $("#panel-2").hide(); $('#lblLedger').html('Ledger'); }

                    //disable enable AccountHead field
                    //var maintainAccCode = t[0].MaintainAccCode;
                    //if (maintainAccCode == "Y" && MNDOldCode == "N") { $("#txtachead").prop("disabled", false);  }
                    //else if (maintainAccCode == "Y" && MNDOldCode == "Y") { $("#txtachead").prop("disabled", true); }
                    //else { $("#txtachead").prop("disabled", false); }
                }
            },
            error: function () { }
        });
    },
    subledger: function () {
$('#page-wrapper').toggleClass('sk-loading');
        $.ajax({
            url: '/Accounts_Form/AccountMaster/GetSubledger',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.subledgerPop(response.Data);
$('#page-wrapper').removeClass('sk-loading');
            },
            error: function () { }
        });
    },
    
    gethead: function (parent, items) {
        app.treeview(items, 0);
    },
    treeviewsasasa: function (list, parent) {

        $.each(list, function (i, element) {
            if (element.ParentAccountCode == '0') {
                var li = $('<li style="color:white;"><a href="' + this.Url + '">' + this.AccountDescription + '</a></li>');
                li.appendTo($('#root'));
            }
            else {

            }

        });

    },


    treeview1: function (data1) {
      
      
        $('#treeview').treeview({
            data: data1,
            levels: 1,
            onNodeSelected: function (event, data) { app.accClick(data);  }
        });
       
    },   //Treeview node click event
    subledgerPop: function (data) {
        $('#subtree').treeview({
            data: data,
            levels: 1,
            onNodeSelected: function (event, data) { app.subClick(data) }
        });
    }, //SubLedger Treeview node click event
    nodeClick: function () {
        $('.node-selected').click(function () {
            alert(this.id);
        });
    },
    parent: function (data) {
        app.child(data, 0);
    },
    child: function (data, parent) {
        var html = [];
        html.push('<ul>');

        html.push('</ul>');
    },
    getState: function () {
        $.ajax({
            url: '/Accounts_Form/AccountMaster/GetState',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.popState(response.Data)
            },
            error: function () { }
        });
    },
    popState: function (data) {
        var ctlr_ddllState = $('#ddlstate');
        ctlr_ddllState.empty();
        ctlr_ddllState.append('<option value=0>--select--</option');
        $(data).each(function (index, item) {
            //ctlr_ddlYear.append('<option value=' + item.Year + '>' + item.YearDesc + '</option>');
            ctlr_ddllState.append('<option value=' + item.StateId + '>' + item.State + '</option>');
        })
    },
    getDistrict: function (Id,callback) {
        $.ajax({
            url: '/Accounts_Form/AccountMaster/GetDistrict',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            //data: "{Id:" + Id + "}",
            data: "{ 'id': '" + Id + "'}",
            dataType: 'json',
            success: function (response) {
                app.popDistrict(response.Data)
                if (callback)
                {
                    callback(response.Data);
                    //callback();
                }
            },
            error: function () { }
        });
    },
    popDistrict: function (data) {
        var ctlr_ddllDistrict = $('#ddldistrict');
        ctlr_ddllDistrict.empty();
        ctlr_ddllDistrict.append('<option value=0>--select--</option');
        $(data).each(function (index, item) {
            //ctlr_ddlYear.append('<option value=' + item.Year + '>' + item.YearDesc + '</option>');
            ctlr_ddllDistrict.append('<option value=' + item.DistID + '>' + item.District + '</option>');
        })
    },
    getAllSector: function () {
        $.ajax({
            url: '/Accounts_Form/AccountMaster/GetAllSector',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.sectorPop(response.Data);
            },
            error: function () { }
        });
    },
    sectorPop: function (data) {
        var ctrl_unit = $('#ddlunit');
        ctrl_unit.empty();
        ctrl_unit.append('<option value>--Select--</option>');
        $.each(data, function (index, item) {
            ctrl_unit.append('<option value=' + item.SectorID + '>' + item.SectorName+'</option>');
        })
    },
    getAllbank: function () {
        $.ajax({
            url: '/Accounts_Form/AccountMaster/GetAllBank',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.bankPopulate(response.Data)
            },
            error: function () { }
        });
    },
    bankPopulate: function (data) {
        var ctlr_ddlBank = $('#ddlbank');
        ctlr_ddlBank.empty();
        ctlr_ddlBank.append('<option value=0>--select--</option');
        $(data).each(function (index, item) {
            //ctlr_ddlYear.append('<option value=' + item.Year + '>' + item.YearDesc + '</option>');
            ctlr_ddlBank.append('<option value=' + item.BankID + '>' + item.BankName + '</option>');
        })
    },

    getbranchByBankId: function (id,callback) {
        var bankId = id;
        $.ajax({
            url: '/Accounts_Form/AccountMaster/GetAllBranch',
            type: 'POST',
            data: "{bankId:" + bankId + "}",
            //contentType: false,
            //dataType: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                app.branchPopulate(response.Data);
                if (callback) {
                    callback(response.Data);
                    //callback();
                }
            },
            error: function () { }
        });
    },
    branchPopulate: function (data) {
        var ctlr_ddlBranch = $('#ddlbranch');
        ctlr_ddlBranch.empty();
        ctlr_ddlBranch.append('<option value=0>--select--</option>');
        $.each(data, function (i, item) {
            ctlr_ddlBranch.append('<option value=' + item.BranchID + '>' + item.Branch + '</option>');
        });
    },
    radiochange: function (ref) {
        //var value = $(this).val();
        var value = $(ref).val();
        //alert(value)
        if (value == "Ledger") {
            $('input[type=checkbox]').prop('disabled', false);
            //$('#txtachead').prop('readonly', false);
            $('#chkIsSubLedger').prop('readonly', true);
            $('#unit').prop('readonly', true);

            //$('#ddlPL').prop('readonly', true);
            $('#ddlPL').prop("disabled", true);
            $('#txtschedule').prop('readonly', true);
            $('#txtschedule').val('');
            $('#ddlPL').val(0);
            $('#ddlunit').prop('disabled', false);


        }
        else {
            $('input[type=checkbox]').prop('disabled', true);
            $('input[type=checkbox]').prop('checked', false);
            //$('#txtachead').prop('readonly', true);
            //$('#txtachead').val('');
            $('#chkIsSubLedger').prop('readonly', false);
            $('#unit').prop('readonly', false);

            $('#txtschedule').prop('readonly', false);
            $('#ddlPL').prop("disabled", false);

            $('#ddlunit').prop('disabled', true);
            $('#ddlunit').val('');
            app.IsChecked()
        }
    },
    IsChecked: function () {
        var ckk = $('input[type="checkbox"]').is(":checked");
        var ischk = $('input[type=checkbox]').prop('checked');
        var ddlbank = $('#ddlbank');
        var ddlbranch = $('#ddlbranch');
        var accno = $('#txtacno');
        if (ischk) {
            //$('#pizza_kind').prop('disabled', false);
            ddlbank.prop('disabled', false);
            ddlbranch.prop('disabled', false);
            accno.prop('disabled', false);
        } else {
            ddlbank.prop('disabled', true);
            ddlbranch.prop('disabled', 'disabled');
            accno.prop('disabled', 'disabled');
            ddlbank.val(0);
            ddlbranch.val(0);
            accno.val('');

        }
    },
    accClick: function (ref) {       //Treeview node click event
      
        var GLS = "";
        var Type = '';
        var group = $('#radiogroup');
        var ledger = $('#radionLedger');
        var major = $('#radioMajor');
        var minor = $('#radionMinor');

        var parent = $('#txtparent');
        var ischeck  = $('input[type=checkbox]').prop('checked');
        var ischk = $('#chkbank').prop('checked');
       
        app.ParentId = ref.AccountCode;
        var dataid = { "id": ref.AccountCode };

        $('#hdnAccountSearchID').val(ref.AccountCode);
        $('#txtacAuto').val('');

        app.autoChangeEvent(ref.AccountCode);

        //$.ajax({
        //    url: '/Accounts_Form/AccountMaster/GetAccuntHeadById',
        //    type: 'POST',
        //    data: JSON.stringify(dataid),
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "json",
        //    success: function (response) {
        //        Type = response.Data[0]["IELA"];
        //        app.IELA = response.Data[0]["IELA"];
        //        app.ParentId = response.Data[0]["AccountCode"];
        //    },
        //    error: function () { }
        //});

        //var IELA_GLS = ref.text.substr(0, 3).replace('(', '').replace(')', '');

        //if (!ischk) {
        //    if (IELA_GLS == "G" && GLS == "G" || IELA_GLS == "G" && GLS == "L" || IELA_GLS == "L" && GLS == "S") {
        //        //parent.val($(ref).text());
        //        parent.val(ref.text);
        //        app.ParentId = ref.AccountCode;
        //        $('#txtacdesc').focus();
        //        if ($('#txtacAuto').val() != '' && $('#hdnAccountSearchID').val() != '') {
        //            if ($('#hdnAccountSearchID').val() == app.ParentId) {
        //                alert('A Account can\'t be parent of itself.');
        //                parent.val('');
        //                app.ParentId = '';
        //            }
        //        }
        //    } else {
        //        parent.val('');
        //        app.ParentId = '';
        //        alert('Sorry ! You can not select this.');
        //    }
        //} else {
        //    if (app.IELA != "A") {
        //        var load = "isBank";
        //        alert('Sorry ! You can not select this. Please Select Asset Because you have selected Bank.');
        //    }
        //    if (IELA_GLS == "G" && GLS == "G" || IELA_GLS == "G" && GLS == "L" || IELA_GLS == "L" && GLS == "S") {
        //        //parent.val($(ref).text());
        //        parent.val(ref.text);
        //        app.ParentId = ref.AccountCode;
        //        if ($('#txtacAuto').val() != '' && $('#hdnAccountSearchID').val() != '') {
        //            if ($('#hdnAccountSearchID').val() == app.ParentId) {
        //                alert('A Account can\'t be parent of itself.');
        //                parent.val('');
        //                app.ParentId = '';
        //            }
        //        }
        //    } else {
        //        parent.val('');
        //        app.ParentId = '';
        //        alert('Sorry ! You can not select this.');
        //    }
        //}

    },
    subClick: function (ref) {
       // $('#txtpayee').focus();
        app.subParentId = ref.AccountCode; 
        $('#hdnParentID').val(ref.AccountCode);
        //var SubParentName = ref.text;
        //var txtparent = $('#txtsubparent');
        //txtparent.val(SubParentName);

        //var dataid = { "id": ref.AccountCode }

        app.subledgerAutopop(ref.AccountCode);

        //$.ajax({
        //    url: '/Accounts_Form/AccountMaster/GetAccuntHeadById',
        //    type: 'POST',
        //    data: JSON.stringify(dataid),
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "json",
        //    success: function (response) {
        //        app.ParentId = response.Data[0]["AccountCode"];
        //        app.IELA = response.Data[0]["IELA"];
        //    },
        //    error: function () { }
        //});
    },
    save: function () {
        var group = $('#radiogroup');
        var ledger = $('#radionLedger');
        var major = $('#radioMajor');
        var minor = $('#radionMinor');

        //var ischk = $('input[type=checkbox]').prop('checked');
        var ischk = $('#chkbank').prop('checked');
        var branchId = ($('#ddlbranch option:selected').val() != null && $('#ddlbranch').val() != '') ? $('#ddlbranch').val():0;
        var acno = $('#txtacno').val();
        
        var achead = $('#txtachead').val();

        var GLS = '';
        var ISBank = '';
        //var IELA = '';

        var parent = $('#txtparent').val();
        var acDesc = $('#txtacdesc').val();

        if (group.prop('checked')) {
            GLS = 'G'
        } else if (ledger.prop('checked')) {
            GLS = 'L';
        } else if (major.prop('checked')) {
            GLS = 'N';
        } else if (minor.prop('checked')) {
            GLS = 'L';
        }

        if (ischk) {
            ISBank = "B";
            app.IELA = "A";
        }
        var IsSubLedger = $('#chkIsSubLedger').prop('checked') ? "Y" : null
        var PL = $('#ddlPL').val();
        var schedule = $('#txtschedule').val();
        //var centerid = $('#unit').prop('checked');
        var centerid = $('#ddlunit').val();
        
        var obj = { "AccountID": null, "parent": app.ParentId, "acDesc": acDesc, "GLS": GLS, "IELA": app.IELA, "IsSubLedger": IsSubLedger, "branchId": branchId, "acno": acno, "ISBank": ISBank, "achead": achead, "centerid": centerid, "schedule": schedule, "PL": PL}
      
        $.ajax({
            url: '/Accounts_Form/AccountMaster/SaveAccountHead',
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var t = response.Data;
                var msgType = "", titles = "";
                if (t == "1" || t == "2") { msgType = "success"; titles = "Success"; }
                else
                { msgType = "warning"; titles = "Warning"; }


                swal({
                    title: titles,
                    text: response.Message,
                    type: msgType,
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            app.populateAccount();
                            app.reset();
                        }
                    });
                //alert(response.Message);
                //app.populateAccount();
            },
            error: function () { }
        });
    },
    update: function () {
        var group = $('#radiogroup');
        var ledger = $('#radionLedger');
        var major = $('#radioMajor');
        var minor = $('#radionMinor');

        var id = $('#hdnAccountSearchID').val();
        var acDesc=$('#txtacdesc').val();
        var parentname = $('#txtparent').val();
        var achead = $('#txtachead').val();
        var branchId = ($('#ddlbranch option:selected').val() != null && $('#ddlbranch').val() != '') ? $('#ddlbranch').val() : 0;
        var acno = $('#txtacno').val();

        var IsSubLedger = $('#chkIsSubLedger').prop('checked') ? "Y" : null
        //var groupLedger = $('input:radio[name=optradio]:checked').val().substr(0, 1);
        if (group.prop('checked')) {
            GLS = 'G'
        } else if (ledger.prop('checked')) {
            GLS = 'L';
        } else if (major.prop('checked')) {
            GLS = 'N';
        } else if (minor.prop('checked')) {
            GLS = 'L';
        }

        var PGroup = $('#txtparent').val().substr(0, 3).replace('(', '').replace(')', '');

        var PL = $('#ddlPL').val();
        var schedule = $('#txtschedule').val();
        //var sec = $('#unit').prop('checked');
        var centerid = $('#ddlunit').val();

        if ($('#chkbank').prop('checked')) {
            ISBank = "B";
            app.IELA = "A";
        }
        else {
            ISBank = null;
        }

        var obj = { "AccountID": id, "parent": app.ParentId, "acDesc": acDesc, "GLS": GLS, "IELA": app.IELA, "IsSubLedger": IsSubLedger, "branchId": branchId, "acno": acno, "ISBank": ISBank, "achead": achead, "centerid": centerid, "schedule": schedule, "PL": PL }

        $.ajax({
            url: '/Accounts_Form/AccountMaster/SaveAccountHead',
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var t = response.Data;
                var msgType = "", titles = "";
                if (t == "1" || t == "2") { msgType = "success"; titles = "Success"; }
                else
                { msgType = "warning"; titles = "Warning"; }

                swal({
                    title: titles,
                    text: response.Message,
                    type: msgType,
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            app.populateAccount();
                            app.reset();
                        }
                    });
                //alert(response.Message);
                //app.populateAccount();
            },
            error: function () { }
        });

    },
    validation: function () {
        $.validator.addMethod('Root', function (value, element) {
            //return parseInt(value) % 5 == 0
            return value !='(G)NA(0)' && value != 'NA'
        });

        $('#Acheadtree').validate({
            rules: {
                txtparent: {
                    required: true,
                    Root: true
                }, 
                txtacdesc: "required",
            },
            messages: {
                txtparent: {
                    required: "Parent Account Should be not Blank !!",
                    Root:"You cant update the Top Root."
                } ,
                txtacdesc: "Enter Account Description"
            }
        });
    },
    accountHeadAutoComplete: function () {
        $('#txtparent').autocomplete({
            source: function (request, response) {

                var GLS = "";
                var group = $('#radiogroup');
                var ledger = $('#radionLedger');
                var major = $('#radioMajor');
                var minor = $('#radionMinor');

                if (group.prop('checked')) {
                    GLS = 'G'
                } else if (ledger.prop('checked')) {
                    GLS =  app.MNDOldCode == "Y" ? "N" : 'G';
                } else if (major.prop('checked')) {
                    GLS = 'M';
                } else if (minor.prop('checked')) {
                    GLS = 'M';
                }

                var S = "{Desc:'" + $('#txtparent').val() + "', MND:'" + GLS + "'}"; //alert(S);
                $.ajax({
                    url: '/Accounts_Form/AccountMaster/Account_Description',
                    type: 'POST',
                    data: S,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (serverResponse) {
                        var AutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                AutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });

                            response(AutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                app.ParentId = i.item.AccountCode;
            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
        });
    },

    test111: function () {
        $("#txtacAuto").autocomplete({
            source: function (request, response) {
                
                $.ajax({
                    type: "POST",
                    url: '/Accounts_Form/AccountMaster/Load_AccountAutoComplete',
                    data: "{ AccountDescription: '" + $('#txtacAuto').val() + "',  AccountORSubLedger: '" + "A" + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (serverResponse) {
                        var ledgerAutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                ledgerAutoComplete.push({
                                    label: item.AccountDescription,
                                    Val: item.AccountCode
                                });
                            });
                            response(ledgerAutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                $("#hdnAccountSearchID").val(i.item.Val);
                app.autoChangeEvent($('#hdnAccountSearchID').val());
                if ($('#hdnAccountSearchID').val() != '') {
                    $('#btnsubmit').text('Update');
                }
                else {
                    $('#btnsubmit').text('Save');
                }

            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
        });

    },
    test222: function () {
        $("#txtAutoSub").autocomplete({
            source: function (request, response) {

                $.ajax({
                    type: "POST",
                    url: '/Accounts_Form/AccountMaster/Load_AccountAutoComplete',
                    data: "{ AccountDescription: '" + $('#txtAutoSub').val() + "',  AccountORSubLedger: '" + "S" + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (serverResponse) {
                        var ledgerAutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                ledgerAutoComplete.push({
                                    label: item.AccountDescription,
                                    Val: item.AccountCode
                                });
                            });
                            response(ledgerAutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                $("#hdnSubLedgerSearchId").val(i.item.Val);
                app.subledgerAutopop($('#hdnSubLedgerSearchId').val());
                if ($('#hdnSubLedgerSearchId').val() != '') {
                    $('#btnsubsubmit').text('Update');
                }
                else {
                    $('#btnsubsubmit').text('Save');
                }

            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
        });

    },
    autoChangeEvent: function (id) {
        $.ajax({
            url: '/Accounts_Form/AccountMaster/GetAccuntHeadById',
            type: 'POST',
            data: "{ 'id': '" + id + "', SectorID: '" + $('#ddlSector').val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                var a = response.Data.Table;
                var b = response.Data.Table1;

                app.ParentId = a[0]["ParentAccountCode"];
                app.IELA = a[0]["IELA"];

                if (app.ParentId != 0) {

                    var group = $('#radiogroup'); var ledger = $('#radionLedger');var major = $('#radioMajor');var minor = $('#radionMinor');
                    group.prop('disabled', false); ledger.prop('disabled', false); major.prop('disabled', false); minor.prop('disabled', false);

                    $('#hdnAccountSearchID').val(a[0]["AccountCode"]);
                    $('#txtacdesc').val(a[0]["AccountName"]);
                    $('#txtparent').val(a[0]["ParentName"] == "NA" ? a[0]["AccountName"] : a[0]["ParentName"]);

                    $('#txtachead').val(a[0]["OldAccountHead"]);
                    var bankid = a[0]["BankID"];
                    $('#ddlbank').prop('selectedIndex', bankid);
                    var branchid = a[0]["BranchID"];
                    $('#ddlbranch').prop('selectedIndex', branchid);

                    app.getbranchByBankId(a[0]["BankID"], function (bankid) {
                        $('#ddlbranch').val(a[0]["BranchID"]);
                    });


                    $('#txtacno').val(a[0]["AccountNumber"]);
                    var isSubledger = a[0]["SubLedger"];
                    if (a[0]["SubLedger"] == "Y") {
                        $("#chkIsSubLedger").prop("checked", true);
                    }
                    else {
                        $("#chkIsSubLedger").prop("checked", false);
                    }

                    if (a[0]["BankCash"] == "B") {
                        $('#chkbank').prop('checked', true);
                    } else {
                        $('#chkbank').prop('checked', false);
                    }

                    app.childCount = a[0]["ChildCount"];

                    if (a[0]["GroupLedger"] == "G") {
                        $('#radiogroup').prop('checked', true);
                    } else if (a[0]["GroupLedger"] == "L") {
                        $('#radionLedger').prop('checked', true)
                    }
                    else if (a[0]["GroupLedger"] == "M") {
                        $('#radioMajor').prop('checked', true)
                    }
                    else if (a[0]["GroupLedger"] == "N") {
                        $('#radionMinor').prop('checked', true)
                    }

                    app.group = a[0]["GroupLedger"];
                    $('#txtschedule').val(a[0]["Schedule"]);
                    if (a[0]["PLDirectIndirect"] == 0 || a[0]["PLDirectIndirect"] == null) {
                        $('#ddlPL').val(0);
                    } else {
                        $('#ddlPL').val(a[0]["PLDirectIndirect"]);
                    }

                    $('#ddlunit').val(a[0]["centerid"]);

                    $('#btnsubmit').text('Update');
                }
                else {
                    $('#radiogroup').prop('checked', false); $('#radionLedger').prop('checked', false);
                    $('#radioMajor').prop('checked', false); $('#radionMinor').prop('checked', false);
                    app.reset();
                }
            },
            error: function () { }
        });
    },
    reset: function () {
        app.ParentId = '',
        $('#txtacdesc').val('');
        $('#txtparent').val('');
        $('#hdnAccountSearchID').val('');
        //$('#txtacAuto').val('');
        $('#txtachead').val('');
        $('#ddlbank').val('0');
        $('#ddlbranch').val('0');
        //$('#ddlbranch')[0].selectedIndex = 0;
        //$("select#elem").prop('selectedIndex', 0);
        $('#txtacno').val('');
        $('#chkbank').prop('checked', false);
        $('#chkIsSubLedger').prop('checked', false);
        $('#unit').prop('checked', false);
        $('#txtschedule').val('');
        $('#ddlPL').val(0);
        $('#ddlunit').val('');
        $('#btnsubmit').text('Save');
    },

    subledgerAutoComplete: function () {
        var E = "{IFSC: '" + $("#txtIFSC").val() + "'}";
        $("#txtIFSC").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/Accounts_Form/AccountMaster/SubledgerAutoComplete',
                    type: 'POST',
                    data: "{ 'IFSC': '" + $('#txtIFSC').val() + "'}",
                    dataType:'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        var t = data.Data;
                       
                        var DataAutoComplete = [];
                        if (t.length > 0) {
                            $.each(t, function (index, item) {
                                DataAutoComplete.push({
                                    label: item.split('|')[0],
                                    Val: item.split('|')[1]
                                });
                            });
                            response(DataAutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                $("#hdnIFSC").val(i.item.Val);
                app.SubledgerAutoFill($("#hdnIFSC").val());
            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
        });

    },
    SubledgerAutoFill: function (id) {
        $.ajax({
            url: '/Accounts_Form/AccountMaster/getSubledgerFillByBranchID',
            type: 'POST',
            data: "{ 'id': '" + id + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                //$('#txtacdesc').val(response.Data[0]["AccountDescription"]);
                $('#txtbank').val(response.Data[0]["BankName"]);
                $('#txtbranch').val(response.Data[0]["Branch"]);
                $('#hdnIFSC').val(response.Data[0]["BranchID"]);
                $('#txtMICR').val(response.Data[0]["MICR"]);
                app.branchId = response.Data[0]["BranchID"];
                
            },
            error: function () { }
        });
    },
    subledgerAuto: function () {
        $('#txtsubparent').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/Accounts_Form/AccountMaster/Account_Description',
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'Desc':'" + $('#txtsubparent').val() + "', MND: '"+ 'L' +"'}",
                    dataType: 'json',
                    success: function (serverResponse) {
                       
                        var SubledgerAutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                SubledgerAutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });
                          
                            response(SubledgerAutoComplete);
                        }
                        
                    },
                    error: function () { }
                });
            },
            select: function (e, i) {
                app.subParentId = i.item.AccountCode;
				 $('#hdnParentID').val(i.item.AccountCode);
                //$('#hdnSubLedgerSearchId').val(i.item.AccountCode);
                //app.subledgerAutopop($('#hdnSubLedgerSearchId').val());
                //if ($('#hdnSubLedgerSearchId').val() != '') {
                //    $('#btnsubsubmit').text('Update');
                //}
                //else {
                //    $('#btnsubsubmit').text('Submit');
                //}
            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()))
        });
    },
    EffectiveLedgerAuto: function () {
        $('#txtEfftGL').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/Accounts_Form/AccountMaster/Account_Description',
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'Desc':'" + $('#txtEfftGL').val() + "', MND: '" + 'L' + "'}",
                    dataType: 'json',
                    success: function (serverResponse) {

                        var SubledgerAutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                SubledgerAutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });

                            response(SubledgerAutoComplete);
                        }

                    },
                    error: function () { }
                });
            },
            select: function (e, i) {
                $('#hdnEffectAdd').val(i.item.AccountCode);
            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()))
        });
    },
    subledgerAutopop: function (ref) {
        $.ajax({
            url: '/Accounts_Form/AccountMaster/GetAccuntHeadById',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: "{ 'id': '" + ref + "', SectorID: '" + $('#ddlSector').val() + "'}",
            dataType: 'json',
            success: function (response) {
               
                var a = response.Data.Table; 
                var b = response.Data.Table1; 

                //alert(a[0]["AccountCode"]);
                app.subParentId = a[0]["ParentAccountCode"];
                app.branchId = a[0]["BranchID"];

                if (a[0]["SubLedger"] != 'Y') {
                    $('#hdnSubLedgerSearchId').val(a[0]["AccountCode"]);
                    $('#txtsubparent').val(a[0]["ParentName"]);$('#hdnParentID').val(a[0]["ParentAccountCode"]);
                    $('#txtpayee').val(a[0]["AccountName"]);
                    $('#txtgst').val(a[0]["GST"]);
                    $('#txtpan').val(a[0]["PAN"]);
                    $('#txtmob').val(a[0]["ContactNo"]);
                    $('#payeeContactPerson').val(a[0]["PayeeContactPerson"]);
                    $('#txtbank').val(a[0]["BankName"]);
                    $('#txtbranch').val(a[0]["Branch"]);
                    $('#txtMICR').val(a[0]["MICR"]);
                    $('#txtIFSC').val(a[0]["IFSC"]);
                    $('#txtaddress').val(a[0]["Address"]);
                    $('#ddlaccounttype').val(a[0]["AccountTypeID"]);
                    $('#ddlstate').val(a[0]["State"]);
                    app.IELA = a[0]["IELA"];
                    app.getDistrict(a[0]["State"], function (distId) {
                        $('#ddldistrict').val(a[0]["DistrictId"]);
                    });
                    $('#txtpin').val(a[0]["PinNo"]);

                    $('#btnsubsubmit').text('Update');
                   
                    $('#tbl tr.myData').remove();
                    if(b.length >0)
                    {
                        var html = ""
                        var $this = $('#tbl .test_0');
                        $parentTR = $this.closest('tr');
                       
                        for(var i =0; i<b.length; i++)
                        {
                            html += "<tr class='myData' >"
                                + "<td style='display:none;' class='clsAccCode' chk-data='" + b[i].ParentAccountCode + "' >" + b[i].ParentAccountCode + "</td>"
                                + "<td>" + b[i].AccountDescription + "</td>"
                                + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='app.DeleteInstDetail(this);' /></td>"
                            html + "</tr>";
                        }
                        $parentTR.after(html);
                    }
                }
            },
            error: function () { }
        });
    },
   
    updateSubledger: function () {
        var subledgerId = $('#hdnSubLedgerSearchId').val();
        app.subParentId;
        var accDesc = $('#txtpayee').val();
        var gst=$('#txtgst').val();
        var pan=$('#txtpan').val();
        var mob=$('#txtmob').val();
        var payeeContactPerson=$('#payeeContactPerson').val();
        var address=$('#txtaddress').val();
        var accountType = $('#ddlaccounttype').val();
        var groupLedger = 'S';
        var branch = app.branchId;
        var state = $('#ddlstate').val();
        var district = $('#ddldistrict').val();
        var PIN = $('#txtpin').val();

        var grdLen = $('#tbl tr.myData').length;  var ArrList = [];
        if (grdLen > 0)
        {
           
            $('#tbl tr.myData').each(function () {
                var accCode = $(this).find('.clsAccCode').text();
                ArrList.push({ 'AccountCode': $('#hdnSubLedgerSearchId').val(), 'ParentAccountCode': accCode });
            });
           
        }

        var EffectedGL = JSON.stringify(ArrList);
        var obj = "{ AccountID: '" + subledgerId + "', Parent: '" + app.subParentId + "', Payee: '" + accDesc + "', GST: '" + gst + "', Address: '" + address + "'," +
                  "PAN: '" + pan + "', GroupLedger: '" + groupLedger + "', IELA: '" + app.IELA + "', Mob: '" + mob + "', PayeeContactPerson: '" + payeeContactPerson + "'," +
                  "branch: '" + branch + "', AccountType: '" + accountType + "', state: '" + state + "', district: '" + district + "', PIN: '" + PIN + "', EffectedGL: " + EffectedGL + "}";

      
        $.ajax({
            url: '/Accounts_Form/AccountMaster/SaveSubledger',
            type: 'POST',
            data: obj,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
             
                var t = response.Data;
                var msgType = "", titles = "";
                if (t == "1" || t == "2") { msgType = "success"; titles = "Success"; }
                else
                { msgType = "warning"; titles = "Warning"; }


                swal({
                    title: titles,
                    text: response.Message,
                    type: msgType,
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            app.subledger();
                            app.subReset();
                        }
                    });
            },
            error: function (isConfirm) {
                if (isConfirm) {
                    app.subledger();
                }}
        });

    },
    sublegerValidation: function () {
        $.validator.addMethod('ddlaccounttype', function (value, element) {
            return value != "0";
        });
        $.validator.addMethod('txtsubparent', function (value, element) {
            return value.substr(0, 3).replace('(', '').replace(')', '') != "S";
        });
        $('#Subledgertree').validate({
		  ignore: "",
            rules: {
                txtpayee: {
                    required: true
                },
                hdnParentID: {
                    required: true
                }
            },
            messages: {
                //txtpayee: {
                    //required:"Plz Enter Payee Name"
                //},
                txtsubparent: {
                    txtsubparent: "You can't create Sub Ledger within Sub Ledger."
                }
            }
        });
    },
    sublegerSave: function () {
        var parent = $('#txtsubparent');
        var payee = $('#txtpayee').val();
        var gst = $('#txtgst').val();
        var pan = $('#txtpan').val();
        var mob = $('#txtmob').val();
        var payeeContactPerson = $('#payeeContactPerson').val();
        var branch = $('#hdnIFSC').val();
        var accountType = $('#ddlaccounttype option:selected').val()
        var groupLedger = 'S';
        var address = $('#txtaddress').val();
        var state = $('#ddlstate').val();
        var district = $('#ddldistrict').val();
        var PIN = $('#txtpin').val();
         
        var obj = { "AccountID": null, "Parent": app.subParentId, "Payee": payee, "GST": gst, "Address": address, "PAN": pan, "GroupLedger": groupLedger, "IELA": app.IELA, "Mob": mob, "PayeeContactPerson": payeeContactPerson, "branch": branch, "AccountType": accountType, "state": state, "district": district, "PIN": PIN }

        $.ajax({
            url: '/Accounts_Form/AccountMaster/SaveSubledger',
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                
                var t = response.Data;
                var msgType = "", titles = "";
                if (t == "1" || t == "2") { msgType = "success"; titles = "Success"; }
                else
                { msgType = "warning"; titles = "Warning"; }


                swal({
                    title: titles,
                    text: response.Message,
                    type: msgType,
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            app.subledger();
                            app.subReset();
                        }
                    });
               
            },
            error: function () { }
        });
    },
    subReset: function () {
        app.subParentId = '';
         $('#txtsubparent').val('');
         $('#txtpayee').val('');
         $('#txtgst').val('');
         $('#txtpan').val('');
         $('#txtmob').val('');
         $('#payeeContactPerson').val('');
         $('#hdnIFSC').val('');
         $('#txtIFSC').val('');
         $('#ddlaccounttype').val('0')
        var groupLedger = 'S';
        var address = $('#txtaddress').val('');
        $('#txtbank').val('');
        $('#txtbranch').val('');
        $('#txtMICR').val('');
        $('#hdnParentID').val('');
        $('#ddlstate').val(0);
        $('#ddldistrict').val(0);
        $('#txtpin').val('');
        $('#btnsubsubmit').text('Save');
        $('#tbl tr.myData').remove();
    },
    onlyonecheck: function () {
       
        //$(this).siblings('input:checkbox').prop('checked', false);
        //$(this).siblings('.tt').prop('checked', false);
        //$('input[type="checkbox"]').not(this).prop('checked', false); 
        //$('.tt').not(this).prop('checked', false);
        //$('input[type="checkbox"]').not(this).prop('checked', false); 

        //$('.tt').not(this).prop('checked', false);  


        
    },
    getAccConfigBySectorId: function () {

        var E = "{SectorID : '" + $('#ddlSector').val() + "'}";
        $.ajax({
            url: '/Accounts_Form/AccountMaster/getAccConfig',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: E,
            success: function (response) {

                app.UnitType = response.Data[0]["UnitType"];
                app.SectorName = response.Data[0]["SectorName"];
               
                if (app.UnitType != "C") {
                    $('#txtachead').prop('readonly', false);
                    $('#btnsubmit').prop('disabled', false);
                }
                else {
                    $('#txtachead').prop('readonly', true);
                    $('#btnsubmit').prop('disabled', true);
                }

            },
            error: function () { }
        });
    },
    EffectAdd: function()
    {
        if ($("#hdnEffectAdd").val() == "") { $("#txtEfftGL").focus(); return false; }
        var chkAccCode = $("#hdnEffectAdd").val();
       

        var l = $('#tbl tr.myData').find("td[chk-data='" + chkAccCode + "']").length; 
       
        if (l > 0) {
            alert('Sorry ! This Account Description is Already Available.'); $("#txtEfftGL").val(''); $("#hdnEffectAdd").val(''); return false;
        }

        var html = ""

        var $this = $('#tbl .test_0');
        $parentTR = $this.closest('tr');

        html += "<tr class='myData' >"
            + "<td style='display:none;' class='clsAccCode' chk-data='" + $("#hdnEffectAdd").val() + "' >" + $("#hdnEffectAdd").val() + "</td>"
            + "<td>" + $("#txtEfftGL").val() + "</td>"
            + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='app.DeleteInstDetail(this);' /></td>"
        html + "</tr>";

        $parentTR.after(html);

        $("#txtEfftGL").val(''); $("#hdnEffectAdd").val('');
    },
    DeleteInstDetail: function(ID)
    {
        $(ID).closest('tr').remove();
    },

    setAccParetnOnly:function(AccCode, GL)
    {
        $.ajax({
            url: '/Accounts_Form/AccountMaster/GetAccuntHeadById',
            type: 'POST',
            data: "{ 'id': '" + AccCode + "', SectorID: '" + $('#ddlSector').val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                var a = response.Data.Table;
                var b = response.Data.Table1;

                app.ParentId = a[0]["AccountCode"];
                app.IELA = a[0]["IELA"];
                var groupLedger = a[0]["GroupLedger"];

                if (groupLedger == "L")
                {
                    alert('Sorry !! You can not Create Account Under Ledger/Detail.'); return false;
                }

                $('#hdnAccountSearchID').val(a[0]["AccountCode"]);
                $('#txtparent').val(a[0]["AccountDescription"]);

                if (GL == "G") {
                    $('#radiogroup').prop('checked', true);
                } else if (GL == "L") {
                    $('#radionLedger').prop('checked', true)
                }
                else if (GL == "O") {
                    if (a[0]["GroupLedger"] == "G") {
                        $('#radiogroup').prop('checked', true);
                    } else if (a[0]["GroupLedger"] == "L") {
                        $('#radionLedger').prop('checked', true)
                    }
                    else if (a[0]["GroupLedger"] == "M") {
                        $('#radioMajor').prop('checked', true)
                    }
                    else if (a[0]["GroupLedger"] == "N") {
                        $('#radionMinor').prop('checked', true)
                    }
                }
            },
            error: function () { }
        });
    },
    setSubParetnOnly: function (AccCode, GL) {
       
        $.ajax({
            url: '/Accounts_Form/AccountMaster/GetAccuntHeadById',
            type: 'POST',
            data: "{ 'id': '" + AccCode + "', SectorID: '" + $('#ddlSector').val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                var a = response.Data.Table;
                var b = response.Data.Table1;

                app.subParentId = a[0]["AccountCode"];
                app.IELA = a[0]["IELA"];
                var groupLedger = a[0]["GroupLedger"];

                if (groupLedger == "S") {
                    alert('Sorry !! You can not Create Account Under SubLedger.'); return false;
                }
               

                $('#hdnParentID').val(AccCode);
                $('#hdnSubLedgerSearchId').val(a[0]["AccountCode"]);
                $('#txtsubparent').val(a[0]["AccountDescription"]);

            }
        });
    },

    DeleteAccount: function (AccCode) {
        $.ajax({
            url: '/Accounts_Form/AccountMaster/DeleteAccountCode',
            type: 'POST',
            data: "{ 'AccCode': '" + AccCode + "', SectorID: '" + $('#ddlSector').val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                var a = response.Data;
                if(a == "Yes")
                {
                    alert("Account Description is Deleted Successfully !!"); 
                    if ($(".pageName").val() === "SubLedgerPage") {
                       // app.subledger(); app.subReset();
                        location.reload();
                    }
                    else {
                        //app.populateAccount();app.reset();
                        location.reload();
                    }
                }
                else if(a == "No")
                {
                    alert("Sorry ! You can not Delete Account Description."); return false;
                }
                else
                {
                    alert("There is Some Problem !!"); return false;
                }
            },
            error: function () { }
        });
    },
    RightClick: function () {
       
        var $contextMenu = app.MNDOldCode == "Y" ? $("#contextMenuMND") : $("#contextMenu");
        $("body").on("contextmenu contextMenuMND", function (e) {
            return false;
        });

        var t = "";
        $(document).on('mousedown', ".right-clicked", function (e) {
            if (e.which == 3) {

                var ths = $(this); 
                ths.removeClass('selected');
                ths.addClass('selected');
                t = ths.attr('id');
                var fordata = $("#hdnfor").val(); 

                if (fordata == "account") {
                    $contextMenu.css({
                        display: "block",
                        left: e.pageX - 75,
                        top: e.pageY - 80
                    });
                }
                if (fordata == "subledger") {
                    $contextMenu.css({
                        display: "block",
                        left: e.pageX - 60,
                        top: e.pageY - 20
                    });
                }
            }
            return false;
        });

        //Add
        $contextMenu.on("click", ".lnkAdd", function () {
            var ths = $(this);
            var acc_sub = ths.attr('acc-sub'); 
            var acc_gl = ths.attr('acc-gl'); 
            if(acc_sub == "A") //Account Form
            {
                 app.reset();
                 app.setAccParetnOnly(t, acc_gl);
            }
            else               //SubLeger Form
            {
                app.subReset();
                app.setSubParetnOnly(t, acc_gl);
            }
        });
        //Delete
        $contextMenu.on("click", ".lnkDelete", function () {
            var ths = $(this);
            var acc_sub = ths.attr('acc-sub');
            var res = confirm('Are You Sure want to Delete this Account Description.');
            if(res == true)
                app.DeleteAccount(t);

        });

        $contextMenu.mouseleave(function () {
            $contextMenu.css({
                display: "none"
            });
            return false;
        });

    }



}

function Blank_Auto()
{
    $('#txtparent').val('');
    app.ParentId = "";
    $('#txtparent').focus();
}

// ========================================================================== TreeView Search ==========================================================================
function reset(tree) {
    tree.collapseAll();
    tree.enableAll();
}

function collectUnrelated(nodes) {
    var unrelated = [];
    $.each(nodes, function (i, n) {
        if (!n.searchResult && !n.state.expanded) { // no hit, no parent
            unrelated.push(n.nodeId);
        }
        if (!n.searchResult && n.nodes) { // recurse for non-result children
            $.merge(unrelated, collectUnrelated(n.nodes));
        }
    });
    return unrelated;
}

var search = function (e) {
   
    var pattern = $(selectors.input).val();
    if (pattern === lastPattern) {
        return;
    }
    lastPattern = pattern;
    var tree = $(selectors.tree).treeview(true);
    reset(tree);
    if (pattern.length < 3) { // avoid heavy operation
        tree.clearSearch();
    } else {
        tree.search(pattern);
        var roots = tree.getSiblings(0);
        roots.push(tree.getNode(0));
        var unrelated = collectUnrelated(roots);
        tree.disableNode(unrelated, { silent: true });
    }
};

$(selectors.reset).on('click', function (e) {
    $(selectors.input).val('');
    var tree = $(selectors.tree).treeview(true);
    reset(tree);
    tree.clearSearch();
});

   