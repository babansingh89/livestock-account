﻿var vchslr = 0;
var curcellGL;
var curCellSUB;
var Modifyflg = 'Add';
var ModifyflgInst = 'Add';
var ModifyflgSL = 'Add';
var glvrno = '';
var GlobalSTATUS = '';
var ss = [];
var isEdit = true;
var MaintainCostCenter="";
var MaintainDeptCode = "";
var MaintainAccCode = "";
var Maintain_ItemSubLedger = "";
var MAIN = {};

$(function () {
    $("#txtNarrationval").bind('paste', function () {
        setTimeout(function () {
            //get the value of the input text
            var data = $('#txtNarrationval').val();
            //replace the special characters to '' 
            var dataFull = data.replace(/'/gi, '');
            //set the new value of the input text without special characters
            $('#txtNarrationval').val(dataFull);
        });
    });
});

$(document).ready(function () {
    
    $('#txtNarrationval').keyup(function () {
        var yourInput = $(this).val();
        re = /'/gi;
        var isSplChar = re.test(yourInput);
        if (isSplChar) {
            var no_spl_char = yourInput.replace(/'/gi, '');
            $(this).val(no_spl_char);
        }
    });
    
    $('#txtVoucherDate').mask("99/99/9999", { placeholder: "_" });
    $('#ddlVchType').val('2');
    Set_Voucher_Date();
   

    window.voucherInfo = new MAIN.VoucherInfo();
    window.voucherInfo.init();
    
    $('.modal').on('shown.bs.modal', function () {
        $(this).find('[autofocus]').focus();
    });

    //======================================== ACCOUNT DESCRIPTION AUTO-COMPLETE =============================================
    $("#lblAccDesc_0").autocomplete({

        source: function (request, response) {

            var Type = "";
            var Value = $.trim($("#lblAccDesc_0").val());
            var isNumorStr = $.isNumeric(Value);

            if (isNumorStr)
                Type = "C";  //FOR CODE
            else
                Type = "D";  //FOR DESCRIPTION

            var E = "{SearchValue: '" + Value + "', SearchType: '" + Type + "', VoucherTypeID: '" + $("#ddlVchType").val() + "', SectorID: '" + $("#ddlSector").val() + "', DRCR: '" + $("#ddlDRCR").val() + "'}";
            $.ajax({
                type: "POST",
                url: "/Accounts_Form/VoucherMaster/Search_AccountDetails",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.GL_NAME,
                                GL_ID: item.GL_ID,
                                GL_TYPE: item.GL_TYPE,
                                OLD_SL_ID: item.OLD_SL_ID,
                                SubLedger: item.MaintainItemDetail
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        autocomplete: true,
        select: function (e, i) {
          
            $("#hdnlblAccCode").val(i.item.GL_ID);
            $("#lblAccDesc_0").val(i.item.label);
            $("#lblGlType").val(i.item.GL_TYPE);
            $("#lblSlID").val('');
            $("#lblGlID").val(i.item.GL_ID);
            $("#lblAccOldSlCode").val(i.item.OLD_SL_ID);

            if ($("#ddlDRCR").val() == 'D')
                $("#txtDebit_0").focus();
            else
                $("#txtCredit_0").focus();

            Maintain_ItemDetail();


            var found = false;
            var B = window.voucherInfo.voucherForm;
            var C = MAIN.Utils.fixArray(B.VoucherDetail); 
            for (var i in C) {
                if (C[i].SLID == $("#lblSlID").val() && C[i].GLID == $("#lblGlID").val() && C[i].SubID == ($("#lblSubID").val() == "" ? '0000000000' : $("#lblSubID").val())) {
                    alert("Account Code Already Entered. Select Another Account Code.");
                    found = true;
                    ClearText();
                    return false;
                }
            }
                DRCRChanged();
        },
        appendTo: "#AutoComplete",
        minLength: 0
    }).click(function () {
        if ($('#ddlVchType').val() == "") {
            $('#ddlVchType').focus();
            return false;
        }
        if ($('#txtVoucherDate').val() == "") {
            $('#txtVoucherDate').focus();
            return false;
        }
        if ($('#ddlDRCR').val() == "") {
            $('#ddlDRCR').focus(); return false;
        }
        $(this).autocomplete('search', ($(this).val()));
    }).focus(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#lblAccDesc_0').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#hdnlblAccCode").val('');
            $("#lblAccDesc_0").val('');
            $("#lblGlType").val('');
            $("#lblSlID").val('');
            $("#lblGlID").val('');
            $("#lblAccOldSlCode").val('');

        }
        if (iKeyCode == 46) {
            $("#hdnlblAccCode").val('');
            $("#lblAccDesc_0").val('');
            $("#lblGlType").val('');
            $("#lblSlID").val('');
            $("#lblGlID").val('');
            $("#lblAccOldSlCode").val('');

        }
    });
    //======================================== VOUCHER NO AUTO-COMPLETE FOR SEARCH =============================================

    //======================================== SUB-LEDGER AUTO-COMPLETE =============================================
    $("#txtSubLedger").autocomplete({

            source: function (request, response) {

                var Value = $.trim($("#txtSubLedger").val());

                var V = "{AccountCode:'" + $("#hdnlblAccCode").val() + "', Desc:'" + Value + "', FinYear:'" + $("#txtAccFinYear").val() + "',  SectorID:'" + $("#ddlSector").val() + "', VchTypeID:'" + $("#ddlVchType").val() + "'}";
               
                $.ajax({
                    type: "POST",
                    url: "/Accounts_Form/VoucherMaster/Auto_SubLedger_Details",
                    data: V,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        var t = D.Data;
                        var DataAutoComplete = [];
                        if (t.length > 0) {
                            //alert(JSON.stringify(t));
                            $.each(t, function (index, item) {
                                DataAutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                   
                                });
                            });
                            response(DataAutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                $("#hdnSubLedgerCode").val(i.item.AccountCode);


            },
            focus: function (e, i) {
                var eCode = e.which;
                if (eCode == 13) {
                    $("#hdnSubLedgerCode").val(i.item.AccountCode);
                }
                else {
                    $("#hdnSubLedgerCode").val(i.item.AccountCode);
                }
            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
        });

    $('#txtSubLedger').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtSubLedger").val('');
            $("#hdnSubLedgerCode").val('');
        }
        if (iKeyCode == 46) {
            $("#txtSubLedger").val('');
            $("#hdnSubLedgerCode").val('');
        }
    });
    //======================================== SUB-LEDGER AUTO-COMPLETE FOR SEARCH =============================================

    $("#txtVoucherNo").autocomplete({
        source: function (request, response) {

          
            var E = "{VoucherNo: '" + $("#txtVoucherNo").val() + "', VoucherTypeID: '" + $("#ddlVchType").val() + "', VoucherDate: '" + $("#txtVoucherDate").val() + "', SectorID: '" + $("#ddlSector").val() + "'}";

            $.ajax({
                type: "POST",
                url: "/Accounts_Form/VoucherMaster/Search_VoucherNo",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.RefVoucherSlNo,
                                VoucherNumber: item.VoucherNumber
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $("#hdnVoucherNo").val(i.item.VoucherNumber);

            var E = "{VoucherTypeID: '" + $("#ddlVchType").val() + "', VoucherDate: '" + $("#txtVoucherNo").val() + "', VoucherNo: '" + $("#hdnVoucherNo").val() + "', AccFinYear: '" + $.trim($("#ddlGlobalFinYear option:selected").text()) + "',  SectorID: '" + $.trim($("#ddlSector").val()) + "'}";

            $.ajax({
                type: "POST",
                url: "/Accounts_Form/VoucherMaster/Get_VoucherDetails",
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data; //alert(JSON.stringify(t));
                   
                       
                        window.voucherInfo.voucherForm = t;
                        var B = window.voucherInfo.voucherForm; //alert(JSON.stringify(B));
                       
                        var M = MAIN.Utils.fixArray(B.VoucherMast); 
                        var C = MAIN.Utils.fixArray(B.VoucherDetail); //alert(JSON.stringify(C));
                        var D = MAIN.Utils.fixArray(B.tmpVchInstType); //alert(JSON.stringify(D));
                        var S = MAIN.Utils.fixArray(B.tmpVchSubLedger); //alert(JSON.stringify(S));

                        $("#ddlVchType").val(M[0].VoucherType);
                        $("#txtVoucherDate").val(M[0].VCHDATE);
                        $("#txtNarrationval").val(M[0].NARRATION);
                        ShowVoucherDetail(C, "I");
                        $('#dialog-confirms').hide();
                   
                }
            });

        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    //$('#txtVoucherNo').keydown(function (evt) {
       // var iKeyCode = (evt.which) ? evt.which : evt.keyCode
       // if (iKeyCode == 8) {
          //  $("#txtVoucherNo").val('');
          //  $("#hdnVoucherNo").val('');
       // }
       // if (iKeyCode == 46) {
           // $("#txtVoucherNo").val('');
           // $("#hdnVoucherNo").val('');
       // }
   // });

    //======================================== ACC DEPT AUTO-COMPLETE FOR SEARCH =============================================
    $("#txtAccDept").autocomplete({
        source: function (request, response) {

            var E = "{Desc: '" + $("#txtAccDept").val() + "', SectorID: '" + $("#ddlSector").val() + "', AccDept_CostCenter: '" + "Y" + "'}";

            $.ajax({
                type: "POST",
                url: "/Accounts_Form/VoucherMaster/AccDept_AccCostCenter",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.Desccription,
                                Code: item.Code
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
                $("#hdnAccDept").val(i.item.Code);
                $("#txtAccDept").val(i.item.Desccription);
        },
        focus:function(e, i)
        {
            var eCode = e.which; 
            if (eCode == 13) {
                $("#hdnAccDept").val(i.item.Code);
                $("#txtAccDept").val(i.item.Desccription);
            }
            else {
                $("#hdnAccDept").val(i.item.Code);
                $("#txtAccDept").val(i.item.Desccription);
            }
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtAccDept').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtAccDept").val('');
            $("#hdnAccDept").val('');
        }
        if (iKeyCode == 46) {
            $("#txtAccDept").val('');
            $("#hdnAccDept").val('');
        }
    });
    //======================================== ACC COST CENTER AUTO-COMPLETE FOR SEARCH =============================================
    $("#txtCostCenter").autocomplete({
        source: function (request, response) {

            var E = "{Desc: '" + $("#txtCostCenter").val() + "', SectorID: '" + $("#ddlSector").val() + "', AccDept_CostCenter: '" + "N" + "'}";

            $.ajax({
                type: "POST",
                url: "/Accounts_Form/VoucherMaster/AccDept_AccCostCenter",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.Desccription,
                                Code: item.Code
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {

            $("#hdnCostCenter").val(i.item.Code);
            $("#txtCostCenter").val(i.item.Desccription);

        },
        focus: function (e, i) {
            var eCode = e.which;
            if (eCode == 13) {
                $("#hdnCostCenter").val(i.item.Code);
                $("#txtCostCenter").val(i.item.Desccription);
            }
            else {
                $("#hdnCostCenter").val(i.item.Code);
                $("#txtCostCenter").val(i.item.Desccription);
            }
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    }).focus(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtCostCenter').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtCostCenter").val('');
            $("#hdnCostCenter").val('');
        }
        if (iKeyCode == 46) {
            $("#txtCostCenter").val('');
            $("#hdnCostCenter").val('');
        }
    });



    $('#btnRefresh').click(function () {
        location.reload();
    });

});
MAIN.VoucherInfo = function () {
    var A = this;
    this.voucherForm;
    this.init = function () {
        this.voucherForm = JSON.parse($("#hdnPopulateJSON").val());
        //alert(JSON.stringify(this.voucherForm));
        this.masterInfo = new MAIN.VoucherMaster();
        this.masterInfo.init();

    };

    this.findAndRemove = function (array, property, value) {
        //alert(JSON.stringify(array)); alert(property); alert(value);
        $.each(array, function (index, result) {
            //alert(" result[property]  "+result[property] +"  val  "+value + " index "+ index);
            if (result[property] == value) {
                //Remove from array
                array.splice(index, 1);
                return false;
            }
        });
    };
    this.findAndRemove3 = function (array, property, value, property2, value2, property3, value3) {
        $.each(array, function (index, result) {
            if (result[property] == value && result[property2] == value2 && result[property3] == value3) {
                //Remove from array
                array.splice(index, 1);
                return false;
            }
        });
    };

    this.findAndSetDeleteFlag = function (VchDetData, value) {

        for (var i = 0 ; i < VchDetData.length; i++) {
            if (value == VchDetData[i].VchSrl) {
                VchDetailID = VchDetData[i].VoucherDetailID; 
                isdel = VchDetData[i].IsDelete; 
              
                if(VchDetailID == "")
                {
                   var B = window.voucherInfo.voucherForm;
                   window.voucherInfo.findAndRemove(B.VoucherDetail, 'VchSrl', value);
                }
                else
                {
                    var B = window.voucherInfo.voucherForm;

                    //FROM MAIN VOUCHER DETAIL
                    VchDetData[i].IsDelete = 'Y';  

                    //FROM INST VOUCHER DETAIL
                    var a = VchDetData[i].VchInstType; 
                    if(a.length >0)
                    {
                        for (var j = 0 ; j < a.length; j++) {
                            a[j].IsDelete = 'Y';
                        }
                    }
                   
                    //FROM SUBLEDGER VOUCHER DETAIL
                    var b = VchDetData[i].VchSubLedger;
                    if (b.length > 0) {
                        for (var k = 0 ; k < b.length; k++) {
                            b[k].IsDelete = 'Y';
                        }
                    }
                }
            }
        }
    };
    this.findAndSetInstDeleteFlag = function (VchInstData, value) {
           
        var a = VchInstData; 
            if (a.length > 0) {
                for (var j = 0 ; j < a.length; j++) {
                    if (value == a[j].InstSrNo) {
                        if (a[j].BillID == "") {
                            var B = window.voucherInfo.voucherForm;
                            window.voucherInfo.findAndRemove(B.tmpVchInstType, 'InstSrNo', value);
                        }
                        else {
                            a[j].IsDelete = 'Y';
                        }
                    }
                }
            }
    };
    this.findAndSetSLDeleteFlag = function (VchSLData, value) {

        var a = VchSLData;
        if (a.length > 0) {
            for (var j = 0 ; j < a.length; j++) {
                if (value == a[j].SLSlNo) {
                    if (a[j].BillID == "") {
                        var B = window.voucherInfo.voucherForm;
                        window.voucherInfo.findAndRemove(B.tmpVchSubLedger, 'SLSlNo', value);
                    }
                    else {
                        a[j].IsDelete = 'Y';
                    }
                }
            }
        }
    };
    this.findAndSetDeptCodeDeleteFlag = function (VchData, value)
    {
        var a = VchData;
        if (a.length > 0) {
            for (var j = 0 ; j < a.length; j++) {
                if (value == a[j].InstSrNo) {
                    if (a[j].BillID == "") {
                        var B = window.voucherInfo.voucherForm;
                        window.voucherInfo.findAndRemove(B.tmpVchInstType, 'InstSrNo', value);
                    }
                    else {
                        a[j].IsDelete = 'Y';
                    }
                }
            }
        }
    }
};
MAIN.VoucherMaster = function () {
    
    var A = this;
    this.validator;
    this.masterValidator;
    this.init = function () {

        $('#ddlVchType').focus(); $('#ddlVchType').css('background-color', '#DAD8D6');
        $("#txtVoucherDate").keypress(VchDateKeyPress);

        $("#ddlDRCR").change(Change_DRCR_forAmount);
        $("#ddlDRCR").keypress(DRCRKeyDown);

        $("#txtDebit_0").keypress(DebitKeyDown);
        $("#txtCredit_0").keypress(DebitKeyDown);

        $("#btnConfYes").click(ConfYes);
        $("#btnConfNo").click(ConfNo);

        //$("#btnInstAdd").click(InstrumentAdd);
        $("#btnInstConfYes").click(InstConfYes);
        $("#btnInstConfNo").click(InstConfNo);
        $("#ddlInstType").change(CallInstypeBlur);
        $("#ddlInstType").keypress(InstTypeKeyPress);
        $("#ddlInstType").keypress(ddlInstTypeKeyPress);
        $("#ddlInstType").keydown(ddlInstTypekeydown);
        $("#btnInstOK").click(CloseInstrumentType);

        $("#btnSubLedgerAdd").click(SubLedgerAdd);
        $("#btnSubLedgerOK").click(CloseSubLedgerType);
        $("#btnSLConfYes").click(SLConfYes);
        $("#btnSLConfNo").click(SLConfNo);
        $("#ddlSubLedgerType").change(AdjustmentNumber);

        //Account Debt
        $("#btnAccDebtConfYes").click(Acc_DebtConfYes);
        $("#btnAccDebtConfYes").click(Acc_DebtConfYes);
        $("#btnAccDebtConfNo").click(Acc_DebtNo);
        $("#btnAccDeptCostCenterOK").click(Acc_DebtAdd);
        $("#btnAccDeptCostCenter").click(btn_ConfNo);
        $("#btnConfAccDebtConfNo").click(btn_ConfNo);
        $("#btnConfAccDebtConfYes").click(btn_ConfYes);
      
        
        $("#btnNarrOK").click(NarrOK);
        $("#btnNarrCancel").click(NarrCancel);

        $("#btnSave").click(SaveClicked);

        $("lblAccDesc_0").trigger("autocomplete");

        $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        $("#btnPrint").click(Print_Voucher);

        //Tab Navigation on Enter KeyPress
        $('#ddlVchType').bind("keydown", function (e) {
            var n = $("#ddlVchType").val();
            if (n != "") {
                if (e.which == 13) {
                    $('#txtVoucherDate').focus();
                    $('#ddlVchType').css('background-color', '');
                    $('#txtVoucherDate').css('background-color', '#DAD8D6'); return false;
                }
                if (e.which == 39) {
                    $('#txtVoucherDate').focus();
                    $('#ddlVchType').css('background-color', '');
                    $('#txtVoucherDate').css('background-color', '#DAD8D6'); return false;
                }
            }
        });
        $('#txtVoucherDate').bind("keydown", function (e) {
            var n = $("#txtVoucherDate").val();
            if (n != "") {
                if (e.which == 13) {
                    $('#ddlDRCR').focus();
                    $('#txtVoucherDate').css('background-color', '');
                    $('#ddlDRCR').css('background-color', '#DAD8D6'); return false;
                }
                if (e.which == 37) { //left arrow key
                    $('#ddlVchType').focus();
                    $('#txtVoucherDate').css('background-color', '');
                    $('#ddlVchType').css('background-color', '#DAD8D6'); return false;
                }
                if (e.which == 39) { //right arrow key
                    $('#ddlDRCR').focus();
                    $('#txtVoucherDate').css('background-color', '');
                    $('#ddlDRCR').css('background-color', '#DAD8D6'); return false;
                }
            }
        });
        $('#ddlDRCR').bind("keydown", function (e) {
            var n = $("#ddlDRCR").val();
            if (n != "") {
                if (e.which == 13) {
                    $('#lblAccDesc_0').focus();
                    $('#ddlDRCR').css('background-color', '');
                    $('#lblAccDesc_0').css('background-color', '#DAD8D6'); return false;
                }
                if (e.which == 37) { //left arrow key
                    $('#txtVoucherDate').focus();
                    $('#ddlDRCR').css('background-color', '');
                    $('#txtVoucherDate').css('background-color', '#DAD8D6'); return false;
                }
                if (e.which == 39) { //right arrow key
                    $('#lblAccDesc_0').focus();
                    $('#ddlDRCR').css('background-color', '');
                    $('#lblAccDesc_0').css('background-color', '#DAD8D6'); return false;
                }
            }
        });
        $('#lblAccDesc_0').bind("keydown", function (e) {
            var n = $("#ddlDRCR").val();
            if (n != "") {
                if (e.which == 13) {
                    var dc = $('#ddlDRCR').val();
                    if ($('#hdnlblAccCode').val() != "") {
                        if (dc == "D")
                        {
                            $('#txtDebit_0').focus();
                            $('#lblAccDesc_0').css('background-color', '');
                            $('#txtDebit_0').css('background-color', '#DAD8D6'); return false;
                        }
                        else
                        {
                            $('#txtCredit_0').focus();
                            $('#lblAccDesc_0').css('background-color', '');
                            $('#txtCredit_0').css('background-color', '#DAD8D6'); return false;
                        }
                    }
                }
                if (e.which == 37) { //left arrow key
                    $('#ddlDRCR').focus();
                    $('#lblAccDesc_0').css('background-color', '');
                    $('#ddlDRCR').css('background-color', '#DAD8D6'); return false;
                }
                if (e.which == 39) { //right arrow key
                    var dc = $('#ddlDRCR').val();
                    if ($('#hdnlblAccCode').val() != "") {
                        if (dc == "D")
                        {
                            $('#txtDebit_0').focus();
                            $('#lblAccDesc_0').css('background-color', '');
                            $('#txtDebit_0').css('background-color', '#DAD8D6'); return false;
                        }
                        else
                        {
                            $('#txtCredit_0').focus();
                            $('#lblAccDesc_0').css('background-color', '');
                            $('#txtCredit_0').css('background-color', '#DAD8D6'); return false;
                        }
                    }
                }
            }
        });
        $('#txtDebit_0').bind("keydown", function (e) {
            if (e.which == 37) { //left arrow key
                $('#lblAccDesc_0').focus(); return false;
            }
            if (e.which == 39) { //right arrow key
                $('#txtCredit_0').focus(); return false;
            }
        });
        $('#txtCredit_0').bind("keydown", function (e) {
            if (e.which == 37) { //left arrow key
                $('#lblAccDesc_0').focus(); return false;
            }
        });

        $("#btnConfYes, #btnConfNo").keydown(function (e) {
            
            var eCode = e.which;
            if (eCode == 39)   //right arrow key
                $("#btnConfNo").focus();
            if (eCode == 37)   //left arrow key
                $("#btnConfYes").focus();
            if (eCode == 38)   //up arrow key
                $("#btnConfYes").focus();
            if (eCode == 40)    //bottom arrow key
                $("#btnConfNo").focus();
        });
        

        //Instrument model popup navigation
        $('#ddlInstType').bind("keydown", function (e) {
            var n = $("#ddlInstType").val();
            if (n != "") {
                if (e.which == 13) {  //Enter
                    $('#txtInstNo').focus();
                    $('#ddlInstType').css('background-color', '');
                    $('#txtInstNo').css('background-color', '#DAD8D6'); return false;
                }
                if (e.which == 39) {  //Right arrow key
                    $('#txtInstNo').focus();
                    $('#ddlInstType').css('background-color', '');
                    $('#txtInstNo').css('background-color', '#DAD8D6'); return false;
                }
            }
        });
        $('#txtInstNo').bind("keydown", function (e) {

                if (e.which == 13) {  //Enter
                    $('#txtInstDate').focus();
                    $('#txtInstNo').css('background-color', '');
                    $('#txtInstDate').css('background-color', '#DAD8D6'); return false;
                }
                if (e.which == 39) {  //Right arrow key
                    $('#txtInstDate').focus();
                    $('#txtInstNo').css('background-color', '');
                    $('#txtInstDate').css('background-color', '#DAD8D6'); return false;
                }
                if (e.which == 37) {  //Left arrow key
                    $('#ddlInstType').focus();
                    $('#txtInstNo').css('background-color', '');
                    $('#ddlInstType').css('background-color', '#DAD8D6'); return false;
                }
            
        });
        $('#txtInstDate').bind("keydown", function (e) {

            if (e.which == 13) {  //Enter
                $('#txtInstAmount').focus();
                $('#txtInstDate').css('background-color', '');
                $('#txtInstAmount').css('background-color', '#DAD8D6'); return false;
            }
            if (e.which == 39) {  //Right arrow key
                $('#txtInstAmount').focus();
                $('#txtInstDate').css('background-color', '');
                $('#txtInstAmount').css('background-color', '#DAD8D6'); return false;
            }
            if (e.which == 37) {  //Left arrow key
                $('#txtInstNo').focus();
                $('#txtInstDate').css('background-color', '');
                $('#txtInstNo').css('background-color', '#DAD8D6'); return false;
            }

        });
        $('#txtInstAmount').bind("keydown", function (e) {

            if (e.which == 13) {  //Enter
                $('#txtDBankBR').focus();
                $('#txtInstAmount').css('background-color', '');
                $('#txtDBankBR').css('background-color', '#DAD8D6'); return false;
            }
            if (e.which == 39) {  //Right arrow key
                $('#txtDBankBR').focus();
                $('#txtInstAmount').css('background-color', '');
                $('#txtDBankBR').css('background-color', '#DAD8D6'); return false;
            }
            if (e.which == 37) {  //Left arrow key
                $('#txtInstDate').focus();
                $('#txtInstAmount').css('background-color', '');
                $('#txtInstDate').css('background-color', '#DAD8D6'); return false;
            }

        });
        //$('#txtDBankBR').bind("keydown", function (e) {

        //    if (e.which == 13) {  //Enter
        //        $('#btnInstAdd').focus();
        //        $('#txtDBankBR').css('background-color', ''); return false;
        //    }
        //    if (e.which == 39) {  //Right arrow key
        //        $('#btnInstAdd').focus();
        //        $('#txtDBankBR').css('background-color', ''); return false;
        //    }
        //    if (e.which == 37) {  //Left arrow key
        //        $('#txtInstAmount').focus();
        //        $('#txtDBankBR').css('background-color', '');
        //        $('#txtInstAmount').css('background-color', '#DAD8D6'); return false;
        //    }

        //});
        $('#btnInstAdd').bind("keydown click", function (e) {

            //if (e.which == 13) {  //Enter
            //    InstrumentAdd();
            //    //$('#btnInstOK').focus(); return false;
            //}
           if (e.which == 37) {  //Left arrow key
                $('#txtDBankBR').focus(); return false;
           }
           else {
               InstrumentAdd(); $('#btnInstConfNo').focus(); return false;
           }

        });
        $('#btnInstConfNo').bind("keydown", function (e) {

            if (e.which == 13) {  //Enter
                InstConfNo(); return false;
            }
            if (e.which == 37) {  //Left arrow key
                $('#btnInstConfYes').focus(); return false;
            }

        });
        $('#btnInstConfYes').bind("keydown", function (e) {

            if (e.which == 13) {  //Enter
                InstConfYes(); return false;
            }
            if (e.which == 39) {  //Right arrow key
                $('#btnInstConfNo').focus(); return false;
            }

        });

        //Dept Code / Cost Center model popup navigation    
        $("#btnAccDebtConfYes").keydown(function (e) {
            var eCode = e.which;
            if (eCode == 39)   //right arrow key
                $("#btnAccDebtConfNo").focus();
        });
        $("#btnAccDebtConfNo").keydown(function (e) {
            var eCode = e.which;
            if (eCode == 37)   //Left arrow key
                $("#btnAccDebtConfYes").focus();
        });

        $("#btnConfAccDebtConfYes").bind("keydown click", function (e) {
            var eCode = e.which;
            if (eCode == 13)   //Enter key
            {
                $("#dialog-ConfAccDebt").hide();
                $("#txtAccDept").focus();
                $('#txtAccDept').css('background-color', '#DAD8D6');
            }
            if (eCode == 39)   //right arrow key
                $("#btnConfAccDebtConfNo").focus();
        });
        $("#btnConfAccDebtConfNo").keydown(function (e) {
            var eCode = e.which;
            if (eCode == 13)   //Enter key
            {
                btn_ConfNo();
            }
            if (eCode == 37)   //Left arrow key
                $("#btnConfAccDebtConfYes").focus();
        });

        $('#txtAccDept').bind("keydown", function (e) {
                if (e.which == 13) {  //Enter
                    $('#txtCostCenter').focus();
                    $('#txtAccDept').css('background-color', '');
                    $('#txtCostCenter').css('background-color', '#DAD8D6'); return false;
                }
                if (e.which == 39) {  //Right arrow key
                    $('#txtCostCenter').focus();
                    $('#txtAccDept').css('background-color', '');
                    $('#txtCostCenter').css('background-color', '#DAD8D6'); return false;
                }
        });
        $('#txtCostCenter').bind("keydown", function (e) {
                if (e.which == 13) {  //Enter
                    $('#txtDeptCodeAmt').focus();
                    $('#txtCostCenter').css('background-color', '');
                    $('#txtDeptCodeAmt').css('background-color', '#DAD8D6'); return false;
                }
                if (e.which == 39) {  //Right arrow key
                    $('#txtCostCenter').css('background-color', '');
                    $('#txtDeptCodeAmt').css('background-color', '#DAD8D6'); return false;
                }
                if (e.which == 37) {  //Left arrow key
                    $('#txtAccDept').focus();
                    $('#txtCostCenter').css('background-color', '');
                    $('#txtAccDept').css('background-color', '#DAD8D6'); return false;
                }
        });
        $('#txtDeptCodeAmt').bind("keydown", function (e) {

                if (e.which == 13) {  //Enter
                    $('#btnAccDeptCostCenterOK').focus();
                    $('#txtDeptCodeAmt').css('background-color', ''); return false;
                }
                if (e.which == 39) {  //Right arrow key
                    $('#btnAccDeptCostCenterOK').focus();
                    $('#txtDeptCodeAmt').css('background-color', ''); return false;
                }
                if (e.which == 37) {  //Left arrow key
                    $('#txtCostCenter').focus();
                    $('#txtDeptCodeAmt').css('background-color', '');
                    $('#txtCostCenter').css('background-color', '#DAD8D6'); return false;
                }
           
        });
        $('#btnAccDeptCostCenterOK').bind("keydown click", function (e) {

            if (e.which == 13) {  //Enter
                Acc_DebtAdd();  return false;
            }
            if (e.which == 37) {  //Left arrow key
                $('#txtDeptCodeAmt').focus(); return false;
            }
            //else
            //{
            //    Acc_DebtAdd();  return false;
            //}

        });
        $('#btnAccDeptCostCenter').bind("keydown", function (e) {

            if (e.which == 13) {  //Enter
                CallCalculateDeptCodeAmount();  return false;
            }
            if (e.which == 37) {  //Left arrow key
                $('#txtAccDept').focus(); return false;
            }
            if (e.which == 39) {  //Left arrow key
                $('#txtAccDept').focus(); return false;
            }
        });

        //SubLedger navigation
        $('#txtSubLedger').bind("keydown", function (e) {
            if (e.which == 13) {  //Enter
                $('#ddlSubLedgerType').focus();
                $('#txtSubLedger').css('background-color', '');
                $('#ddlSubLedgerType').css('background-color', '#DAD8D6'); return false;
            }
            if (e.which == 39) {  //Right arrow key
                $('#ddlSubLedgerType').focus();
                $('#txtSubLedger').css('background-color', '');
                $('#ddlSubLedgerType').css('background-color', '#DAD8D6'); return false;
            }
        });
        $('#ddlSubLedgerType').bind("keydown", function (e) {

            if (e.which == 13) {  //Enter
                $('#txtSubLedNo').focus();
                $('#ddlSubLedgerType').css('background-color', '');
                $('#txtSubLedNo').css('background-color', '#DAD8D6'); return false;
            }
            if (e.which == 39) {  //Right arrow key
                $('#txtSubLedNo').focus();
                $('#ddlSubLedgerType').css('background-color', '');
                $('#txtSubLedNo').css('background-color', '#DAD8D6'); return false;
            }
            if (e.which == 37) {  //Left arrow key
                $('#txtSubLedger').focus();
                $('#ddlSubLedgerType').css('background-color', '');
                $('#txtSubLedger').css('background-color', '#DAD8D6'); return false;
            }

        });
        $('#txtSubLedNo').bind("keydown", function (e) {

            if (e.which == 13) {  //Enter
                $('#txtSubLedAmount').focus();
                $('#txtSubLedNo').css('background-color', '');
                $('#txtSubLedAmount').css('background-color', '#DAD8D6'); return false;
            }
            if (e.which == 39) {  //Right arrow key
                $('#txtSubLedAmount').focus();
                $('#txtSubLedNo').css('background-color', '');
                $('#txtSubLedAmount').css('background-color', '#DAD8D6'); return false;
            }
            if (e.which == 37) {  //Left arrow key
                $('#ddlSubLedgerType').focus();
                $('#txtSubLedAmount').css('background-color', '');
                $('#ddlSubLedgerType').css('background-color', '#DAD8D6'); return false;
            }

        });
        $('#txtSubLedAmount').bind("keydown", function (e) {

            if (e.which == 13) {  //Enter
                $('#btnSubLedgerAdd').focus();
                $('#txtSubLedAmount').css('background-color', ''); return false;
            }
            if (e.which == 39) {  //Right arrow key
                $('#btnSubLedgerAdd').focus();
                $('#txtSubLedAmount').css('background-color', ''); return false;
            }
            if (e.which == 37) {  //Left arrow key
                $('#txtSubLedNo').focus();
                $('#txtSubLedAmount').css('background-color', '');
                $('#txtSubLedNo').css('background-color', '#DAD8D6'); return false;
            }

        });
        $('#btnSubLedgerAdd').bind("keydown click", function (e) {

            if (e.which == 13) {  //Enter
                SubLedgerAdd(); return false;
            }
            if (e.which == 37) {  //Left arrow key
                $('#txtSubLedAmount').focus();
                $('#txtSubLedAmount').css('background-color', '#DAD8D6'); return false;
            }
        });
        $("#btnSLConfYes").keydown(function (e) {
            var eCode = e.which;
            if (eCode == 39)   //right arrow key
                $("#btnSLConfNo").focus();
        });
        $("#btnSLConfNo").keydown(function (e) {
            var eCode = e.which;
            if (eCode == 37)   //Left arrow key
                $("#btnSLConfYes").focus();
        });

        OnPage_Load();
    }
};
MAIN.Utils = function () {
    return {
        /* ReatEasyFix for Arrays START */
        fixArray: function (array) {
            if ($.isArray(array)) {
                return array;
            } else {
                return [array];
            }
        }
        /* ReatEasyFix for Arrays END */
    };
}();

VchDateKeyPress = function (evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
    if (iKeyCode == 13) {

        if ($("#txtVoucherDate").val() != "") {
           
            if (isValidDate($.trim($("#txtVoucherDate").val()))) {
                $("#lblAccDesc_0").focus();
                return false;
            }
            else {
                alert("Please Enter Valid Voucher Date !!");
                return false;
            }
        } else {
            alert("Please Enter Voucher Date !!");
            $("#txtVoucherDate").focus();
            return false;
        }

    }
    if (iKeyCode == 9) {
        if ($("#txtVoucherDate").val() != "") {
            if (isValidDate($.trim($("#txtVoucherDate").val()))) {
                $("#lblAccDesc_0").focus();
                return false;
            } else {
                alert("Please Enter Valid Voucher Date !!");
                return false;
            }
        } else {
            alert("Please Enter Voucher Date !!");
            $("#txtVoucherDate").focus();
            return false;
        }

    }
};
DRCRChanged = function () {
    if ($("#ddlDRCR").val() != "") {
        
        if ($("#hdnlblAccCode").val() == "") {$("#lblAccDesc_0").focus(); $("#ddlDRCR").val(''); return false;}

        $("#txtDebit_0").removeAttr("disabled");
        $("#txtCredit_0").removeAttr("disabled");
        $("#txtCredit_0").val("");
        $("#txtDebit_0").val("");
        var ValGLType = $("#lblGlType").val();

        var VchType = $("#ddlVchType").val();

        if (ValGLType == "B") {  //FOR LEDGER
            if ($("#ddlDRCR").val() == "D") {

                $("#txtCredit_0").prop("disabled", true);
                $("#txtDebit_0").prop("disabled", false);
                $("#txtDebit_0").focus();
                $("#txtInstAmount").val(GetDebitCreditBal); //A.GetDebitCreditBal() $("#txtTotalCredit").val()GetCreditDebitBal

                VchType == "2" ? ($("#txtInstDate").val('')) : ($("#txtInstDate").val($("#txtVoucherDate").val()));
               
                ShowModalPopupInstType();
                $("#ddlInstType").focus();
                $('#ddlInstType').css('background-color', '#DAD8D6');

            } else if ($("#ddlDRCR").val() == "C") {
                $("#txtDebit_0").prop("disabled", true);
                $("#txtCredit_0").prop("disabled", false);
                $("#txtInstAmount").val(GetDebitCreditBal());
                $("#txtCredit_0").focus();

                VchType == "2" ? ($("#txtInstDate").val('')) : ($("#txtInstDate").val($("#txtVoucherDate").val()));
               
                ShowModalPopupInstType();
                $("#ddlInstType").focus();
                $('#ddlInstType').css('background-color', '#DAD8D6');
            }


        } else if (ValGLType == "S") {   //FOR SUBLEDGER

            if ($("#ddlDRCR").val() == "D") {
                $("#txtCredit_0").prop("disabled", true);
                $("#txtDebit_0").prop("disabled", false);
                $("#txtDebit_0").focus();
              
                ShowModalPopup_SubLedger();
               

            } else if ($("#ddlDRCR").val() == "C") {
                $("#txtDebit_0").prop("disabled", true);
                $("#txtCredit_0").prop("disabled", false).val(parseFloat(GetDebitCreditBal()).toFixed(2));
                $("#txtCredit_0").focus();
               
                ShowModalPopup_SubLedger();
               
               
            }
        }
        else {
            if ($("#ddlDRCR").val() == "D") {

                $("#txtCredit_0").prop("disabled", true);
                $("#txtDebit_0").prop("disabled", false);
                $("#txtDebit_0").prop("disabled", false).val(parseFloat(GetDebitCreditBal()).toFixed(2));
                $("#txtDebit_0").focus();
                $("#txtDebit_0").select();
                Show_AccountDebt();  ///this is confirmation message
                //Acc_DebtConfYes();     ///this is direct open popup for Cost Dept and Cost Center
                //$("#txtAccDept").focus();
                //$('#txtAccDept').css('background-color', '#DAD8D6');

            } else if ($("#ddlDRCR").val() == "C") {
                $("#txtDebit_0").prop("disabled", true);
                $("#txtCredit_0").prop("disabled", false).val(parseFloat(GetDebitCreditBal()).toFixed(2));
                $("#txtCredit_0").focus();
                $("#txtCredit_0").select();
                Show_AccountDebt();  ///this is confirmation message
                //Acc_DebtConfYes();     ///this is direct open popup for Cost Dept and Cost Center
                //$("#txtAccDept").focus();
                //$('#txtAccDept').css('background-color', '#DAD8D6');
            }

        }

    } else {

        $("#txtCredit_0").val("");
        $("#txtDebit_0").val("");
        $("#txtDebit_0").prop("disabled", true);
        $("#txtCredit_0").prop("disabled", true);
    }
};
DRCRKeyDown = function (evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

    if (iKeyCode == 13) {
        DRCRChanged();
    }
}
GetDebitCreditBal = function () {
    //if (Modifyflg == 'Add') {
    var BalAmount = '';
    var TotalDeb = ($("#txtTotalDebit").val() == "" ? 0 : parseFloat($("#txtTotalDebit").val()));  //yes
    var TotalCre = ($("#txtTotalCredit").val() == "" ? 0 : parseFloat($("#txtTotalCredit").val()));
    if ($("#ddlDRCR").val() == "C") {
        BalAmount = TotalDeb - TotalCre;
    }
    else if ($("#ddlDRCR").val() == "D") {
        BalAmount = TotalCre - TotalDeb;
    }
    if (BalAmount <= 0) { BalAmount = 0; }
    return BalAmount;
    //}
}

DebitKeyDown = function (evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode == 13) {
        isEdit = true;
        if ($("#ddlDRCR").val() == "") {
            alert("Please Select Debit or Credit First !!");
            $('#ddlDRCR').css('background-color', '#DAD8D6');
            $('#ddlDRCR').focus();
            return false;
        }
        if ($("#txtDebit_0").val() != "" || $("#txtCredit_0").val() != "") {
            if ($("#ddlDRCR").val() == "D" && parseFloat($.trim($("#txtDebit_0").val())) == 0) {
                alert("Amount Must be greater than 0");
                return false;
            }

            if ($("#ddlDRCR").val() == "C" && parseFloat($.trim($("#txtCredit_0").val())) == 0) {
                alert("Amount Must be greater than 0");
                return false;
            }

            var B = window.voucherInfo.voucherForm; 
            var SectorID = B.VoucherMast.SectorID; 
            var C = MAIN.Utils.fixArray(B.VoucherDetail); 
            var D = MAIN.Utils.fixArray(B.tmpVchInstType); //alert(D.length); alert(JSON.stringify(D));
            var E = MAIN.Utils.fixArray(B.tmpVchSubLedger); 
           
            if ($("#lblGlType").val() == "B") {

                if (D == "") {
                    alert("Instrument is required when Bank/Branch/Cash is Selected");
                    ShowModalPopupInstType();
                    if ($("#ddlDRCR").val() == "D")
                        $("#txtInstAmount").val($("#txtDebit_0").val());
                    else
                        $("#txtInstAmount").val($("#txtCredit_0").val());

                    $("#ddlInstType").val("C");
                    $("#ddlInstType").focus();
                    return false;
                }


                var AMOUNT = ($("#ddlDRCR").val() == "D" ? $("#txtDebit_0").val() : $("#txtCredit_0").val());
                var AMOUNTType = ($("#ddlDRCR").val() == "D" ? "Debit Amount" : "Credit Amount");
                var instamt = parseFloat(0);

                for (var k in D) {
                    if (D[k].IsDelete != 'Y')
                        instamt += parseFloat(D[k].Amount);
                }

                if (parseFloat(AMOUNT) != parseFloat(instamt.toFixed(2))) {
                    alert("Total Amount of Instrument Details and " + AMOUNTType + " must be equal.");
                    ShowModalPopupInstType();
                    $("#ddlInstType").val("C");
                    $("#ddlInstType").focus();
                    return;
                }
                if (parseFloat(instamt.toFixed(2)) == 0) {
                    alert("Total Amount.");
                    ShowModalPopupInstType();
                    $("#ddlInstType").val("C");
                    $("#ddlInstType").focus();
                    return;
                }
            }
            else if ($("#lblGlType").val() == "S") {
                if (E == "") {
                    alert("Sub Ledger Details Entry is required.");
                    ShowModalPopup_SubLedger();
                    if ($("#ddlDRCR").val() == "D")
                        $("#txtSubLedAmount").val($("#txtDebit_0").val());
                    else
                        $("#txtSubLedAmount").val($("#txtCredit_0").val());

                    $("#txtSubLedger").focus();
                    return false;
                }
                var AMOUNT = ($("#ddlDRCR").val() == "D" ? $("#txtDebit_0").val() : $("#txtCredit_0").val()); 
                var AMOUNTType = ($("#ddlDRCR").val() == "D" ? "Debit Amount" : "Credit Amount");
                var slamt = parseFloat(0);

                for (var v in E) {
                    if (E[v].IsDelete != 'Y')
                        slamt += parseFloat(E[v].Amount);
                }
                
                if (parseFloat(AMOUNT) != parseFloat(slamt.toFixed(2))) {
                    alert("Total Amount of SubLedger Details and " + AMOUNTType + " must be equal.");
                    ShowModalPopup_SubLedger();
                    $("#txtSubLedger").focus();
                    return;
                }
           }
           else
           {
               //alert(JSON.stringify(D));
               if (D != "") {
                   var AMOUNT = ($("#ddlDRCR").val() == "D" ? $("#txtDebit_0").val() : $("#txtCredit_0").val());
                   var AMOUNTType = ($("#ddlDRCR").val() == "D" ? "Debit Amount" : "Credit Amount");
                   var instamt = parseFloat(0);

                   for (var k in D) {
                       if (D[k].IsDelete != 'Y')
                           instamt += parseFloat(D[k].Amount);
                   }

                   var fa = false;
                   for (var k in D) {
                       var val = D[k].IsDelete; 
                       if (val != 'Y') {
                           fa = true; 
                       }
                   }
                   // alert(AMOUNT); alert(instamt); 
                   //alert(fa);

                   if (fa) {
                     

                       if (parseFloat(AMOUNT) != parseFloat(instamt.toFixed(2))) {
                           alert("Total Amount of Dept Code / Cost Center Details and " + AMOUNTType + " must be equal.");
                           // Acc_DebtConfYes();
                           $("#dialog-AccDeptCostCenter").modal("show");
                           return;
                       }
                       if (parseFloat(instamt.toFixed(2)) == 0) {
                         

                           alert("Total Amount.");
                           //Acc_DebtConfYes();
                           $("#dialog-AccDeptCostCenter").modal("show");
                           return;
                       }
                   }
               }
           }
           
         
            var found = false;

           
            if (Modifyflg == 'Add') {
                vchslr =  1;
                var arr = $("#tbl_1 tbody tr.cls_test_Detail").map(function (ind, val) { return Number($(val).attr("max-vch-detail")) }).sort(function (a, b) { return b - a });
                if (arr.length > 0) { vchslr = arr[0] + 1; }
                

            }
            else {
                vchslr = glvrno;
            }

          
            if (C == "") {
             
                if ($("#lblGlType").val() == 'B' && $("#ddlVchType").val() == 1 && $("#ddlDRCR").val() == 'C') {
                    alert('Sorry ! You can not take Bank.');
                    found = true;
                }
                if ($("#lblGlType").val() == 'B' && $("#ddlVchType").val() == 2 && $("#ddlDRCR").val() == 'D') {
                    alert('Sorry ! You can not take Bank.');
                    found = true;
                }
               
                if (!found) {


                    C.push({
                        YearMonth: $("#txtVoucherDate").val(),
                        VoucherDetailID: $("#lblVoucherDetailID").val(),
                        VoucherType: $("#ddlVchType").val(),
                        VCHNo: 0,
                        OLDSLID: $("#lblAccOldSlCode").val(),
                        GLName: $("#lblAccDesc_0").val(),
                        VchSrl: vchslr,
                        GlType: $("#lblGlType").val(),
                        GLID: $("#lblGlID").val(),
                        SLID: $("#lblSlID").val(),
                        SubID: ($("#lblSubID").val() == "" ? '0000000000' : $("#lblSubID").val()),
                        OLDSUBID: ($("#lblOldSubCode").val() == "" ? '' : $("#lblOldSubCode").val()),
                        SUBDesc: ($("#lblSubAccDesc_0").val() == "" ? '' : $("#lblSubAccDesc_0").val()),
                        DRCR: $("#ddlDRCR").val(),
                        AMOUNT: ($("#ddlDRCR").val() == "D" ? $("#txtDebit_0").val() : $("#txtCredit_0").val()),
                        SectorID: SectorID,
                        IsItemDetail: Maintain_ItemSubLedger,
                        IsDelete: null,
                        AccDept: $("#txtAccDept").val(),
                        AccDeptCode: $("#hdnAccDept").val(),
                        AccCostCenterCode: $("#hdnCostCenter").val(),
                        AccCostCenter: $("#txtCostCenter").val(),
                        VchInstType: D,
                        VchSubLedger: E
                        
                    });
                }
            }
            else {
               
                for (var i in C) {

                    if (C[i].SLID == $("#lblSlID").val() && C[i].GLID == $("#lblGlID").val() && C[i].SubID == ($("#lblSubID").val() == "" ? '0000000000' : $("#lblSubID").val())) {
                        alert("Account Code Already Entered. Select Another Account Code.");
                        found = true;
                    }

                }
                if ($("#lblGlType").val() == 'B' && $("#ddlVchType").val() == 1 && $("#ddlDRCR").val() == 'C') {
                    alert('Sorry ! You can not take Bank.');
                    found = true;
                }
                if ($("#lblGlType").val() == 'B' && $("#ddlVchType").val() == 2 && $("#ddlDRCR").val() == 'D') {
                    alert('Sorry ! You can not take Bank.');
                    found = true;
                }
                if (!found) {

                    C.push({
                        YearMonth: $("#txtVoucherDate").val(),
                        VoucherDetailID: $("#lblVoucherDetailID").val(),
                        VoucherType: $("#ddlVchType").val(),
                        VCHNo: 0,
                        OLDSLID: $("#lblAccOldSlCode").val(),
                        GLName: $("#lblAccDesc_0").val(),
                        VchSrl: vchslr,
                        GlType: $("#lblGlType").val(),
                        GLID: $("#lblGlID").val(),
                        SLID: $("#lblSlID").val(),
                        SubID: ($("#lblSubID").val() == "" ? '0000000000' : $("#lblSubID").val()),
                        OLDSUBID: ($("#lblOldSubCode").val() == "" ? '' : $("#lblOldSubCode").val()),
                        SUBDesc: ($("#lblSubAccDesc_0").val() == "" ? '' : $("#lblSubAccDesc_0").val()),
                        DRCR: $("#ddlDRCR").val(),
                        AMOUNT: ($("#ddlDRCR").val() == "D" ? $("#txtDebit_0").val() : $("#txtCredit_0").val()),
                        SectorID: SectorID,
                        IsItemDetail: Maintain_ItemSubLedger,
                        IsDelete: null,
                        AccDept: $("#txtAccDept").val(),
                        AccDeptCode: $("#hdnAccDept").val(),
                        AccCostCenterCode: $("#hdnCostCenter").val(),
                        AccCostCenter: $("#txtCostCenter").val(),
                        VchInstType: D,
                        VchSubLedger: E
                    });
                }
            }
          
            D = [];
            E = [];
            B.tmpVchSubLedger = [];
            B.tmpVchInstType = [];
            ClearText();
            //alert(JSON.stringify(C));
            ShowVoucherDetail(C, "I");
            Modifyflg = 'Add';
            return false;
        } else {
            alert("Please Enter " + ($("#ddlDRCR").val() == "D" ? "Debit" : "Credit") + " amount.");
            return false;
        }
    } else {
        return true; Modifyflg = 'Add';
    }
    Modifyflg = 'Add';
    
    return false;
}

ClearText = function () {

    $("#hdnlblAccCode").val("");
    $("#lblAccOldSlCode").val("");
    $("#lblAccDesc_0").val("");
    $("#lblGlID").val("");
    $("#lblSlID").val("");
    $("#lblSubID").val("");
    $("#lblOldSubCode").val("");
    $("#lblSubAccDesc_0").val("");
    $("#ddlDRCR").val("");
    $("#txtDebit_0").val("");
    $("#txtCredit_0").val("");
    $("#lblGlType").val("");
    $(".tblInst").find("tr:gt(1)").remove();
    $("#lblVoucherDetailID").val('');
    $("#ddlDRCR").focus();

    $("#hdnAccDept").val('');
    $("#txtAccDept").val('');
    $("#hdnCostCenter").val('');
    $("#txtCostCenter").val('');
}
ShowVoucherDetail = function (C, Type) {
    var html = "";
    var slno = 0;
    
    $('.cls_test_Detail').remove();
    var $this = $('#tbl_1 .test_0');
    $parentTR = $this.closest('tr');

    if (C.length > 0) {
        //alert(JSON.stringify(C));
        for (var i in C) {
            slno = slno + 1;
            //instrument Date Exceed 90-Day's! Will you Proceed Further (Y/N): 
            var isDelete = C[i].IsDelete; 
            if (isDelete != 'Y') {

                var visible = 'block';
                var glType = C[i].GlType; 
                if (glType == "B" || glType == 'S')
                    visible = 'none';

                html += "<tr class='cls_test_Detail' id='tr_" + C[i].VchSrl + "' max-vch-detail='" + C[i].VchSrl + "' style='cursor:pointer' >"  //onclick='Get_Details(this, \"" + C[i].VchSrl + "\")'
                    + "<td id='td_" + C[i].VchSrl + "'><div style='text-align:center;'>"
                    + "<img src='/Content/Images/Delete.png' alt='Delete Voucher' style='width:20px;height:20px;cursor:pointer;' title='Delete Voucher' id='deleteVchSID_" + C[i].VchSrl + "' onClick='removeVoucherDetail(" + C[i].VchSrl + ");' />"
                    + "</div></td>"
                    + "<td style='width: 10%; font-family:Verdana; text-align:center;'>" + (C[i].DRCR == "D" ? "DR" : "CR") + "</td>"
                    + "<td style='width: 15%; font-family:Verdana; text-align:center; display: " + (MaintainAccCode == "N" ? "none" : "normal") + "'>" + C[i].OLDSLID + "<br/>" 
                    + "<td style='width: 40%; font-family:Verdana;'>" + C[i].GLName + "<br/>" 
                    + "<td style='width: 10%; font-family:Verdana; text-align:right;'>" + (C[i].DRCR == "D" ? C[i].AMOUNT : "") + "</td>"
                    + "<td style='width: 10%; font-family:Verdana; text-align:right;'>" + (C[i].DRCR == "C" ? C[i].AMOUNT : "") + "</td>"
                    + "<td style='width: 10%; font-family:Verdana; text-align:right; display:none;'>" + C[i].VoucherDetailID + "</td>"
                    + "<td style='width: 10%; font-family:Verdana; text-align:right; display:none;'>" + C[i].GLID + "</td>"
                    + "<td style='width: 10%; font-family:Verdana; text-align:right; display:none;'>" + C[i].IsItemDetail + "</td>"
                 + "<td id='td_" + C[i].VchSrl + "'><div style='text-align:center;'>"
                 + "<img src='/Content/Images/Edit.png' style='width:20px;height:20px;cursor:pointer;'  title='Edit Voucher' alt='Edit Voucher' id='editVchSID_" + C[i].VchSrl + "' onClick='editVoucherDetail(this, \"" + C[i].VchSrl + "\");' />"  //onClick='editVoucherDetail(this, \"" + C[i].VchSrl + "\");'
                 + "</div></td>"
                 //+ "<td id='td_" + C[i].VchSrl + "'><div style='text-align:center;'>"
                 //+ "<img src='/Content/Images/Edit.png' style='width:20px;height:20px;cursor:pointer; display:"+ visible +"'  title='Edit Acc Dept/Cost Center' alt='Edit Voucher' id='editVchSID_" + C[i].VchSrl + "' onClick='editAccDeptCostCenter(this, \"" + C[i].VchSrl + "\");' />"  //onClick='editVoucherDetail(this, \"" + C[i].VchSrl + "\");'
                 //+ "</div></td>"

                html + "</tr>";
            }
        }
        $parentTR.after(html);
    }
    CalCulateDebitCredit(C, Type);
};
CalCulateDebitCredit = function (C, Type) {
   
    var TotalDebit = 0;
    var TotalCredit = 0;
    for (var i in C) {
        if (C[i].IsDelete != 'Y') {
            if (C[i].DRCR == "D") {
                TotalDebit = (parseFloat(TotalDebit) + parseFloat(C[i].AMOUNT)).toFixed(2);
            } else if (C[i].DRCR == "C") {
                TotalCredit = (parseFloat(TotalCredit) + parseFloat(C[i].AMOUNT)).toFixed(2);
            }
        }
    }

    $("#txtTotalDebit").val(TotalDebit);
    $("#txtTotalCredit").val(TotalCredit);

    if (TotalCredit > 0 && TotalDebit > 0 && TotalCredit == TotalDebit && Type == "I") {
        ShowConfirmSaveDialog();
    }
    else
    {
        $("#ddlDRCR").focus(); $('#lblAccDesc_0').css('background-color', ''); $('#ddlDRCR').css('background-color', '#DAD8D6'); return false;
    }

};
ShowConfirmSaveDialog = function () {
    $("#lblMSG").html("Total Debit is equal to Total Credit. Do you want to save the voucher ??");
    //$("#dialog-confirms").show();
    $("#dialog-confirms").fadeToggle(1000);
    $("#btnConfYes").focus();
};
removeVoucherDetail = function (VchSRL) {
    var res = confirm('Are You Sure want to Delete this Record ??');
    if (res == true) {
        if (GlobalSTATUS == 'Complete') {
            alert('This Voucher already have been approved! Can Not Delete?');
            return false;
        }

        var B = {};
        B = window.voucherInfo.voucherForm;

        var a = window.voucherInfo.findAndSetDeleteFlag(B.VoucherDetail, VchSRL);// alert(JSON.stringify(a));
        //window.voucherInfo.findAndRemove(B.VoucherDetail, 'VchSrl', VchSRL);

        ShowVoucherDetail(B.VoucherDetail, "I");
        $("#lblAccDesc_0").focus();
        return false;
    }
}
editVoucherDetail = function (h, VchSRL) {
  
    if (isEdit == true) {
        isEdit = false;
        if (GlobalSTATUS == 'Complete') {
            alert('This Voucher already have been approved! Can Not Edit?');
            return false;
        }

        var B = {};
        B = window.voucherInfo.voucherForm;
        var ValAccOldSlCode = '';

        var VchMstData = {};
        var VchDetData = {};
        var VchInstData = {};
        var VchSLData = {};

        VchMstData = B.VoucherMast;
        VchDetData = B.VoucherDetail; //alert(JSON.stringify(VchDetData));
        VchInstData = B.tmpVchInstType; //alert(JSON.stringify(VchInstData));
        //VchSLData = B.tmpVchSubLedger;

        var ValVchDetailID = '';
        var VallblGlID = '';
        var VallblGlType = '';
        var VallblSlID = '';
        var VallblSubID = '';
        var ValOldSlCode = '';
        var ValAccDesc = '';
        var ValueDRCR = '';
        var ValueAmount = '';

        var SubID = '';
        var SubName = '';

        var AccDept = '';
        var AccDeptCode = '';
        var AccCostCenterCode = '';
        var AccCostCenter = '';


        for (var i = 0 ; i < VchDetData.length; i++) {
            if (VchSRL == VchDetData[i].VchSrl) {
                ValVchDetailID = VchDetData[i].VoucherDetailID;
                VallblGlID = VchDetData[i].GLID;
                VallblGlType = VchDetData[i].GlType;
                VallblSlID = VchDetData[i].SLID;
                ValOldSlCode = VchDetData[i].OLDSLID;
                ValAccDesc = VchDetData[i].GLName;
                ValueDRCR = VchDetData[i].DRCR;
                ValueAmount = VchDetData[i].AMOUNT;

                VallblSubID = VchDetData[i].SubID;
                SubID = VchDetData[i].OLDSUBID;
                SubName = VchDetData[i].SUBDesc;
                Maintain_ItemSubLedger = VchDetData[i].IsItemDetail;

                AccDept = VchDetData[i].AccDept;
                AccDeptCode = VchDetData[i].AccDeptCode;
                AccCostCenterCode = VchDetData[i].AccCostCenterCode;
                AccCostCenter = VchDetData[i].AccCostCenter;
            }
        }

        $("#lblGlID").val(VallblGlID);
        $("#lblGlType").val(VallblGlType);
        $("#lblSlID").val(VallblSlID);
        $("#lblSubID").val(VallblSubID);

        $("#lblAccOldSlCode").val(ValOldSlCode);
        $("#lblAccDesc_0").val(ValAccDesc);
        $("#hdnlblAccCode").val(VallblGlID);
        $("#lblVoucherDetailID").val(ValVchDetailID);

        $("#hdnAccDept").val(AccDeptCode);
        $("#txtAccDept").val(AccDept);
        $("#hdnCostCenter").val(AccCostCenterCode);
        $("#txtCostCenter").val(AccCostCenter);

        if (ValueDRCR == 'D') {
            $("#ddlDRCR").val(ValueDRCR);
            $("#txtDebit_0").val(ValueAmount);
            $("#txtCredit_0").val('');
        }
        else {
            $("#ddlDRCR").val(ValueDRCR);
            $("#txtCredit_0").val(ValueAmount);
            $("#txtDebit_0").val('');
        }

        if (VallblSubID == '0000000000') {
            $("#lblOldSubCode").val('');
            $("#lblSubAccDesc_0").val('');
        }
        else {
            $("#lblOldSubCode").val(SubID);
            $("#lblSubAccDesc_0").val(SubName);
        }

        B.tmpVchInstType = [];
        B.tmpVchSubLedger = [];

        glvrno = VchSRL;
        var SectorID = B.VoucherMast.SectorID;
        var C = MAIN.Utils.fixArray(B.tmpVchInstType); //alert(JSON.stringify(C));   //FOR INSTRUMENT POPUP
        var S = MAIN.Utils.fixArray(B.tmpVchSubLedger); //alert(JSON.stringify(S));    //FOR SUBLEDGER POPUP 
        var rowid_C = 1;
        var rowid_S = 1;


        $.each(VchDetData, function (index, result) {
            if (result['VchSrl'] == glvrno) {

                $.each(result, function (index1, DataInst) {

                    for (var k in DataInst) {

                        if ($("#lblGlID").val() == DataInst[k].GLID && $("#lblSlID").val() == DataInst[k].SLID) {

                            C.push({
                                InstSrNo: rowid_C,
                                YearMonth: '',
                                BillID: DataInst[k].BillID,
                                VoucherType: $("#ddlVchType").val(),
                                VCHNo: DataInst[k].VCHNo,
                                GLID: $("#lblGlID").val(),
                                SLID: $("#lblSlID").val(),
                                DRCR: $("#ddlDRCR").val(),
                                SlipNo: DataInst[k].SlipNo,
                                InstType: DataInst[k].InstType,
                                InstTypeName: DataInst[k].InstTypeName,
                                InstNo: DataInst[k].InstNo,
                                InstDT: DataInst[k].InstDT,
                                Amount: DataInst[k].Amount,
                                ActualAmount: DataInst[k].Amount,
                                Drawee: DataInst[k].Drawee,
                                SectorID: DataInst[k].SectorID,
                                IsDelete: DataInst[k].IsDelete,
                                AccDeptCode: DataInst[k].AccDeptCode,
                                AccCostCenterCode: DataInst[k].AccCostCenterCode,
                                AccDept: DataInst[k].AccDept,
                                AccCostCenter: DataInst[k].AccCostCenter,
                            });

                            rowid_C = rowid_C + 1;
                            ShowInstDetail(C);
                            //ShowDeptCodeDetail(C);
                        }
                    }
                });
            }
        });

        $.each(VchDetData, function (index, result) {
            if (result['VchSrl'] == glvrno) {

                $.each(result, function (index1, DataInst) {

                    for (var k in DataInst) {

                        if ($("#lblGlID").val() == DataInst[k].AccountCODE) {

                            S.push({
                                SLSlNo: rowid_S,
                                BillID: DataInst[k].BillID,
                                SubLedgerTypeID: DataInst[k].SubLedgerTypeID,
                                SubLedgerType: DataInst[k].SubLedgerType,
                                Number: DataInst[k].Number,
                                Qty: DataInst[k].Qty,
                                IRate: DataInst[k].IRate,
                                IsItemDetail: Maintain_ItemSubLedger,
                                Amount: DataInst[k].Amount,
                                DRCR: DataInst[k].DRCR,
                                AccountCODE: DataInst[k].AccountCODE,
                                SubLedger: DataInst[k].SubLedger,
                                SubLedgerCODE: DataInst[k].SubLedgerCODE,
                                IsDelete: DataInst[k].IsDelete,
                            });

                            rowid_S = rowid_S + 1;
                            ShowSubLedgerDetail(S);
                        }
                    }
                });
            }
        });


        window.voucherInfo.findAndRemove(B.VoucherDetail, 'VchSrl', VchSRL);
        ShowVoucherDetail(B.VoucherDetail, "I");

        // alert(JSON.stringify(C));
        if (S.length > 0 && $("#lblGlType").val() == "S") {
            ShowModalPopup_SubLedger();
        }
        else if (C.length > 0 && $("#lblGlType").val() == "B") {
            ShowModalPopupInstType();
        }
        else {
            if (C.length > 0)
            {
                if (MaintainCostCenter == "Y" || MaintainDeptCode == "Y") {
                    ShowDeptCodeDetail(C);
                    $("#dialog-AccDeptCostCenter").modal("show");
                    $('#txtAccDept').css('background-color', '#DAD8D6');
                    $('#txtAccDept').focus();
                }
            }
            else
            {
                if (MaintainCostCenter == "Y" || MaintainDeptCode == "Y") {
                    $("#dialog-AccDeptCostCenter").modal("show");
                }
            }
           
            if ($("#ddlDRCR").val() == 'D') {
                $("#txtCredit_0").prop("disabled", true);
                $("#txtDebit_0").prop("disabled", false).val();
                $("#txtDebit_0").focus();
                $("#txtDebit_0").select();
            }
            else {
                $("#txtDebit_0").prop("disabled", true);
                $("#txtCredit_0").prop("disabled", false).val();
                $("#txtCredit_0").focus();
                $("#txtCredit_0").select();
            }

        }
        ModifyflgInst = 'Add';
        Modifyflg = 'Modify';
        return false;
    }
    else
    {
        alert("Sorry ! One record is already in Edit Mode !!");
        $("#ddlDRCR").val() == "D" ? $("#txtDebit_0").focus() : $("#txtCredit_0").focus();
        return false;
    }
}
edit_AccDeptVoucherDetail = function (h, VchSRL) {
  
    var B = {};
    B = window.voucherInfo.voucherForm;
    var ValAccOldSlCode = '';

    var VchMstData = {};
    var VchDetData = {};
    var VchInstData = {};
    var VchSLData = {};

    VchMstData = B.VoucherMast;
    VchDetData = B.VoucherDetail; alert(JSON.stringify(VchDetData));
   

    var ValVchDetailID = '';
    var VallblGlID = '';
    var VallblGlType = '';
    var VallblSlID = '';
    var VallblSubID = '';
    var ValOldSlCode = '';
    var ValAccDesc = '';
    var ValueDRCR = '';
    var ValueAmount = '';

    var SubID = '';
    var SubName = '';

    var AccDept = '';
    var AccDeptCode = '';
    var AccCostCenterCode = '';
    var AccCostCenter = '';


    for (var i = 0 ; i < VchDetData.length; i++) {
        if (VchSRL == VchDetData[i].VchSrl) {
            ValVchDetailID = VchDetData[i].VoucherDetailID;
            VallblGlID = VchDetData[i].GLID; 
            VallblGlType = VchDetData[i].GlType;
            VallblSlID = VchDetData[i].SLID;
            ValOldSlCode = VchDetData[i].OLDSLID;
            ValAccDesc = VchDetData[i].GLName;
            ValueDRCR = VchDetData[i].DRCR;
            ValueAmount = VchDetData[i].AMOUNT;

            VallblSubID = VchDetData[i].SubID;
            SubID = VchDetData[i].OLDSUBID;
            SubName = VchDetData[i].SUBDesc;

            AccDept = VchDetData[i].AccDept;
            AccDeptCode = VchDetData[i].AccDeptCode;
            AccCostCenterCode = VchDetData[i].AccCostCenterCode;
            AccCostCenter = VchDetData[i].AccCostCenter;


        }
    }

    $("#lblGlID").val(VallblGlID);
    $("#lblGlType").val(VallblGlType);
    $("#lblSlID").val(VallblSlID);
    $("#lblSubID").val(VallblSubID);

    $("#lblAccOldSlCode").val(ValOldSlCode);
    $("#lblAccDesc_0").val(ValAccDesc);
    $("#hdnlblAccCode").val(VallblGlID);
    $("#lblVoucherDetailID").val(ValVchDetailID);

    //$("#hdnAccDept").val(AccDeptCode);
    //$("#txtAccDept").val(AccDept);
    //$("#hdnCostCenter").val(AccCostCenterCode);
    //$("#txtCostCenter").val(AccCostCenter);

    if (ValueDRCR == 'D') {
        $("#ddlDRCR").val(ValueDRCR);
        $("#txtDebit_0").val(ValueAmount);
        $("#txtCredit_0").val('');
    }
    else {
        $("#ddlDRCR").val(ValueDRCR);
        $("#txtCredit_0").val(ValueAmount);
        $("#txtDebit_0").val('');
    }

    if (VallblSubID == '0000000000') {
        $("#lblOldSubCode").val('');
        $("#lblSubAccDesc_0").val('');
    }
    else {
        $("#lblOldSubCode").val(SubID);
        $("#lblSubAccDesc_0").val(SubName);
    }

    B.tmpVchInstType = [];
    B.tmpVchSubLedger = [];

    glvrno = VchSRL;
    var SectorID = B.VoucherMast.SectorID;
    var C = MAIN.Utils.fixArray(B.tmpVchInstType); //alert(JSON.stringify(C));   //FOR INSTRUMENT POPUP
    var S = MAIN.Utils.fixArray(B.tmpVchSubLedger); //alert(JSON.stringify(S));    //FOR SUBLEDGER POPUP 
    var rowid_C = 1;
    var rowid_S = 1;


    $.each(VchDetData, function (index, result) {
        if (result['VchSrl'] == glvrno) {
          
            $.each(result, function (index1, DataInst) {
               
                for (var k in DataInst) {
                  
                    if ($("#lblGlID").val() == DataInst[k].GLID) {
                       
                        C.push({
                            InstSrNo: rowid_C,
                            YearMonth: '',
                            BillID: DataInst[k].BillID,
                            VoucherType: $("#ddlVchType").val(),
                            VCHNo: DataInst[k].VCHNo,
                            GLID: $("#lblGlID").val(),
                            SLID: $("#lblSlID").val(),
                            DRCR: $("#ddlDRCR").val(),
                            SlipNo: DataInst[k].SlipNo,
                            InstType: DataInst[k].InstType,
                            InstTypeName: DataInst[k].InstTypeName,
                            InstNo: DataInst[k].InstNo,
                            InstDT: DataInst[k].InstDT,
                            Amount: DataInst[k].Amount,
                            ActualAmount: DataInst[k].Amount,
                            Drawee: DataInst[k].Drawee,
                            SectorID: DataInst[k].SectorID,
                            IsDelete: DataInst[k].IsDelete,
                            AccDeptCode: DataInst[k].AccDeptCode,
                            AccCostCenterCode: DataInst[k].AccCostCenterCode,
                            AccDept: DataInst[k].AccDept,
                            AccCostCenter: DataInst[k].AccCostCenter
                        });
                      
                        rowid_C = rowid_C + 1;
                        ShowInstDetail(C);
                    }
                }
            });
        }
    });

    $.each(VchDetData, function (index, result) {
        if (result['VchSrl'] == glvrno) {

            $.each(result, function (index1, DataInst) {

                for (var k in DataInst) {

                    if ($("#lblGlID").val() == DataInst[k].AccountCODE) {

                        S.push({
                            SLSlNo: rowid_S,
                            BillID: DataInst[k].BillID,
                            SubLedgerTypeID: DataInst[k].SubLedgerTypeID,
                            SubLedgerType: DataInst[k].SubLedgerType,
                            Number: DataInst[k].Number,
                            Amount: DataInst[k].Amount,
                            DRCR: DataInst[k].DRCR,
                            AccountCODE: DataInst[k].AccountCODE,
                            SubLedger: DataInst[k].SubLedger,
                            SubLedgerCODE: DataInst[k].SubLedgerCODE,
                            IsDelete: DataInst[k].IsDelete
                        });

                        rowid_S = rowid_S + 1;
                        ShowSubLedgerDetail(S);
                    }
                }
            });
        }
    });

  
    window.voucherInfo.findAndRemove(B.VoucherDetail, 'VchSrl', VchSRL);
    ShowVoucherDetail(B.VoucherDetail, "I");

   
        $("#dialog-AccDeptCostCenter").modal("show");

        if ($("#ddlDRCR").val() == 'D') {
            $("#txtCredit_0").prop("disabled", true);
            $("#txtDebit_0").prop("disabled", false).val();

            $("#txtDebit_0").focus();
            $("#txtDebit_0").select();
        }
        else {
            $("#txtDebit_0").prop("disabled", true);
            $("#txtCredit_0").prop("disabled", false).val();
            $("#txtCredit_0").focus();
            $("#txtCredit_0").select();
        }

    
    ModifyflgInst = 'Add';
    Modifyflg = 'Modify';
    return false;
}
editAccDeptCostCenter = function (h, VchSRL) {

    editVoucherDetail(h, VchSRL);



}
Change_DRCR_forAmount = function () {
    if($("#ddlDRCR").val() !='')
    {
        if ($("#hdnlblAccCode").val() != "")
        {
            DRCRChanged();
        }
    }
};
ShowModalPopupInstType = function () {
    SetVoucherDate();
   
    var VchType = $('#ddlVchType').val(); 
    if (VchType == "2")
        $('#ddlInstType').val('O');
    else
        $('#ddlInstType').val('');

    $('#dialog-Instrument').modal('show');
   
};
Get_Details = function (ID, VchSrl) {
   
    var AccDeptCode = $(ID).closest('tr').find('td:eq(7)').text();
    var AccDept = $(ID).closest('tr').find('td:eq(8)').text();
    var AccCostCenterCode = $(ID).closest('tr').find('td:eq(9)').text();
    var AccCostCenter = $(ID).closest('tr').find('td:eq(10)').text();

    var html = "";
    $("#tbl_2 tbody").empty();

    html += "<tr style='cursor:pointer' onclick='Edit_AccDeptCostCenter(\"" + VchSrl + "\")'>"
    html += "<td style='width: 10%; font-family:Verdana; text-align:right;' >" + VchSrl + "</td>"
    html += "<td style='width: 10%; font-family:Verdana; text-align:right; '>Account Department</td>"
    html += "<td style='width: 10%; font-family:Verdana; text-align:right;' >" + AccDept + "</td>"
    html + "</tr>";

    html += "<tr style='cursor:pointer' onclick='Edit_AccDeptCostCenter(\"" + VchSrl + "\")'>"
    html += "<td style='width: 10%; font-family:Verdana; text-align:right;' >" + VchSrl + "</td>"
    html += "<td style='width: 10%; font-family:Verdana; text-align:right; '>Cost Center</td>"
    html += "<td style='width: 10%; font-family:Verdana; text-align:right; '>" + AccCostCenter + "</td>"
    html + "</tr>";

    $("#tbl_2 tbody").append(html);
}
function Edit_AccDeptCostCenter(VchSrl)
{
    B = window.voucherInfo.voucherForm;
    alert(JSON.stringify(B.VoucherDetail));
    $("#hdnVchSrl").val(VchSrl);
    $("#btnAccDeptCostCenterOK").html("Update");
    $("#dialog-AccDeptCostCenter").modal("show");
}


ConfNo = function () {
    $("#dialog-confirms").hide();
};
ConfYes = function () {
    $('#dialog-confirms').hide();
    $('#dialog-Narration').show();
    $('#txtPopNarration').focus();

    $('#txtPopNarration').val($("#txtNarrationval").val());
};
NarrOK = function () {
    if ($('#txtPopNarration').val() != "")
    {
        $('#dialog-Narration').hide();
        SetMasterFormValues();
    }
    else
    {
        $('#txtPopNarration').focus(); return false;
    }
};
NarrCancel = function () {
    $('#txtPopNarration').val('');
    $('#dialog-Narration').hide();
};
CallSaveVoucher = function () {
   
    //var B = {};
    //B = window.voucherInfo.voucherForm;
    //var E = "{\'VoucherForm\':\'" + JSON.stringify(B).replace("'", "$") + "\'}";
    //var C = JSON.stringify(E);

    var B = {};
    B = window.voucherInfo.voucherForm;
    var C = JSON.stringify(B);
   
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/VoucherMaster/SaveVoucher",
        data: C,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            window.voucherInfo.voucherForm = D.Data;
            var B = window.voucherInfo.voucherForm;


            if (t.SessionExpired == "N") {
                if (t.EntryType == "I") {
                    $("#txtVoucherNo").val(t.VoucherMast.VoucherNo);
                    //alert("Voucher Saved Successfully. Voucher No is '" + t.VoucherMast.VoucherNo + '(' + t.VoucherMast.VCHNo + ')' + "'.");
                    //location.reload();
                    //Print_Voucher_AfterSaveUpdate(t.VoucherMast.VoucherNo, t.VoucherMast.VCHNo);

                    swal({
                        title: "Success",
                        text: "Voucher Saved Successfully. Voucher No is '" + t.VoucherMast.VoucherNo + '(' + t.VoucherMast.VCHNo + ')' + "'.",
                        type: "success",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: false,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                location.reload();
                                Print_Voucher_AfterSaveUpdate(t.VoucherMast.VoucherNo, t.VoucherMast.VCHNo);
                            }
                        });
                    return;
                }

                else if (t.EntryType == "E") {
                    if (t.SearchResult == "U") {
                        //alert(t.Message);
                        //location.reload();
                        //Print_Voucher_AfterSaveUpdate(t.VoucherNo, t.VoucherID);
                        swal({
                            title: "Success",
                            text: t.Message,
                            type: "success",
                            confirmButtonColor: "#AEDEF4",
                            confirmButtonText: "OK",
                            closeOnConfirm: false,
                        },
                            function (isConfirm) {
                                if (isConfirm) {
                                    location.reload();
                                    Print_Voucher_AfterSaveUpdate(t.VoucherMast.VoucherNo, t.VoucherMast.VCHNo);
                                }
                            });
                        return;
                    } else if (t.SearchResult == "F") {
                        alert(t.Message);
                        location.reload();
                        return;
                    } else if (t.SearchResult == "R") {
                        alert(t.Message);
                        location.reload();
                        return;
                    }
                }
            } else {
                alert("Your Session Expired. Login Again");
                location.reload();
                return;
            }


            //A.showEnquiryDetail(window.enquiryPage.enquiryForm.enquiryDetail);
            //A.ClearBookingDetails();
            //if (A.checkMasterDetail()) {
            //    $("#cmdSave").removeAttr("disabled");
            //} else {
            //    $("#cmdSave").attr("disabled", "disabled");
            //}

        },
        error: function (response) {
            var responseText;
            responseText = JSON.parse(response.responseText);
            alert("Error : " + responseText.Message);
            //alert('Some Error Occur');
            //$("#button-personal-details").prop("disabled", false);
        },
        failure: function (response) {
            alert(response.d);
        }
    });
};   //This is for Final Save
SaveClicked = function () {
    event.preventDefault();

    if (isEdit == false)
    {
        alert("Sorry ! One record is already in Edit Mode !!");
        $("#ddlDRCR").val() == "D" ? $("#txtDebit_0").focus() : $("#txtCredit_0").focus();
        return false;
    }

    var VchType = $("#ddlVchType").val();
    var VchDate = $("#txtVoucherDate").val();
    if (VchType == "") {
        alert('Please Select Voucher Type.'); $("#ddlVchType").focus(); return false;
    }
    if (VchDate == "") {
        alert('Please Enter Voucher Date.'); $("#txtVoucherDate").focus(); return false;
    }
    var B = window.voucherInfo.voucherForm;
    var SectorID = B.VoucherMast.SectorID;
    var C = MAIN.Utils.fixArray(B.VoucherDetail);
    var D = MAIN.Utils.fixArray(B.tmpVchInstType);
    var E = MAIN.Utils.fixArray(B.tmpVchFA); 
    if (C == "") {
        alert("Please Add Voucher Detail before Saving !!");
        $("#lblAccDesc_0").focus();
        return false;
    }
    var TotalDebit = 0;
    var TotalCredit = 0;
    for (var i in C) {
        if (C[i].IsDelete != 'Y') {
            if (C[i].DRCR == "D") {
                TotalDebit = (parseFloat(TotalDebit) + parseFloat(C[i].AMOUNT)).toFixed(2);
            } else if (C[i].DRCR == "C") {
                TotalCredit = (parseFloat(TotalCredit) + parseFloat(C[i].AMOUNT)).toFixed(2);
            }
        }
    }
    $("#txtTotalDebit").val(TotalDebit);
    $("#txtTotalCredit").val(TotalCredit);

    if (TotalCredit > 0 && TotalDebit > 0 && TotalCredit == TotalDebit) {
        ShowConfirmSaveDialog();
    } else {
        alert("Total Debit must be equal to Total Credit and Greater than zero(0). Cannot Save the Voucher.");
        return false;
    }
   
};
SetMasterFormValues = function () {

    var VoucherDate = $.trim($("#txtVoucherDate").val());
    //if ($.trim($("#txtVoucherDate").val()) != "") {
    //    var refDate = ($("#txtVoucherDate").val()).split('/');
    //    VoucherDate = refDate[2] + "-" + refDate[1] + "-" + refDate[0];
    //}

    var B = window.voucherInfo.voucherForm;
    var C = B.VoucherMast;
    var D = B.VoucherDetail;  

    C.NARRATION = $.trim($("#txtPopNarration").val()).toString().toUpperCase();
    C.YearMonth = VoucherDate; //$.trim($("#txtAccFinYear").val());   //FinYear
    C.VoucherType = $.trim($("#ddlVchType").val());
    C.VCHDATE = VoucherDate;
    C.STATUS = 'D';
    C.VCHAMOUNT = $.trim($("#txtTotalDebit").val());

    $("#txtNarrationval").val(C.NARRATION);

    var foundResult = false;
    for (var i in D) {
        if (D[i].DRCR == 'D' && D[i].GlType == 'B') { foundResult = true; } //FOR BANK RECEIPT
        if (D[i].DRCR == 'C' && D[i].GlType == 'B') { foundResult = true; } //FOR BANK PAYMENT

        if (D[i].DRCR == 'D' && D[i].GlType == 'C') { foundResult = true; } //FOR CASH RECEIPT
        if (D[i].DRCR == 'C' && D[i].GlType == 'C') { foundResult = true; } //FOR CASH PAYMENT
    }
    if ($("#ddlVchType").val() == 1 && foundResult == false) {
        alert("Sorry ! Please Select at least one Bank in DEBIT."); $('#txtPopNarration').val(''); $('#txtNarrationval').val(''); return false;
    }
    if ($("#ddlVchType").val() == 2 && foundResult == false) {
        alert("Sorry ! Please Select at least one Bank in CREDIT."); $('#txtPopNarration').val(''); $('#txtNarrationval').val(''); return false;
    }
    if ($("#ddlVchType").val() == 3 && foundResult == false) {
        alert("Sorry ! Please Select at least one Cash in DEBIT."); $('#txtPopNarration').val(''); $('#txtNarrationval').val(''); return false;
    }
    if ($("#ddlVchType").val() == 4 && foundResult == false) {
        alert("Sorry ! Please Select at least one Cash in CREDIT."); $('#txtPopNarration').val(''); $('#txtNarrationval').val(''); return false;
    }

   
    CallSaveVoucher();
};

// ======================================================= Instrument Type Details ==============================================================
InstrumentAdd = function () {

    if ($("#ddlInstType").val() == "") {
        alert("Please Select Instrument Type !!");
        $("#ddlInstType").focus();
        return false;
    }
  
    //if ($("#ddlVchType").val() != '2' && $("#ddlInstType").val() != 'O')
    if ($("#ddlInstType").val() == 'Q')
    {
     
        if ($("#txtInstNo").val() == "") {  // && ($("#lblGlType").val() == 'A' || $("#lblGlType").val() == 'B') && ($("#ddlInstType").val() != "N")
            alert("Please Enter Instrument Number !!");
            $("#txtInstNo").focus();
            return false;
        }
        if ($("#txtInstDate").val() == "") {
            alert("Please Enter Instrument Date !!");
            $("#txtInstDate").focus();
            return false;
        }
    }
    if ($("#txtInstAmount").val() <= 0) {
        alert("Please Enter Amount !!");
        $("#txtInstAmount").focus();
        return false;
    }
    if ($("#txtDBankBR").val() == "" && ($("#lblGlType").val() == 'A' || $("#lblGlType").val() == 'B')) {
        alert("Please Enter Drawee Bank or Branch");
        $("#txtDBankBR").focus();
        return false;
    }
    // ========= Start Code For Less 90 Days check instrument Date from voucher Date ========

    var VDate = $('#txtVoucherDate').val().split("/");
    var StrVchDate = VDate[2] + "-" + VDate[1] + "-" + VDate[0];
    var NewVchDate = new Date(StrVchDate);
    var LessInstdate = NewVchDate.setDate(NewVchDate.getDate() + parseInt(-90));
    var DtLessInstdate = new Date(LessInstdate); //compare les 90 days
    var SetInstDate = $('#txtInstDate').val().split("/");
    var CurrInstDate = SetInstDate[2] + "-" + SetInstDate[1] + "-" + SetInstDate[0];
    var DtCurrInstDate = new Date(CurrInstDate); //Compare Current Inst Date

    if (DtCurrInstDate < DtLessInstdate) {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("Instrument Date Exced 90 Days's! Will You Proceed Further (Y/N)?")) {
            confirm_value.value = "Yes";

        } else {
            confirm_value.value = "No";
        }

        if (confirm_value.value == "Yes") {
            $('#txtInstAmount').focus();
            $('#txtInstAmount').select();
        }
        if (confirm_value.value == "No") {
            $('#txtInstDate').val('');
            $('#txtInstDate').focus();
            return false;
        }
    } // Main If Close

    // ========= End Code For Less 90 Days check instrument Date from voucher Date ========
    var InstDate = $("#txtInstDate").val();

    var B = window.voucherInfo.voucherForm;
    var SectorID = B.VoucherMast.SectorID;
    var C = MAIN.Utils.fixArray(B.tmpVchInstType);

    var instsrno = C.length + 1;
    C.push({
        InstSrNo: instsrno,
        YearMonth: "", //$("#txtYearMonth").val(),
        BillID: $("#lblInstBillID").val(),
        VoucherType: $("#ddlVchType").val(),
        VCHNo: 0,
        GLID: $("#lblGlID").val(),
        SLID: $("#lblSlID").val(),
        DRCR: $("#ddlDRCR").val(),
        SlipNo: '',   //$("#txtSlipNo").val()
        InstType: $("#ddlInstType").val(),
        InstTypeName: $("#ddlInstType option:selected").text(),
        InstNo: $("#txtInstNo").val(),
        InstDT: InstDate,
        Amount: $("#txtInstAmount").val(),
        ActualAmount: $("#txtInstAmount").val(),
        Drawee: $("#txtDBankBR").val().toString().toUpperCase(),
        SectorID: SectorID,
        AccDeptCode: $("#hdnAccDept").val(),
        AccCostCenterCode: $("#hdnCostCenter").val(),
        AccDept: $("#txtAccDept").val(),
        AccCostCenter: $("#txtCostCenter").val(),
        IsDelete: null
    });

    //alert(JSON.stringify(C));

    ShowInstDetail(C);
    ClearInstText();
    CallModalConfirm();
    ModifyflgInst = 'Add';
}
ShowInstDetail = function (C) {
    var html = ""
 
    if (C.length > 0) {
        
        for (var i in C) {
            var isDelete = C[i].IsDelete;
            if (isDelete != 'Y') {
                html += "<tr class='all_Inst' id='trInst_" + C[i].InstSrNo + "'>"
                    + "<td id='tdInst_" + C[i].InstSrNo + "' style='text-align:center;'><img src='/Content/Images/Delete.png' title='Delete' id='deleteInstID_" + C[i].InstSrNo + "' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='removeInstDetail(" + C[i].InstSrNo + ");' /></td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center;'>" + C[i].InstTypeName + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center;'>" + (C[i].InstNo) + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center;'>" + (C[i].InstDT) + "</td>"
                    + "<td  style='border:solid 1px black;text-align:right;font-family:Verdana; text-align:center;'>" + (C[i].Amount) + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; '>" + (C[i].Drawee) + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center; display:none;'>" + C[i].BillID + "</td>"
                     + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center; display:none;'>" + C[i].GLID + "</td>"
                + "<td id='tdInst_" + C[i].InstSrNo + "' style='text-align:center;'><img src='/Content/Images/Edit.png' title='Edit' id='EditInstID_" + C[i].InstSrNo + "' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='EditInstDetail(this, \"" + C[i].InstSrNo + "\");' /></td>"
                html + "</tr>";
            }
        }
    }


    $(".tblInst").find("tr:gt(1)").remove();
    $(".tblInst").append(html);
};
ClearInstText = function () {
    $("#txtSlipNo").val("");
    $("#ddlInstType").val("");
    $("#txtInstNo").val("");
    $("#txtInstDate").val("")
    $("#txtInstAmount").val("")
    $("#txtDBankBR").val("");
    $("#lblInstBillID").val('');
};
CallModalConfirm = function () {
    $("#lblInstMSG").html("Do you want to add any more instrument type ?");
    $("#dialog-Instconfirms").fadeToggle(1000);
    $("#btnConfYes").focus();
};
InstConfYes = function () {
   
    $("#dialog-Instconfirms").hide();
    $("#ddlInstType").focus();
    $("#txtInstAmount").val($("#txtInstAmount").val());

    if ($("#ddlDRCR").val() == "D") {
        var B = window.voucherInfo.voucherForm;
        var C = MAIN.Utils.fixArray(B.tmpVchInstType);
        var InstAmount = 0;
        for (var i in C) {
            if (C[i].IsDelete != 'Y')
            InstAmount = (parseFloat(InstAmount) + parseFloat(C[i].Amount)).toFixed(2);
        }
        var TotalDebt = ($("#txtTotalDebit").val() == "" ? 0 : parseFloat($("#txtTotalDebit").val()));  //yes
        var TotalCret = ($("#txtTotalCredit").val() == "" ? 0 : parseFloat($("#txtTotalCredit").val()));
        var BalanceAmount = TotalCret - TotalDebt - InstAmount;
        if (BalanceAmount <= 0) {
            $("#txtInstAmount").val(0);
        }
        else {
            $("#txtInstAmount").val(BalanceAmount);
        }

    } else if ($("#ddlDRCR").val() == "C") {

        var B = window.voucherInfo.voucherForm;
        var C = MAIN.Utils.fixArray(B.tmpVchInstType);
        var InstAmount = 0;
        for (var i in C) {
            if (C[i].IsDelete != 'Y')
            InstAmount = (parseFloat(InstAmount) + parseFloat(C[i].Amount)).toFixed(2);
        }

        var TotalDebt1 = ($("#txtTotalDebit").val() == "" ? 0 : parseFloat($("#txtTotalDebit").val()));  //sm
        var TotalCret1 = ($("#txtTotalCredit").val() == "" ? 0 : parseFloat($("#txtTotalCredit").val()));
        var BalanceAmount1 = TotalDebt1 - TotalCret1 - InstAmount;
        if (BalanceAmount1 <= 0) {
            $("#txtInstAmount").val(0);
        }
        else {
            $("#txtInstAmount").val(BalanceAmount1);
        }
    }
};
InstConfNo = function () {
    $("#dialog-Instconfirms").hide();

    $("#dialog-Instrument").modal('hide');
    CallculateInstAmount();
};
CallculateInstAmount = function () {
  
    var B = window.voucherInfo.voucherForm;
   
    var SectorID = B.VoucherMast.SectorID;
   
    var C = MAIN.Utils.fixArray(B.tmpVchInstType);
   
    var InstAmount = 0;
    for (var i in C) {
        if (C[i].IsDelete != 'Y') {
            InstAmount = (parseFloat(InstAmount) + parseFloat(C[i].Amount)).toFixed(2);
        }
    }
    //$("#dialogInstrumentType").dialog('close');
    $("#dialogInstrumentTypes").hide();
    $("#txtCredit_0").val("");
    $("#txtDebit_0").val("");
    if ($("#ddlDRCR").val() == "D") {

        $("#txtCredit_0").prop("disabled", true);
        //$("#txtDebit_0").prop("disabled", false).val(parseFloat(InstAmount).toFixed(2));
        $("#txtDebit_0").prop("disabled", false).val(InstAmount);
        $("#txtDebit_0").focus();

    } else if ($("#ddlDRCR").val() == "C") {

        $("#txtDebit_0").prop("disabled", true);
        //$("#txtCredit_0").prop("disabled", false).val(parseFloat(InstAmount).toFixed(2));
        $("#txtCredit_0").prop("disabled", false).val(InstAmount);
        $("#txtCredit_0").focus();
    }
};
removeInstDetail = function (InstSrno) {
    var res = confirm('Are You Sure want to Delete this Record ??');
    if (res == true) {
        var B = {};
        B = window.voucherInfo.voucherForm;

        var a = window.voucherInfo.findAndSetInstDeleteFlag(B.tmpVchInstType, InstSrno);
        //window.voucherInfo.findAndRemove(B.tmpVchInstType, 'InstSrNo', InstSrno);

        ShowInstDetail(B.tmpVchInstType);
        $("#ddlInstType").focus();
        return false;
    }
};
EditInstDetail = function (h, InstSrno) {

    var B = {};
    B = window.voucherInfo.voucherForm;

    var ValInstBillID = '';
    var ValType = '';
    var ValInstType = '';
    var ValSlipNo = '';
    var ValNumber = '';
    var ValDate = '';
    var ValAmount = '';
    var ValBank = '';
    ValType = $(h).closest('tr').find('td:eq(1)').text(); 
    ValNumber = $(h).closest('tr').find('td:eq(2)').text();
    ValDate = $(h).closest('tr').find('td:eq(3)').text();
    ValAmount = $(h).closest('tr').find('td:eq(4)').text();
    ValBank = $(h).closest('tr').find('td:eq(5)').text();
    ValInstBillID = $(h).closest('tr').find('td:eq(6)').text();

    $('#ddlInstType option').filter(function () {
        var textValue = $.trim($(this).text());
        var textSearch = $.trim(ValType);

        if (textValue == textSearch)
            return $.trim(textValue) == textSearch;
    }).prop('selected', 'selected');

    $("#lblInstBillID").val(ValInstBillID);
    $("#txtInstNo").val(ValNumber);
    $("#txtInstDate").val(ValDate);
    $("#txtInstAmount").val(ValAmount);
    $("#txtDBankBR").val(ValBank);

    window.voucherInfo.findAndRemove(B.tmpVchInstType, 'InstSrNo', InstSrno);
    ShowInstDetail(B.tmpVchInstType);
    $("#ddlInstType").focus();
    ModifyflgInst = 'Modify';
    return false;

};
edit_INstAccDeptCostCenter = function (h, VchSRL) {
    var BillID = $(h).closest('tr').find('td:eq(6)').text();
    var ValAccCode = $(h).closest('tr').find('td:eq(7)').text();
    $('#hdnVchDtlID').val(ValType);
    $('#hdnAccCode').val(ValAccCode);








    //var E = "{BillID: '" + BillID + "', VoucherNo: '" + $('#hdnVoucherNo').val() + "', AccCode: '" + $('#hdnAccCode').val() + "' , Condition: '" + 'select' + "'," +
    //    "DeptCode: '" + $('#hdnAccDept').val() + "', CostCenterCode: '" + $('#hdnCostCenter').val() + "'}";
    //$.ajax({
    //    type: "POST",
    //    url: "/Accounts_Form/VoucherMaster/Get_InstDeptCodeCostCenterCode",
    //    data: E,
    //    contentType: "application/json; charset=utf-8",
    //    success: function (D) {
    //        var t = D.Data;
    //        if (t.length > 0 && D.Status == 200) {
    //            $('#txtAccDept').val(t[0].DeptDesc);
    //            $('#hdnAccDept').val(t[0].DeptCode);
    //            $('#txtCostCenter').val(t[0].CostCenterDesc);
    //            $('#hdnCostCenter').val(t[0].CostCenterCode);
    //            $("#dialog-AccDeptCostCenter").modal("show");
    //        }
    //        else
    //            $("#dialog-AccDeptCostCenter").modal("show");
    //    }
    //});



}
CallInstypeBlur = function () {
   
    if ($("#ddlInstType").val() != "") {
        var InstType = $("#ddlInstType").val(); 


        var B = window.voucherInfo.voucherForm;
        var D = MAIN.Utils.fixArray(B.tmpVchInstType);
        //Shrawan
        if (InstType == "O" || InstType == "H" || InstType == "R" || InstType == "E") {
            if (ModifyflgInst == 'Add') {
                var i = D.length + 1;
                var instSrl = leftpad(i.toString(), 4, '0');
                
                var YearMonth = Get_YearMonth();
                var InstNo = (YearMonth + $("#lblAccOldSlCode").val() + instSrl);

                if (InstType != 'O')
                $("#txtInstNo").val(InstNo);

                $("#txtInstDate").val($("#txtVoucherDate").val());
                //$("#txtInstAmount").focus();
                return false;
            }
        } else if (InstType == "P") {   //else if (InstType == "P" || InstType == "H") {
            if (ModifyflgInst == 'Add') {
                $("#txtInstNo").val("");
                $("#txtInstDate").val($("#txtVoucherDate").val());
                return false;
                //$("#txtInstAmount").focus();
            }
        } else {
            if (ModifyflgInst == 'Add') {
                $("#txtInstNo").val("");
                $("#txtInstDate").val($("#txtVoucherDate").val());
                //$("#txtSlipNo").focus();
                return false;
            }
        }
    } else {
        alert("Please instrument type");
        return false;
    }
}
InstTypeKeyPress = function (evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
    if (iKeyCode == 13) {

        //SetVoucherDate();
        CallInstypeBlur();
    }
    return false;
};
ddlInstTypeKeyPress = function (evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
    var InstType = $("#ddlInstType").val();
    //var B = window.voucherInfo.voucherForm;
    //var D = WBFC.Utils.fixArray(B.tmpVchInstType);
    if (iKeyCode == 13 || iKeyCode == 9) {
        if (InstType == "O" || InstType == "H" || InstType == "R" || InstType == "E") {
            $("#txtInstAmount").focus();
            $("#txtInstAmount").select();
            return false;
        } else if (InstType == "P") {   //else if (InstType == "P" || InstType == "H") {
            $("#txtInstAmount").focus();
            $("#txtInstAmount").select();
        } else {
            $("#txtSlipNo").focus();
            $("#txtSlipNo").select();
            return false;
        }
    }
    //return false;
};
ddlInstTypekeydown = function (evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
    var InstType = $("#ddlInstType").val();
    if (iKeyCode == 9) {
        if (InstType == "O" || InstType == "H" || InstType == "R") {
            $("#txtInstAmount").focus();
            $("#txtInstAmount").select();
            //SetVoucherDate();
            return false;
        } else if (InstType == "P") {   //else if (InstType == "P" || InstType == "H") {
            $("#txtInstAmount").focus();
            $("#txtInstAmount").select();
            //SetVoucherDate();
            return false;
        } else {
            $("#txtSlipNo").focus();
            $("#txtSlipNo").select();
            //SetVoucherDate();
            return false;
        }
    }

    if (iKeyCode == 67) {
        if ($("#ddlInstType").val() == 'C') {
            $("#ddlInstType").val('H');
            CallInstypeBlur();
        }
        else { $("#ddlInstType").val('C'); CallInstypeBlur(); }
    }

    if (iKeyCode == 68) {
        $("#ddlInstType").val('D');
        CallInstypeBlur();
    }

    if (iKeyCode == 69) {
        $("#ddlInstType").val('E');
        CallInstypeBlur();
    }

    if (iKeyCode == 72) {
        $("#ddlInstType").val('H');
        CallInstypeBlur();
    }

    if (iKeyCode == 73) {
        $("#ddlInstType").val('I');
        CallInstypeBlur();
    }

    if (iKeyCode == 78) {
        $("#ddlInstType").val('N');
        CallInstypeBlur();
    }

    if (iKeyCode == 79) {
        $("#ddlInstType").val('O');
        CallInstypeBlur();
    }
    if (iKeyCode == 80) {
        $("#ddlInstType").val('P');
        CallInstypeBlur();
    }

    if (iKeyCode == 82) {
        $("#ddlInstType").val('R');
        CallInstypeBlur();
    }

};
CloseInstrumentType = function () {
   
    $("#dialog-Instrument").modal('hide');
    CallculateInstAmount();
}

// ======================================================= Subledger Type Details ==============================================================
SubLedgerAdd = function () {

    if ($("#hdnSubLedgerCode").val() == "") {
        $("#txtSubLedger").focus();
        return false;
    }

    if ($("#ddlSubLedgerType").val() == "") {
        $("#ddlSubLedgerType").focus();
        return false;
    }
   
    //if ($("#ddlSubLedgerType").val() == "O" && $("#txtSubLedNo").val() == "") {
    //    $("#txtSubLedNo").focus();
    //    return false;
    //}

    if ($("#ddlSubLedgerType").val() == "N" && $("#txtSubLedNo").val() == "") {
        $("#txtSubLedNo").focus();
        return false;
    }

    //var VoucherTypeID = $("#ddlVchType").val(); 
    //if (Maintain_ItemSubLedger == "I" && (VoucherTypeID == 7 || VoucherTypeID == 8))
    //{
    //    if ($("#txtItemQty").val() == "") {
    //        $("#txtItemQty").focus();
    //        return false;
    //    }
    //    if ($("#txtItemRate").val() == "") {
    //        $("#txtItemRate").focus();
    //        return false;
    //    }
    //}

    if ($("#txtSubLedAmount").val() == "" || $("#txtSubLedAmount").val() == 0) {
        $("#txtSubLedAmount").focus();
        return false;
    }

    var B = window.voucherInfo.voucherForm;
    var C = MAIN.Utils.fixArray(B.tmpVchSubLedger);

        var srno = C.length + 1;
        C.push({
            SLSlNo: srno,
            BillID: $("#lblSLBillID").val(), 
            SubLedgerTypeID: $("#ddlSubLedgerType").val(),
            SubLedgerType: $('#ddlSubLedgerType').find('option:selected').text(),
            Number: $("#txtSubLedNo").val(),
            Qty: $("#txtItemQty").val(),
            IRate: $("#txtItemRate").val(),
            IsItemDetail: "",
            Amount: $("#txtSubLedAmount").val(),
            DRCR: $("#ddlDRCR").val(),
            SubLedger: $("#txtSubLedger").val(),
            AccountCODE: $("#hdnlblAccCode").val(),
            SubLedgerCODE: $("#hdnSubLedgerCode").val(),
            IsDelete:null
        });
      
    ShowSubLedgerDetail(C);
    ClearSubLedgerText();
    Call_SubLedger_ModalConfirm();
    ModifyflgSL = 'Add';
   
}
ShowSubLedgerDetail = function (C) {
   
    var html = ""; var isItemDetail='';
    var VoucherTypeID = $("#ddlVchType").val();
    if (C.length > 0) {
       
        for (var i in C) {
            var isDelete = C[i].IsDelete;
            if (isDelete != 'Y') {
              
                html += "<tr class='SubLedger' id='trSL_" + C[i].SLSlNo + "'>"
                    + "<td id='tdSL_" + C[i].SLSlNo + "' style='text-align:center;'><img src='/Content/Images/Delete.png' title='Delete' id='deleteSLID_" + C[i].SLSlNo + "' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='removeSLDetail(" + C[i].SLSlNo + ");' /></td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center;'>" + (C[i].SubLedger) + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center; display:none;'>" + (C[i].SubLedgerCODE) + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center; display:none;'>" + C[i].SubLedgerTypeID + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center;' class='isSubItemType'>" + C[i].SubLedgerType + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center;'>" + (C[i].Number) + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:right;' class='m_itemdetail'>" + (C[i].Qty) + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:right;' class='m_itemdetail'>" + (C[i].IRate) + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:right;'>" + (C[i].Amount) + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:right; display:none;'>" + (C[i].BillID) + "</td>"
                + "<td id='tdSL_" + C[i].SLSlNo + "' style='text-align:center;'><img src='/Content/Images/Edit.png' title='Edit' id='EditSLID_" + C[i].SLSlNo + "' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='EditSLDetail(this, \"" + C[i].SLSlNo + "\");' /></td>"
                html + "</tr>";
            }
        }
    }
   
    $(".tblSL").find("tr:gt(1)").remove();
    $(".tblSL").append(html);
    Maintain_ItemDetail();
};
ShowModalPopup_SubLedger = function () {
    //SetVoucherDate();   // jQuery.noConflict();

   
    $('#dialog-SubLedger').modal('show');
    $('#txtSubLedger').css('background-color', '#DAD8D6');
    $('#txtSubLedger').focus();
   
};
ClearSubLedgerText = function () {
    $("#ddlSubLedgerType").val("");
    $("#txtSubLedNo").val("");
    $("#txtSubLedAmount").val("");
    $("#txtSubLedger").val("")
    $("#hdnSubLedgerCode").val("");
    $("#lblSLBillID").val("");
    $("#txtItemQty").val("");
    $("#txtItemRate").val("");
};
Call_SubLedger_ModalConfirm = function () {
    $("#lblSLMSG").html("Do you want to add any more Details for SubLedger ?");
    $("#dialog-SLconfirms").fadeToggle(1000);
    $("#btnSLConfYes").focus();
};
SLConfYes = function () {

    $("#dialog-SLconfirms").hide();
    $("#ddlSubLedgerType").focus();
    $("#txtSubLedAmount").val($("#txtSubLedAmount").val());

    if ($("#ddlDRCR").val() == "D") {
        var B = window.voucherInfo.voucherForm;
        var C = MAIN.Utils.fixArray(B.tmpVchSubLedger);
        var SLAmount = 0;
        for (var i in C) {
            if (C[i].IsDelete != 'Y')
            SLAmount = (parseFloat(SLAmount) + parseFloat(C[i].Amount)).toFixed(2);
        }
        var TotalDebt = ($("#txtTotalDebit").val() == "" ? 0 : parseFloat($("#txtTotalDebit").val()));  //yes
        var TotalCret = ($("#txtTotalCredit").val() == "" ? 0 : parseFloat($("#txtTotalCredit").val()));
        var BalanceAmount = TotalCret - TotalDebt - SLAmount;
        if (BalanceAmount <= 0) {
            $("#txtSubLedAmount").val(0);
        }
        else {
            $("#txtSubLedAmount").val(BalanceAmount);
        }

    } else if ($("#ddlDRCR").val() == "C") {

        var B = window.voucherInfo.voucherForm;
        var C = MAIN.Utils.fixArray(B.tmpVchSubLedger);
        var SLAmount = 0;
        for (var i in C) {
            if (C[i].IsDelete != 'Y')
            SLAmount = (parseFloat(SLAmount) + parseFloat(C[i].Amount)).toFixed(2);
        }

        var TotalDebt1 = ($("#txtTotalDebit").val() == "" ? 0 : parseFloat($("#txtTotalDebit").val()));  //sm
        var TotalCret1 = ($("#txtTotalCredit").val() == "" ? 0 : parseFloat($("#txtTotalCredit").val()));
        var BalanceAmount1 = TotalDebt1 - TotalCret1 - SLAmount;
        if (BalanceAmount1 <= 0) {
            $("#txtSubLedAmount").val(0);
        }
        else {
            $("#txtSubLedAmount").val(BalanceAmount1);
        }
    }
};
SLConfNo = function () {
    $("#dialog-SLconfirms").hide();

    $("#dialog-SubLedger").modal('hide');
    CallculateSLAmount();
};
CallculateSLAmount = function () {
    
    var B = window.voucherInfo.voucherForm;
    var C = MAIN.Utils.fixArray(B.tmpVchSubLedger);
   
    var SLAmount = 0;
    for (var i in C) {
        if (C[i].IsDelete !='Y')
        SLAmount = (parseFloat(SLAmount) + parseFloat(C[i].Amount)).toFixed(2);
    }
   
    $("#txtCredit_0").val("");
    $("#txtDebit_0").val("");
    if ($("#ddlDRCR").val() == "D") {

        $("#txtCredit_0").prop("disabled", true);
        //$("#txtDebit_0").prop("disabled", false).val(parseFloat(InstAmount).toFixed(2));
        $("#txtDebit_0").prop("disabled", false).val(SLAmount);
        $("#txtDebit_0").focus();
    } else if ($("#ddlDRCR").val() == "C") {

        $("#txtDebit_0").prop("disabled", true);
        //$("#txtCredit_0").prop("disabled", false).val(parseFloat(InstAmount).toFixed(2));
        $("#txtCredit_0").prop("disabled", false).val(SLAmount);
        $("#txtCredit_0").focus();
    }
    $(".tblSL").find("tr:gt(1)").remove();
};
CloseSubLedgerType = function () {

    $("#dialog-SubLedger").modal('hide');
    CallculateSLAmount();


}
removeSLDetail = function (SLSrno) {
    var res = confirm('Are You Sure want to Delete this Record ??');
    if (res == true) {
        var B = {};
        B = window.voucherInfo.voucherForm;

        var a = window.voucherInfo.findAndSetSLDeleteFlag(B.tmpVchSubLedger, SLSrno);
        //window.voucherInfo.findAndRemove(B.tmpVchSubLedger, 'SLSlNo', SLSrno);

        ShowSubLedgerDetail(B.tmpVchSubLedger);
        $("#ddlSubLedgerType").focus();
        return false;
    }
};
EditSLDetail = function (h, SLSrno) {

    var B = {};
    B = window.voucherInfo.voucherForm;

    var SLTypeID = $(h).closest('tr').find('td:eq(3)').text();
    var Number = $(h).closest('tr').find('td:eq(5)').text();
    var Amount = $(h).closest('tr').find('td:eq(8)').text();
    var SubLedger = $(h).closest('tr').find('td:eq(1)').text(); 
    var SubLedgerCode = $(h).closest('tr').find('td:eq(2)').text();
    var BillID = $(h).closest('tr').find('td:eq(9)').text();
    var Qty = $(h).closest('tr').find('td:eq(6)').text();
    var Rate = $(h).closest('tr').find('td:eq(7)').text();

    $("#ddlSubLedgerType").val(SLTypeID);
    $("#txtSubLedNo").val(Number);
    $("#txtSubLedAmount").val(Amount);
    $("#txtSubLedger").val(SubLedger);
    $("#hdnSubLedgerCode").val(SubLedgerCode);
    $("#lblSLBillID").val(BillID);
    $("#txtItemQty").val(Qty);
    $("#txtItemRate").val(Rate);
    
  
    window.voucherInfo.findAndRemove(B.tmpVchSubLedger, 'SLSlNo', SLSrno);
  
 

    ShowSubLedgerDetail(B.tmpVchSubLedger);
    $("#ddlSubLedgerType").focus();
    ModifyflgSL = 'Modify';
    return false;

};
AdjustmentNumber = function () {
    var drcr = $("#ddlDRCR").val();
    var type = $("#ddlSubLedgerType").val();
    var SubCode = $("#hdnSubLedgerCode").val();

   
    if (type == "A") {
        if (drcr == "C") {
            $("#txtSubLedNo").prop("disabled", true);
            if (SubCode != "") {
                var E = "{SubCode: '" + $("#hdnSubLedgerCode").val() + "'}";
                $.ajax({
                    type: "POST",
                    url: "/Accounts_Form/VoucherMaster/AdjustmentNumber",
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        var t = D.Data;

                        $("#tblAdjustmentNo tbody").empty();
                        if (t.length > 0 && D.Status == 200) {
                            for (var i = 0; i < t.length; i++) {
                                $("#tblAdjustmentNo").append("<tr style='cursor:pointer;' onclick='Get_AdjstNo(\"" + t[i].AdjustmentNumber + "\")'>" +
                                    "<td style='display:none;'>" + t[i].Billid + "</td>" +
                                    "<td style='text-align:center; width:30%''>" + t[i].AdjustmentNumber + "</td>" +
                                    "<td style='text-align:right; width:20%'>" + t[i].AdjustmentAmount + "</td>" +
                                    "</tr>");
                            }
                           
                            $("#dialog-AdjustNumber").modal('show');
                        }
                    }
                });
            }
            else {
                $("#txtSubLedger").focus(); $("#ddlSubLedgerType").val('');
            }
        }
        else {
            alert('Sorry ! You can not Adjust in DEBIT.'); $("#ddlSubLedgerType").val(''); return false;
        }
    }
    else
        $("#txtSubLedNo").prop("disabled", false);

}
Get_AdjstNo = function (AdjustmentNumber)
{
    $("#dialog-AdjustNumber").modal('hide');
    $("#txtSubLedNo").val(AdjustmentNumber);
  
}

OnPage_Load = function () {
    AccountRule();
    if ($("#hdnGlobal_VoucherID").val() != "") {
        var E = "{VoucherTypeID: '" + "" + "', VoucherDate: '" + "" + "', VoucherNo: '" + $("#hdnGlobal_VoucherID").val() + "', AccFinYear: '" + $.trim($("#txtAccFinYear").val()) + "',  SectorID: '" + $.trim($("#ddlSector").val()) + "'}";
        //alert(E);
        $.ajax({
            type: "POST",
            url: "/Accounts_Form/VoucherMaster/Get_VoucherDetails",
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data; 


                window.voucherInfo.voucherForm = t;
                var B = window.voucherInfo.voucherForm;

                var M = MAIN.Utils.fixArray(B.VoucherMast); 
                var C = MAIN.Utils.fixArray(B.VoucherDetail);
                var D = MAIN.Utils.fixArray(B.tmpVchInstType);
                var S = MAIN.Utils.fixArray(B.tmpVchSubLedger);

                $("#ddlVchType").val(M[0].VoucherType);
                $("#txtVoucherDate").val(M[0].VCHDATE);

                $("#txtVoucherNo").val(M[0].VoucherNo);
                $("#hdnVoucherNo").val(M[0].VCHNo);

                $("#txtNarrationval").val(M[0].NARRATION);
                ShowVoucherDetail(C, "I");
                $('#dialog-confirms').hide();

                if (M[0].STATUS == "F")
                    $('#btnSave').prop("disabled", true);
                else
                    $('#btnSave').prop("disabled", false);
            }
        });
    }
    if (MaintainAccCode == "N")
    {
        $('#grd_1').addClass("isAccountHead");
        $('#grd_2').addClass("isAccountHead");
        $('#grd_3').attr("colspan", 1);
    }
    else
    {
        $('#grd_1').removeClass("isAccountHead");
        $('#grd_2').removeClass("isAccountHead");
        $('#grd_3').attr("colspan", 2);
    }
}
Print_Voucher = function () {

    if ($("#hdnVoucherNo").val() == "")
    {
        alert('Please, First Select Voucher !!'); $("#txtVoucherNo").focus(); return false;
    }

    if ($("#txtVoucherDate").val() == "") {
            alert("Please Select Voucher Date !!"); $("#txtVoucherDate").focus(); return false;
    }

    if ($("#txtVoucherDate").val() != "") {
        var isFrom = isValidDate($("#txtVoucherDate").val());
        if (isFrom == false) {
            alert("Please Select Valid Voucher Date !!"); $("#txtVoucherDate").focus(); return false;
        }
    }

   // $("#btnPrint").attr('href', "/Accounts_Form/VoucherMaster/Generate_Report?SectorID=" + $("#ddlSector").val() + "&VoucherNo=" + $("#hdnVoucherNo").val() + "");

    var ControllerName = "VoucherMaster"; //FormName
    var ReportName = "Voucher";
    var ReportType = "Voucher Master";

    E = "{FromDate:'" + $.trim($("#txtVoucherDate").val()) + "', ToDate:'" + $.trim($("#txtVoucherDate").val()) + "', VoucherNo:'" + $.trim($("#hdnVoucherNo").val()) + "', " +
        "FormName:'" + ControllerName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "'}";

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "VoucherCash.rpt",
        FileName: $("#txtVoucherNo").val()

    });

    detail.push({
        VoucherNumber: $("#hdnVoucherNo").val(),
        sectorid: $("#ddlSector").val()
    });

    final = {
        Master: master,
        Detail: detail
    }

    // $.ajax({
    // type: "POST",
    // url: "/Accounts_Form/VoucherMaster/Generate_Report",
    // data: E,
    // contentType: "application/json; charset=utf-8",
    // dataType: "json",
    // success: function (D) {
    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
    popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
    // }
    // });
}
Print_Voucher_AfterSaveUpdate = function (VoucherNo, VoucherID) {

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "VoucherCash.rpt",
        FileName: VoucherNo
    });

    detail.push({
        VoucherNumber: VoucherID,
        sectorid: $("#ddlSector").val()
    });

    final = {
        Master: master,
        Detail: detail
    }


    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
    popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();

}
//============================================================ Account Debt ==============================================================
Show_AccountDebt = function () {
    if (MaintainCostCenter == "Y" || MaintainDeptCode == "Y") {
        $("#lblAccDebtMSG").html("Do you want to add Account Department / Cost Center ?");
        $("#dialog-AccDebt").fadeToggle(1000);
        $("#btnAccDebtConfNo").focus();
    }
}
Acc_DebtConfYes = function () {
    if (MaintainCostCenter == "Y" || MaintainDeptCode == "Y") {

        $("#dialog-AccDebt").hide();
        $('.all_DeptCode').remove();

        $("#dialog-AccDeptCostCenter").modal("show");
        $("#txtAccDept").focus();
        $('#txtAccDept').css('background-color', '#DAD8D6');
    }
}
Acc_DebtNo = function () {
    $("#dialog-AccDebt").hide();

    if ($("#ddlDRCR").val() == "D") {
        $("#txtCredit_0").prop("disabled", true);
        $("#txtDebit_0").prop("disabled", false);
        $("#txtDebit_0").prop("disabled", false).val(parseFloat(GetDebitCreditBal()).toFixed(2));
        $("#txtDebit_0").focus();
        $("#txtDebit_0").select();
    } else if ($("#ddlDRCR").val() == "C") {
        $("#txtDebit_0").prop("disabled", true);
        $("#txtCredit_0").prop("disabled", false).val(parseFloat(GetDebitCreditBal()).toFixed(2));
        $("#txtCredit_0").focus();
        $("#txtCredit_0").select();
    }
} 
Acc_DebtAdd = function () {

    if ($("#hdnAccDept").val() == "")
    {
        $("#txtAccDept").focus(); return false;
    }
    if ($("#hdnCostCenter").val() == "") {
        $("#txtCostCenter").focus(); return false;
    }
    if ($("#txtDeptCodeAmt").val() == "") {
        $("#txtDeptCodeAmt").focus(); return false;
    }
   
    var B = window.voucherInfo.voucherForm;
    var SectorID = B.VoucherMast.SectorID;
    var C = MAIN.Utils.fixArray(B.tmpVchInstType);

        var instsrno = C.length + 1;
        C.push({
            InstSrNo: instsrno,
            YearMonth: "", //$("#txtYearMonth").val(),
            BillID: $("#lblInstBillID").val(),
            VoucherType: $("#ddlVchType").val(),
            VCHNo: 0,
            GLID: $("#lblGlID").val(),
            SLID: $("#lblSlID").val(),
            DRCR: $("#ddlDRCR").val(),
            SlipNo: '',   //$("#txtSlipNo").val()
            InstType: "",
            InstTypeName: "",
            InstNo: "",
            InstDT: "",
            Amount: $("#txtDeptCodeAmt").val(),
            ActualAmount: $("#txtDeptCodeAmt").val(),
            Drawee: "",
            SectorID: SectorID,
            AccDeptCode: $("#hdnAccDept").val(),
            AccCostCenterCode: $("#hdnCostCenter").val(),
            AccDept: $("#txtAccDept").val(),
            AccCostCenter: $("#txtCostCenter").val(),
            IsDelete: null
        });
  
    ShowDeptCodeDetail(C);
    ClearDeptCodeText();
    Call_ModelConfrimation();
    ModifyflgInst = 'Add';
    return false;
}
ShowDeptCodeDetail = function (C) {
    var html = ""
    //alert(JSON.stringify(C));

    if (C.length > 0) {
       
        for (var i in C) {
            var isDelete = C[i].IsDelete;
            if (isDelete != 'Y') {
                    html += "<tr class='all_DeptCode' id='trSL_" + C[i].InstSrNo + "'>"
                        + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center; display:none;'>" + (C[i].AccDeptCode) + "</td>"
                        + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center;'>" + (C[i].AccDept) + "</td>"
                        + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center; display:none;'>" + (C[i].AccCostCenterCode) + "</td>"
                        + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center;'>" + (C[i].AccCostCenter) + "</td>"
                        + "<td  style='border:solid 1px black; font-family:Verdana; text-align:right;'>" + (C[i].Amount) + "</td>"
                        + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center; display:none;'>" + C[i].BillID + "</td>"
                    + "<td id='tdSL_" + C[i].InstSrNo + "' style='text-align:center;'><img src='/Content/Images/Edit.png' title='Edit' id='EditSLID_" + C[i].InstSrNo + "' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='EditDeptCodeDetail(this, \"" + C[i].InstSrNo + "\");' /></td>"
                    html + "</tr>";
            }
        }
    }

    $(".tbl_deptCost").find("tr:gt(1)").remove();
    $(".tbl_deptCost").append(html);
}
Call_ModelConfrimation = function()
{
    if (MaintainCostCenter == "Y" || MaintainDeptCode == "Y") {
        $("#lblConfAccDebtMSG").html("Do You Want to Add More Account Department / Cost Center ?");
        $("#dialog-ConfAccDebt").fadeToggle(1000);
        $("#btnConfAccDebtConfNo").focus();
    }
}
ClearDeptCodeText = function () {
    $("#hdnAccDept").val('');
    $("#hdnCostCenter").val('');
    $("#txtAccDept").val('');
    $("#txtCostCenter").val('');
    $("#txtDeptCodeAmt").val('');
    $("#lblInstBillID").val('');
}
EditDeptCodeDetail = function (h, InstSrNo) {
   
    $("#hdnAccDept").val($(h).closest('tr').find('td:eq(0)').text());
    $("#txtAccDept").val($(h).closest('tr').find('td:eq(1)').text());
    $("#hdnCostCenter").val($(h).closest('tr').find('td:eq(2)').text());
    $("#txtCostCenter").val($(h).closest('tr').find('td:eq(3)').text());
    $("#txtDeptCodeAmt").val($(h).closest('tr').find('td:eq(4)').text());
    var ValInstBillID = $(h).closest('tr').find('td:eq(5)').text();
    $("#lblInstBillID").val(ValInstBillID);

    var B = window.voucherInfo.voucherForm;

    //alert(JSON.stringify(B.tmpVchInstType));
    var a = window.voucherInfo.findAndSetDeptCodeDeleteFlag(B.tmpVchInstType, InstSrNo);
    //window.voucherInfo.findAndRemove(B.tmpVchInstType, 'InstSrNo', InstSrNo);
    //alert(JSON.stringify(B.tmpVchInstType));
    ShowDeptCodeDetail(B.tmpVchInstType);
    ModifyflgInst = 'Modify';
    return false;
}
CallCalculateDeptCodeAmount = function () {
   
    ClearDeptCodeText();
    var B = window.voucherInfo.voucherForm;

    var SectorID = B.VoucherMast.SectorID;

    var C = MAIN.Utils.fixArray(B.tmpVchInstType); //alert(JSON.stringify(C));

    var InstAmount = 0;
        for (var i in C) {
            if (C[i].IsDelete != 'Y') {
                InstAmount = (parseFloat(InstAmount) + parseFloat(C[i].Amount)).toFixed(2);
            }
        }
      
   
    //$("#dialog-AccDeptCostCenter").modal("hide");
   

    $("#txtCredit_0").val("");
    $("#txtDebit_0").val("");
    if ($("#ddlDRCR").val() == "D") {
        $("#txtCredit_0").prop("disabled", true);
        //$("#txtDebit_0").prop("disabled", false).val(parseFloat(InstAmount).toFixed(2));
        $("#txtDebit_0").prop("disabled", false).val(InstAmount);
        $("#txtDebit_0").focus();

    } else if ($("#ddlDRCR").val() == "C") {

        $("#txtDebit_0").prop("disabled", true);
        //$("#txtCredit_0").prop("disabled", false).val(parseFloat(InstAmount).toFixed(2));
        $("#txtCredit_0").prop("disabled", false).val(InstAmount);
        $("#txtCredit_0").focus();
    }
};

btn_ConfNo = function () {
    $("#dialog-ConfAccDebt").hide();
    $("#dialog-AccDeptCostCenter").modal("hide");
    CallCalculateDeptCodeAmount();
}
btn_ConfYes = function () {
    $("#dialog-ConfAccDebt").hide();
    $("#txtAccDept").focus();
    $('#txtAccDept').css('background-color', '#DAD8D6');
}
//============================================================ Account Cost Center ==============================================================

//============================================================ NORMAL FUNCTIONS ==============================================================
function isValidDate(dateString) {
    var regEx = /^(0[1-9]|[12][0-9]|3[01])[/ /.](0[1-9]|1[012])[/ /.](19|20)\d\d$/;
    return dateString.match(regEx) != null;
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31
    && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
function SetVoucherDate() {
    $('#txtInstDate').datepicker({
        autoOpen: false,
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',

        onSelect: function (event) {
            $('#txtInstDate').datepicker("hide");
            checkInstDtless90days();
            return false;
        }

    });
    $("#txtInstDate").datepicker("option", "maxDate", $('#txtVoucherDate').val())
}
function leftpad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
function Get_YearMonth()
{
    if($("#txtVoucherDate").val() !="")
    {
        var a = $("#txtVoucherDate").val();
        var t = a.split('/');
        var monthyear = t[2] + t[1];
        return monthyear;
    }
}
function Set_Voucher_Date() {

    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtVoucherDate').val(output);
}
function Check_Array(value, list) {

    if (jQuery.inArray(value, list) != '-1') {
        return true;
    } else {
        return false;
    }
}

function Highlight_Row() {

    $("[id*=tblAccDept] tr").not(':first').hover(
       function () { $(this).css("background", "#D4E6F1"); },
       function () { $(this).css("background", ""); });
}
function Search_AccDept(e) {

    var forSearchprefix = $("#txtSearchAccDept").val().trim().toUpperCase();
    var tablerow = $('#tblAccDept').find('.allAccDept');
    $.each(tablerow, function (index, value) {
        var DeptDesc = $(this).find('.DeptDesc').text().toUpperCase();
        if (DeptDesc.indexOf(forSearchprefix) > -1) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}
function AccountRule()
{
    var V = "{SectorID : '" + $("#ddlSector").val() + "'}";
    $.ajax({
        url: '/Accounts_Form/AccountMaster/AccountRule',
        data: V,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
  async:false,
        success: function (D) {
            var t = D.Data;
            if (t.length > 0 && D.Status == 200) {

                MaintainCostCenter = t[0].MaintainCostCenter; 
                MaintainDeptCode = t[0].MaintainDeptCode;
                  MaintainAccCode = t[0].MaintainAccCode;
               
            }
        }
    });
}
function Maintain_ItemDetail() {
    var VoucherTypeID = $("#ddlVchType").val();

    if (VoucherTypeID == 7 || VoucherTypeID == 8) {
        $(".isSubItem").text('Particulars');
        $(".isSubItemType").css('display', 'none');
        $("#ddlSubLedgerType").val('O');

        $(".m_itemdetail").css('display', '');
    }
    else {
        $(".isSubItem").text('SubLedger');
        $(".m_itemdetail").css('display', 'none');
        $(".isSubItemType").css('display', '');
        $("#txtItemQty").val('');
        $("#txtItemRate").val('');
    }
}
function Cal_Amount() {
    var qty = $('#txtItemQty').val() == "" ? 0 : $('#txtItemQty').val(); 
    var rate = $('#txtItemRate').val() == "" ? 0 : $('#txtItemRate').val();

    var amt = qty * rate;
    $('#txtSubLedAmount').val(amt.toFixed(2));
}
