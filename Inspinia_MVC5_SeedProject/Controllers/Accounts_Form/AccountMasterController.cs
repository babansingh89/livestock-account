﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.App_Codes.BLL;

namespace wbEcsc.Controllers.Accounts
{
    //[SecuredFilter]
    public class AccountMasterController : Controller
    {

        ClientJsonResult cr = new ClientJsonResult();
        Account_BLL acbll= new Account_BLL();

        SessionData sData;
        public AccountMasterController()
        {
            sData = SessionContext.SessionData;
        }
     
        // GET: AccountMaster
        public ActionResult Index()
        {
            return View("~/Views/Accounts/Index.cshtml");
        }

        public ActionResult Subledger()
        {
            return View("~/Views/Accounts/Subledger.cshtml");
        }

        [HttpPost]
        public JsonResult GetAccountHead() {
            try
            {
                int? SectorID = sData.CurrSector;
                var root = acbll.GetAccountHead(SectorID);
                cr.Data = root;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetSubledger() {
            try
            {
                int? SectorID = sData.CurrSector;
                var lst = acbll.getSubledger(SectorID);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetSubledgerAuto(string AccountDescription, string SectorID)
        {
            try
            {
                var lst = acbll.GetSubledgerAuto(AccountDescription, SectorID);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status= ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult AccountHeadAutoComplete(string AccountDescription,string Group)
        {
            try
            {
                var lst = acbll.GetAccountHdeadAuto(AccountDescription, Group);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Load_AccountAutoComplete(string AccountDescription, string AccountORSubLedger)
        {
            try
            {
                var lst = acbll.Load_AccountAutoComplete(AccountDescription, AccountORSubLedger);
                cr.Data = lst;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult SubledgerAutoComplete(string IFSC) {
            try
            {
                var lst = acbll.subledgerAutoComplete(IFSC);
                cr.Data = lst;
                cr.Message = string.Format("{0}", lst);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult getSubledgerFillByBranchID(string id) {
            try
            {
                var x = acbll.getSubledgerByBranchId(id);
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 1);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetState() {
            try
            {
                var x = acbll.getState();
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetDistrict(int Id) {
            try
            {
                var x = acbll.getDistrict(Id);
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetAllBank()
        {
            try
            {
                var x = acbll.getAllBank();
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAllSector() {
            try
            {
                var lst = acbll.getAllSector();
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data="";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAllBranch(int bankID)
        {

            try
            {
                var x = acbll.getAllBranch(bankID);
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAccuntHeadById(string id, string SectorID)
        {
            try
            {
                var x = acbll.getAccountHeadById(id, SectorID);
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 1);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult SaveAccountHead(string AccountID,string parent , string acDesc ,string GLS ,string IELA ,string IsSubLedger, int branchId ,string acno ,string ISBank ,string achead,int? centerid, string schedule,string PL )
        {
            try
            {
                int? SectorID = sData.CurrSector;
                long InsertedBy = sData.UserID;

                acbll.saveAccountHead(AccountID,parent, acDesc, GLS, IELA, IsSubLedger, branchId, acno, ISBank, achead, SectorID, InsertedBy,  schedule,  PL, centerid);
                cr.Data = true;
                if (AccountID != null)
                    cr.Message = "Ledger Updated Successfully !!";
                else
                    cr.Message = "Ledger Inserted Successfully !!";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult UpdateAccountHead(string Id, string Name, string Parent, string ParentId, string acHead, int branchId, string txtacno, string IsSubLedger, string GroupLedger, bool sec, string schedule, string PL)
        {
            try
            {
                int? SectorID = sData.CurrSector;
                long InsertedBy = sData.UserID;
                SectorID = sec ? SectorID : 0;
                acbll.updateAccountHead(Id, Name, Parent, ParentId, acHead, branchId, txtacno, IsSubLedger, GroupLedger, SectorID, InsertedBy, schedule, PL);
                cr.Data = true;
                cr.Message = "SubLedger Updated Successfully !!";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult SaveSubledger(string AccountID,string Parent,string Payee,string GST,string Address, string PAN,string GroupLedger,string IELA, string Mob,string PayeeContactPerson,string branch,int AccountType,int state,int district,string PIN, List<Account_Description> EffectedGL)
        {
            try
            {
                int? SectorID = sData.CurrSector;
                long InsertedBy = sData.UserID;

                string Msg =  acbll.saveSubLedger(AccountID, Parent,Payee,GST, Address, PAN,GroupLedger, IELA, Mob,PayeeContactPerson,branch,AccountType, SectorID, InsertedBy,state,district,PIN, EffectedGL);
                cr.Data = Msg;
                //if (AccountID != null)
                //    cr.Message = "Ledger Updated Successfully !!";
                //else
                //    cr.Message = "Ledger Inserted Successfully !!";

                if (Msg == "1")
                    cr.Message = "Account Ledger Inserted Successfully !!";
                if (Msg == "2")
                    cr.Message = "Account Ledger Updated Successfully !!";
                if (Msg == "3")
                    cr.Message = "Sorry ! You are not Authorised !!";
                if (Msg == "")
                    cr.Message = "Sorry ! There is Some Problem !!";

                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        
        [HttpPost]
        public JsonResult GetAccountHeader()
        {
            try
            {
                DataTable dt = acbll.TreeView();

                var headers = dt.AsEnumerable().Where(r=> (string)r["ParentAccountCode"] == "0").Select(r=> (string)r["AccountCode"]);

                JObject acc = new JObject();

                foreach (string header in headers) {
                    acc.Add(header,(JToken) getNestedChildren(header, ref dt));
                }
                

                cr.Data = acc;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult getAccConfig() {
            try
            {
                int? SectorID = sData.CurrSector;
                var lst = acbll.getAccConfig(SectorID);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        private IDictionary<string, JToken> getNestedChildren(string header, ref DataTable dt) {
            JObject acc = new JObject();

            var enumData = dt.AsEnumerable().Where(r => r.Field<string>("ParentAccountCode") == header).Select(r => new 
            {
                AccountCode = r.Field<string>("AccountCode"),
                AccountDescription = r.Field<string>("AccountDescription"),
                ParentAccountCode = r.Field<string>("ParentAccountCode")
            }).ToList();

           string strArr = JsonConvert.SerializeObject(enumData).ToString();
            if (acc[header] == null)
            {
                acc.Add(header, JArray.Parse(strArr));
            }
                foreach (JObject child in  acc[header]) {

                    var childData = dt.AsEnumerable().Where(r => r.Field<string>("ParentAccountCode") == child["AccountCode"].ToString()).Select(r => new
                    {
                        AccountCode = r.Field<string>("AccountCode"),
                        AccountDescription = r.Field<string>("AccountDescription"),
                        ParentAccountCode = r.Field<string>("ParentAccountCode")

                    }).ToList();

                    if (childData.Count() > 0)
                    {
                        child["AccountCode"] =(JObject) getNestedChildren(child["AccountCode"].ToString(), ref dt);

                    }

                }
           // }

            return acc;
        }


        [HttpPost]
        public JsonResult test(dynamic obj)
        {
            //IEnumerable<dynamic> obj
            return Json(obj);
        }

        [HttpPost]
        public JsonResult test1(string obj)
        {
            //IEnumerable<dynamic> obj
            return Json(obj);
        }



        [HttpPost]
        public JsonResult AccountRule(string SectorID)
        {
            try
            {
                DataTable dt = acbll.Get_AccountRule(SectorID);
                cr.Data = dt;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Account_Description(string Desc, string MND)
        {
            try
            {
                List<Account_MD> dt = acbll.Account_Description(Desc, MND);
                cr.Data = dt;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult DeleteAccountCode(string AccCode, string SectorID)
        {
            try
            {
                string result = acbll.DeleteAccountCode(AccCode, SectorID);
                cr.Data = result;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
		
		        [HttpPost]
        public JsonResult FinYear(string SectorID)
        {
            try
            {
                if (SectorID != "")
                {
                    sData.CurrSector = Convert.ToInt32(SectorID);
                    var lst = new EmployeeMaster().Get_SectorWiseFinYear(Convert.ToInt32(SectorID));
                    cr.Data = lst;
                }
                else
                {
                    sData.CurrSector = null;
                    cr.Data = "";
                }
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
    }
}