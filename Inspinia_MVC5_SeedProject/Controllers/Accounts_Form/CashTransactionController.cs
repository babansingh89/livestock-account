﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels.Accounts;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class CashTransactionController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;

        public CashTransactionController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            VoucherInfo voucherInfo = new VoucherInfo();
            VoucherMaster vchMast = new VoucherMaster();
            List<VoucherDetail> vchDet = new List<VoucherDetail>();
            List<VoucherInstType> vchInstType = new List<VoucherInstType>();
            List<VoucherSubLedgerType> vchSL = new List<VoucherSubLedgerType>();


            voucherInfo.SessionExpired = "N";
            voucherInfo.EntryType = "I";
            vchMast.SectorID = 0;
            voucherInfo.VoucherMast = vchMast;
            voucherInfo.VoucherDetail = vchDet;
            voucherInfo.tmpVchInstType = vchInstType;
            voucherInfo.tmpVchSubLedger = vchSL;


            ViewBag.PopulateJSON = JsonConvert.SerializeObject(voucherInfo);

            return View("~/Views/Accounts/CashTransaction.cshtml");
        }

        [HttpPost]
        public ActionResult SubLedger(string TransType, string Desc, string VoucherTypeID, string ParentAccCode, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Get_CashTransaction", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@VoucherTypeID", VoucherTypeID);
            cmd.Parameters.AddWithValue("@AccountCode", ParentAccCode);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveVoucher(VoucherInfo VoucherForm)
        {
            try
            {
                var result = new VoucherMaster_BLL().SaveVoucher(VoucherForm, sData.UserID, sData.CurrSector);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Search_VoucherNo(string VoucherNo, string VoucherTypeID, string VoucherDate, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Get_CashTransaction", con);
            cmd.Parameters.AddWithValue("@TransactionType", "VoucherNo");
            cmd.Parameters.AddWithValue("@VoucherNo", VoucherNo);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@VoucherTypeID", VoucherTypeID);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetDetbitCreditVendor(string SectorID, string VoucherTypeID, string VchDate)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Get_CashTransaction", con);
            cmd.Parameters.AddWithValue("@TransactionType", "DebitCredit");
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@VoucherTypeID", VoucherTypeID);
            cmd.Parameters.AddWithValue("@VchDate", VchDate);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_VoucherDetails(string VoucherTypeID, string VoucherDate, string VoucherNo, string AccFinYear, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Get_CashVoucherSearchInfo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@VCH_NO", VoucherNo);
            cmd.Parameters.AddWithValue("@VCH_TYPE", VoucherTypeID);
            cmd.Parameters.AddWithValue("@YrMonth", AccFinYear);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@UserID", sData.UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            VoucherInfo voucherInfo = new VoucherInfo();

            try
            {
                con.Open();
                da.Fill(ds);
                if (ds.Tables.Count > 0)
                {
                    VoucherMaster vchMast = new VoucherMaster();
                    VoucherDetail vchDetail = new VoucherDetail();
                    List<VoucherDetail> vchDet = new List<VoucherDetail>();
                    List<VoucherInstType> vchInstType = new List<VoucherInstType>();
                    List<VoucherSubLedgerType> vchslType = new List<VoucherSubLedgerType>();

                    DataTable dtMast = ds.Tables[0];
                    DataTable dtDetail = ds.Tables[1];
                    DataTable dtInst = ds.Tables[2];
                    DataTable dtSl = ds.Tables[2];

                    if (dtMast.Rows.Count > 0)
                    {
                        vchMast.YearMonth = dtMast.Rows[0]["FinYear"].ToString();
                        vchMast.VCHDATE = dtMast.Rows[0]["VCH_DATE"].ToString();
                        vchMast.VCHNo = Convert.ToInt32(dtMast.Rows[0]["VoucherNumber"].ToString());
                        vchMast.VoucherType = Convert.ToInt32(dtMast.Rows[0]["VoucherTypeID"].ToString());
                        vchMast.STATUS = dtMast.Rows[0]["STATUS"].ToString();
                        vchMast.VoucherNo = dtMast.Rows[0]["RefVoucherSlNo"].ToString();
                        vchMast.SectorID = Convert.ToInt32(dtMast.Rows[0]["SectorID"].ToString());
                        vchMast.NARRATION = dtMast.Rows[0]["NARRATION"].ToString();
                        vchMast.VCHAMOUNT = dtMast.Rows[0]["VCH_AMOUNT"].ToString();


                        foreach (DataRow dr in dtDetail.Rows)
                        {
                            VoucherDetail vd = new VoucherDetail();
                            vd.YearMonth = dr["FinYear"].ToString();
                            vd.VoucherDetailID = Convert.ToInt32(dr["VoucherDetailID"].ToString());
                            vd.VoucherType = Convert.ToInt32(dr["VoucherTypeID"].ToString());
                            vd.VCHNo = Convert.ToInt32(dr["VoucherNumber"].ToString());
                            vd.VchSrl = Convert.ToInt32(dr["VCH_SRL"].ToString());
                            vd.OLDSLID = dr["OldAccountHead"].ToString();
                            vd.GLName = (dr["GL_NAME"].ToString());
                            vd.GLID = (dr["GL_ID"].ToString());
                            vd.SLID = "";
                            vd.SubID = "";
                            vd.OLDSUBID = "";
                            vd.SUBDesc = "";
                            vd.DRCR = (dr["DRCR"].ToString());
                            vd.GlType = (dr["GL_TYPE"].ToString());
                            vd.AMOUNT = Convert.ToDouble(dr["AMOUNT"].ToString());
                            vd.SectorID = Convert.ToInt32(dr["SectorID"].ToString());
                            //vd.AccCostCenterCode = (dr["CostCenterCode"].ToString());
                            //vd.AccDeptCode = (dr["DeptCode"].ToString());
                            //vd.AccDept = (dr["DeptDesc"].ToString());
                            //vd.AccCostCenter = (dr["CostCenterDesc"].ToString());
                            //vchDet.Add(vd);

                            //DataRow[] rows = dtInst.Select("GL_ID='" + dr["GL_ID"].ToString() + "' and SubCodeType='" + "I" + "'");
                            DataRow[] rows = dtInst.Select("GL_ID='" + dr["GL_ID"].ToString() + "' and VoucherDetailID='" + dr["VoucherDetailID"].ToString() + "' and SubCodeType='" + "I" + "'");
                            List<VoucherInstType> vchTmpInstType = new List<VoucherInstType>();
                            int j = 0;
                            foreach (DataRow row in rows)
                            {
                                VoucherInstType vit = new VoucherInstType();
                                vit.InstSrNo = j + 1;
                                vit.YearMonth = row["FinYear"].ToString();
                                vit.BillID = Convert.ToInt32(row["BillID"].ToString());
                                vit.VoucherType = Convert.ToInt32(row["VoucherTypeID"].ToString());
                                vit.VCHNo = Convert.ToInt32(row["VoucherNumber"].ToString());
                                vit.GLID = (row["GL_ID"].ToString());
                                vit.SLID = "";
                                vit.DRCR = (row["DRCR"].ToString());
                                vit.SlipNo = "";
                                vit.InstType = (row["InstrumentType"].ToString());
                                vit.InstTypeName = (row["InstTypeName"].ToString());
                                vit.InstNo = (row["InstrumentNumber"].ToString());
                                vit.InstDT = (row["INST_DT"].ToString());
                                vit.Amount = Convert.ToDouble(row["AdjustmentAmount"].ToString());
                                vit.ActualAmount = Convert.ToDouble(row["AdjustmentAmount"].ToString());
                                vit.Drawee = (row["DRAWEE"].ToString());
                                vit.Cleared = (row["CLEARED"].ToString());
                                vit.ClearedOn = (row["CLEARED_ON"].ToString());
                                vit.SectorID = Convert.ToInt32(row["SectorID"].ToString());
                                vit.AccCostCenterCode = (row["CostCenterCode"].ToString());
                                vit.AccDeptCode = (row["DeptCode"].ToString());
                                vit.AccDept = (row["DeptDesc"].ToString());
                                vit.AccCostCenter = (row["CostCenterDesc"].ToString());
                                vchTmpInstType.Add(vit);
                            }

                            //DataRow[] slrow = dtInst.Select("GL_ID='" + dr["GL_ID"].ToString() + "' and SubCodeType='" + "S" + "'");
                            DataRow[] slrow = dtInst.Select("GL_ID='" + dr["GL_ID"].ToString() + "' and VoucherDetailID='" + dr["VoucherDetailID"].ToString() + "' and SubCodeType='" + "S" + "'");
                            List<VoucherSubLedgerType> vchTmpSlType = new List<VoucherSubLedgerType>();
                            int k = 0;
                            foreach (DataRow row in slrow)
                            {
                                VoucherSubLedgerType sl = new VoucherSubLedgerType();
                                sl.SLSlNo = k + 1;
                                sl.BillID = Convert.ToInt32(row["BillID"].ToString());
                                sl.AccountCODE = row["GL_ID"].ToString();
                                sl.SubLedgerCODE = row["SubCode"].ToString();
                                sl.SubLedger = row["SubLedger"].ToString();
                                sl.SubLedgerTypeID = row["SubLedgerTypeID"].ToString();
                                sl.SubLedgerType = row["SubLedgerType"].ToString();
                                sl.Number = row["AdjustmentNumber"].ToString();
                                sl.Amount = row["AdjustmentAmount"].ToString();
                                sl.DRCR = row["DRCR"].ToString();
                                //sl.AccCostCenterCode = (row["CostCenterCode"].ToString());
                                //sl.AccDeptCode = (row["DeptCode"].ToString());
                                //sl.AccDept = (row["DeptDesc"].ToString());
                                //sl.AccCostCenter = (row["CostCenterDesc"].ToString());
                                vchTmpSlType.Add(sl);
                            }

                            vd.VchInstType = vchTmpInstType;
                            vd.VchSubLedger = vchTmpSlType;


                            vchDet.Add(vd);
                        }

                    }
                    voucherInfo.SessionExpired = "N";
                    voucherInfo.EntryType = "E";

                    voucherInfo.VoucherMast = vchMast;
                    voucherInfo.VoucherDetail = vchDet;
                    voucherInfo.tmpVchInstType = vchInstType;
                    voucherInfo.tmpVchSubLedger = vchslType;
                }

                cr.Data = voucherInfo;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Search_AccountDetails(string SearchValue, string SearchType, string VoucherTypeID, string SectorID, string DRCR, string TransType)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Get_Acc_VchGLSL", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Type", SearchType);
            cmd.Parameters.AddWithValue("@Description", SearchValue);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@DRCR", DRCR);
            cmd.Parameters.AddWithValue("@VoucherTypeID", VoucherTypeID);
            cmd.Parameters.AddWithValue("@TransType", TransType);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }
    }
}