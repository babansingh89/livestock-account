﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.ViewModels.Accounts;

namespace wbEcsc.Controllers.Accounts_Form
{
    //[SecuredFilter]
    public class Bank_Payment_UpdationController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public Bank_Payment_UpdationController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/Bank_Payment_Updation.cshtml");
        }

        [HttpPost]
        public JsonResult Show_Detail()
        {
            try
            {
                DataTable dt = new Bank_Payment_BLL().Show_Detail();
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Load_Instrument()
        {
            try
            {
                List<VoucherMaster_VM> dt = new VoucherMaster_BLL().Get_Instrument();
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAuto(string AccountDescription, string SectorID)
        {
            try
            {
                DataTable dt = new Bank_Payment_BLL().GetAuto(AccountDescription, SectorID);
                cr.Data = dt;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save(string BillID, string VoucherNumber, string VoucherDetailID, string AccountCode, string InstTypeID, string InstNo, string InstDate, string PaymentDate)
        {
            try
            {
                string result = new Bank_Payment_BLL().Save(BillID, VoucherNumber, VoucherDetailID, AccountCode, InstTypeID, InstNo, InstDate, PaymentDate, sData.UserID);
                cr.Data = result;
                cr.Message = string.Format("{0} Found", result);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
    }
}