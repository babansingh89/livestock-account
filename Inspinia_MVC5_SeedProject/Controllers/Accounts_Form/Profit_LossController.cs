﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models.Account;
using System.Data;

namespace wbEcsc.Controllers.Accounts_Form
{
    //[SecuredFilter]
    public class Profit_LossController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public Profit_LossController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
           ViewBag.AccFinYear=  sData.CurFinYear;
            return View("~/Views/Accounts/Profit_Loss.cshtml");
        }

        [HttpPost]
        public ActionResult Show_Details(string FromDate, string ToDate, string SectorID, string OnlyTrans)
        {
            try
            {
                DataSet  lstTrail = new Profit_Loss_BLL().Show_Details(FromDate, ToDate, SectorID, OnlyTrans);
                cr.Data = lstTrail;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "found");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Show_Group(string AccCode, string FromDate, string ToDate, string SectorID, string OnlyTrans)
        {
            try
            {
                DataSet lstTrail = new Profit_Loss_BLL().Show_Group(AccCode, FromDate, ToDate, SectorID, OnlyTrans);
                cr.Data = lstTrail;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "found");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

    }
}