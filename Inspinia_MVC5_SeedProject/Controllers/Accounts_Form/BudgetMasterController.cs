﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class BudgetMasterController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;


        public BudgetMasterController()
        {

                sData = SessionContext.SessionData;
            
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/BudgetMaster.cshtml");
        }

        [HttpPost]
        public ActionResult Get_Particulars(string TrasactionType, string BudgetTypeID, string CategoryID, string SubCategoryID, string SectorID, string Desc)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BudgetMaster", con);
            cmd.Parameters.AddWithValue("@TransactionType", TrasactionType);
            cmd.Parameters.AddWithValue("@BudgetType", BudgetTypeID);
            cmd.Parameters.AddWithValue("@BudgetCat", CategoryID);
            cmd.Parameters.AddWithValue("@BudgetSubID", SubCategoryID);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                var lst = dt.AsEnumerable().Select(t => new
                {
                    BudgetID = t.Field<int>("BudgetID"),
                    BudgetDescription = t.Field<string>("BudgetDescription"),
                    BudgetCode = t.Field<string>("BudgetCode"),
                    BudgetAmount = t.Field<decimal>("BudgetAmount"),
                }).ToList();


                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_ConParticulars(string TrasactionType, string BudgetTypeID, string CategoryID, string SubCategoryID, string SectorID, string Desc)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BudgetMaster", con);
            cmd.Parameters.AddWithValue("@TransactionType", TrasactionType);
            cmd.Parameters.AddWithValue("@BudgetType", BudgetTypeID);
            cmd.Parameters.AddWithValue("@BudgetCat", CategoryID);
            cmd.Parameters.AddWithValue("@BudgetSubID", SubCategoryID);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                var lst = dt.AsEnumerable().Select(t => new
                {
                    BudgetID = t.Field<int>("BudgetID"),
                    BudgetDescription = t.Field<string>("BudgetDescription"),
                    BudgetCode = t.Field<string>("BudgetCode"),
                    BudgetAmount = t.Field<decimal>("BudgetAmount"),
                    ConcurranceAmount = t.Field<decimal>("ConcurranceAmount"),
                    BalanceAmount = t.Field<decimal>("BalanceAmount"),
                }).ToList();


                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_BudgetDetails(string TrasactionType, string BudgetID, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BudgetMaster", con);
            cmd.Parameters.AddWithValue("@TransactionType", TrasactionType);
            cmd.Parameters.AddWithValue("@BudgetID", BudgetID);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InsertUpdate(BudgetMaster_MD Master)
        {
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            string Result = "";
            try
            {
                try
                {
                    DataTable Detail = new DataTable();
                    Detail.Columns.Add("BId", typeof(int));
                    Detail.Columns.Add("BAmt", typeof(decimal));
                    Detail.Columns.Add("EffDate", typeof(string));
                    Detail.Columns.Add("Remarks", typeof(string));
                    int k = 0;
                    if (Master.Budgetdetail != null)
                    {
                        foreach (var MD in Master.Budgetdetail)
                        {
                            DataRow drr = Detail.NewRow();
                            drr["BId"] = MD.BudgetID;
                            drr["BAmt"] = MD.BudgetAmount;
                            drr["EffDate"] = MD.EffectiveDate;
                            drr["Remarks"] = MD.Remarks;
                            Detail.Rows.Add(drr);
                        }
                    }
                    BudgetMaster_MD obj = new BudgetMaster_MD();
                    SqlConnection con = new SqlConnection(conString);
                    cmd = new SqlCommand("account.BudgetMaster", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransactionType", Master.BudgetID == null ? "Insert" : "Update");
                    cmd.Parameters.AddWithValue("@BudgetID", Master.BudgetID);
                    cmd.Parameters.AddWithValue("@BudgetCode", Master.BudgetCode);
                    cmd.Parameters.AddWithValue("@BudgetAmount", Master.BudgetAmount);
                    cmd.Parameters.AddWithValue("@SectorID", Master.SectorID);
                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                    cmd.Parameters.AddWithValue("@BudgetDetail", Detail);
                    
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Result = dr["Result"].ToString();
                        }
                    }
                    dr.Close();
                    transaction.Commit();
                    cr.Data = Result;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0}", Result);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult InsertUpdate_Concurrance(string BudgetID, string SectorID, List<ConcurranceDetail_MD> Master)
        {
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            string Result = "";
            try
            {
                try
                {
                    DataTable Detail = new DataTable();
                    Detail.Columns.Add("BudgetID", typeof(int));
                    Detail.Columns.Add("ConcurranceSlNo", typeof(int));
                    Detail.Columns.Add("ConcurranceAmount", typeof(decimal));
                    Detail.Columns.Add("ConcurranceDate", typeof(string));
                    Detail.Columns.Add("ReferanceNo", typeof(string));
                    Detail.Columns.Add("Remarks", typeof(string));
                    int k = 0;
                    if (Master !=null)
                    {
                        foreach (var MD in Master)
                        {
                            k = k + 1;
                            DataRow drr = Detail.NewRow();
                            drr["BudgetID"] = MD.BudgetID;
                            drr["ConcurranceSlNo"] = k;
                            drr["ConcurranceAmount"] = MD.ConcurranceAmount;
                            drr["ConcurranceDate"] = MD.ConcurranceDate;
                            drr["ReferanceNo"] = MD.ReferanceNo;
                            drr["Remarks"] = MD.Remarks;
                            Detail.Rows.Add(drr);
                        }
                    }
                    BudgetMaster_MD obj = new BudgetMaster_MD();
                    SqlConnection con = new SqlConnection(conString);
                    cmd = new SqlCommand("account.BudgetMaster", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransactionType", "ConInsert");
                    cmd.Parameters.AddWithValue("@BudgetID", BudgetID);
                    cmd.Parameters.AddWithValue("@SectorID", SectorID);
                    cmd.Parameters.AddWithValue("@ConBudgetDetail", Detail);

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Result = dr["Result"].ToString();
                        }
                    }
                    dr.Close();
                    transaction.Commit();
                    cr.Data = Result;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0}", Result);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Get_Dashboard(string TrasactionType, string BudgetTypeID, string CategoryID, string SubCategoryID, string SectorID, string Desc)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BudgetMaster", con);
            cmd.Parameters.AddWithValue("@TransactionType", TrasactionType);
            cmd.Parameters.AddWithValue("@BudgetType", BudgetTypeID);
            cmd.Parameters.AddWithValue("@BudgetCat", CategoryID);
            cmd.Parameters.AddWithValue("@BudgetSubID", SubCategoryID);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }
    }
}