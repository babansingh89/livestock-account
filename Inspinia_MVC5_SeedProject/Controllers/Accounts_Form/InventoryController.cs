﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace wbEcsc.Controllers.Accounts_Form
{
    //[SecuredFilter]
    public class InventoryController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        Inventory_BLL InvBll = new Inventory_BLL();
        SessionData sData;
        public InventoryController()
        {
            sData = SessionContext.SessionData;
        }
        // GET: Inventory
        public ActionResult Inventory()
        {
            return View("~/Views/Accounts/Inventory.cshtml");
        }

        [HttpPost]
        public JsonResult AccontHeadAuto(string AccountDescription, string SectorID) {
            try
            {
                var lst = InvBll.GetAuto(AccountDescription, SectorID);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult InsUpdInventorydetails(InventoryDetails InvDetails) {
            try
            {
                InvDetails.SectorID = sData.CurrSector;
                InvDetails.InsertedBy = sData.UserID;

                InvBll.InsUpdInventorydetails(InvDetails);

                cr.Data = true;
                if (InvDetails.Id != 0)
                    cr.Message = "Inventory Updated Successfully !!";
                else
                    cr.Message = "Inventory Inserted Successfully !!";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception)
            {

                throw;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAllInventoryDetails(string stockdate) {
            try
            {
                var lst = InvBll.GetAllInventoryDetails(stockdate);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult DeleteInvDetailsById(int Id)
        {
            try
            {
                //InvDetails.SectorID = sData.CurrSector;
                //InvDetails.InsertedBy = sData.UserID;
                InvBll.DeleteInvDetailsById(Id);
                cr.Data = true;
                cr.Message = "Inventory Deleted Successfully !!";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception)
            {
                throw;
            }
            return Json(cr);
        }
    }
}