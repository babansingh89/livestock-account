﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.App_Start;
using wbEcsc.Models;
using System.Data;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes;

namespace wbEcsc.Controllers.Accounts
{
    //[SecuredFilter]
    public class AccountsVoucherOpeningController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        AccountVoucherOpening aco = new AccountVoucherOpening();

        SessionData sData;
        public AccountsVoucherOpeningController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/AccountOpeningBalance.cshtml");
        }

        [HttpPost]
        public JsonResult GET_AccountsDetails(string FinYear,string VoucherType, string SectorID)
        {
            try
            {
                var lst=aco.getAccountDetails(FinYear, VoucherType, SectorID);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Search_AutoComplete(string AutoText, string SearchType, string VoucherType, string FinYear, string SectorID)
        {
            try
            {
                var lst = aco.getAccountDetailsbyCodeName(AutoText, SearchType, VoucherType, FinYear, SectorID);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Update_Details(string AccountCode,string DRCR,decimal Amount,string FinYear,string SectorID) {
            try
            {
                aco.update_Details(AccountCode,DRCR,Amount,FinYear,SectorID, sData.UserID);
                cr.Data = "";
                cr.Message = "Success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Auto_SubLedger_Details(string AccountCode, string Desc, string FinYear, string SectorID)
        {
            try
            {
                DataTable dt= aco.Auto_Ledger_Details(AccountCode, Desc, FinYear, SectorID);
                cr.Data = dt;
                cr.Message = "Success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
    }
}