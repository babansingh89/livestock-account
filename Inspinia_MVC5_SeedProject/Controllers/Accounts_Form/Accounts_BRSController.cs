﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace wbEcsc.Controllers.Accounts_Form
{
    public class Accounts_BRSController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public Accounts_BRSController()
        {
            sData = SessionContext.SessionData;
        }

        public ActionResult Index()
        {
            return View("~/Views/Accounts/Accounts_BRS.cshtml");
        }

        [HttpPost]
        public JsonResult Auto_Bank(string Desc, string SectorID, string Type)
        {
            try
            {
                List<AccountBRS> result = new AccountBRS_BLL().Auto_Bank(Desc, SectorID, Type);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", result.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Show_Detail(string AccountCode, string CorresOption, string InstRecIss, string FromDate, string ToDate, string InstrumentNo, string SectorID)
        {
            try
            {
                DataTable dt = new AccountBRS_BLL().Show_Detail(AccountCode, CorresOption, InstRecIss, FromDate, ToDate, InstrumentNo, SectorID);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save(List<AccountBRS> Master)
        {
            try
            {
                string result = new AccountBRS_BLL().Save(Master, sData.UserID);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}