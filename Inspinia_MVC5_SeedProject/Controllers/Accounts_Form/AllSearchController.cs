﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using System.Configuration;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class AllSearchController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        public ActionResult Index()
        {
            return View("~/Views/Accounts/AllSearch.cshtml");
        }

        public ActionResult Load_ItemDetail(string From, string To, string AmountFrom, string AmountTo, string AccountCode, string DRCR, string VchType, string AdjType,
                                            string ChequeNo, string Narration, string SectorID, string IncludeUnpaidTransac)
        {
            List<AllSearch_MD> lst = new List<AllSearch_MD>();
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Load_AllSearch", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@FromDate", From == "" ? DBNull.Value : (object)From);
            cmd.Parameters.AddWithValue("@ToDate", To == "" ? DBNull.Value : (object)To);
            cmd.Parameters.AddWithValue("@AmountFrom", AmountFrom == "" ? 0 : (object)AmountFrom);
            cmd.Parameters.AddWithValue("@AmountTo", AmountTo == "" ? 0 : (object)AmountTo);
            cmd.Parameters.AddWithValue("@AccountCode", AccountCode == "" ? DBNull.Value : (object)AccountCode);
            cmd.Parameters.AddWithValue("@DRCR", DRCR == "" ? DBNull.Value : (object)DRCR);
            cmd.Parameters.AddWithValue("@VCH_TYPE", VchType == "" ? DBNull.Value : (object)VchType);
            cmd.Parameters.AddWithValue("@Adj_TYPE", AdjType == "" ? DBNull.Value : (object)AdjType);
            cmd.Parameters.AddWithValue("@ChequeNo", ChequeNo == "" ? DBNull.Value : (object)ChequeNo);
            cmd.Parameters.AddWithValue("@Narration", Narration == "" ? DBNull.Value : (object)Narration);
            cmd.Parameters.AddWithValue("@SectorID", SectorID == "" ? DBNull.Value : (object)SectorID);
            cmd.Parameters.AddWithValue("@IncludeUnpaidTransac", IncludeUnpaidTransac == "" ? DBNull.Value : (object)IncludeUnpaidTransac);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                lst = CommonMethod.ConvertToList<AllSearch_MD>(dt);

                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }
        #region
        public static class CommonMethod
        {
            public static List<T> ConvertToList<T>(DataTable dt)
            {
                var columnNames = dt.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();
                var properties = typeof(T).GetProperties();
                return dt.AsEnumerable().Select(row => {
                    var objT = Activator.CreateInstance<T>();
                    foreach (var pro in properties)
                    {
                        if (columnNames.Contains(pro.Name.ToLower()))
                        {
                            try
                            {
                                pro.SetValue(objT, row[pro.Name]);
                            }
                            catch (Exception ex) { }
                        }
                    }
                    return objT;
                }).ToList();
            }
        }
        #endregion
    }
}