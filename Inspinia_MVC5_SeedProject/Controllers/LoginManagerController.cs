﻿using System;
using System.Data.SqlClient;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.Authenticator;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Account.Logins;
using wbEcsc.Models.Application;
using wbEcsc.Utils.EncryptionDecrytion;

namespace wbEcsc.Controllers
{
    [SecuredFilter]
    public class LoginManagerController : Controller
    {
        ClientJsonResult cj = new ClientJsonResult();
        IEncryptDecrypt chiperAlgorithm;
        AdministrativeUser_BLL bll = new AdministrativeUser_BLL();
        SessionData sData;
        public LoginManagerController()
        {
            sData = SessionContext.SessionData;
        }

        public ActionResult AdministrativeLogin(AdministrativeLogin LoginParam)
        {
            var obj = TempData["LogInObj"] as AdministrativeLogin;
            TempData["LogInObj"] = null;

            if (obj == null && LoginParam.UserName == null && LoginParam.Password == null && obj.UserName == null && obj.Password == null)
            {
                return new RedirectResult("/LoginManager/LogOut");
            }
            if (obj != null)
            {
                LoginParam = obj as AdministrativeLogin;
            }
            if (LoginParam.UserName == null && LoginParam.Password == null)
            {
                return new RedirectResult("/LoginManager/LogOut");
            }

            #region
            Action<AdministrativeLogin> Validate = (_loginParam) =>
            {
                throw new NotImplementedException();
            };
            #endregion
            try
            {
                AdministrativeAuthenticator act = new AdministrativeAuthenticator(LoginParam);
                AdministrativeUser user = act.Authenticate();
                SessionContext.Login(user);

                Session["session_UserName"] = LoginParam.UserName;
                Session["session_Password"] = LoginParam.Password;

                //return new RedirectResult("/Administration/AdminHome/HomeUI");

                cj.Data = new Models.AppResource.InOut()
                {
                    User = user,
                    LogoutUrl = "/Home/Logout",
                    LoginUIUrl = "/Home/AdministrativeLoginUI",
                    LoginUrl = "/LoginManager/AdministrativeLogin",
                    LandingPageUrl = "/Administration/AdminHome/HomeUI",
                    RedirectTo = "/Administration/AdminHome/HomeUI",
                    IsAuthenticated = SessionContext.IsAuthenticated
                };
                cj.Status = ResponseStatus.SUCCESS;
                cj.Message = "Successfully Logged In";
            }
            catch (SqlException ex)
            {
                cj.Message = "Database Error! Please try again later";
                cj.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            catch (SecurityException ex)
            {
                cj.Message = ex.Message;
                cj.Status = ResponseStatus.UNAUTHORIZED;
            }
            catch (FormatException ex)
            {
                return new RedirectResult("/LoginManager/LogOut");
            }
            catch (ArgumentException ex)
            {
                return new RedirectResult("/LoginManager/LogOut");
            }
            return Json(cj);
        }

        public ActionResult LogOut()
        {
            try
            {
                App_Codes.SessionContext.Logout();
                Session.Clear();
                cj.Data = new Models.AppResource.InOut()
                {
                    //LoginUIUrl= "/Home/LoginMenuUI",
                    //RedirectTo = "/Home/LoginMenuUI",
                    LoginUIUrl = "/Home/AdministrativeLoginUI",
                    RedirectTo = "/Home/AdministrativeLoginUI",
                    IsAuthenticated = SessionContext.IsAuthenticated
                };
                cj.Message = "Logged Out";
                cj.Status = ResponseStatus.SUCCESS;
            }
            catch (ArgumentException ex)
            {
                cj.Data = null;
                cj.Message = ex.Message;
                cj.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cj);
        }

        public ActionResult RedirectfromOtherDomain(string Data)
        {
            if (Data != null && Data != "")
            {
                chiperAlgorithm = new AES_Algorithm("EIIPL@201^");
                string decryptedURL = chiperAlgorithm.Decrypt(Data);
                string urlDecodedData = HttpUtility.UrlDecode(decryptedURL);
                JavaScriptSerializer js = new JavaScriptSerializer();
                var serializedData = js.Deserialize<AdministrativeLogin>(urlDecodedData);

                TempData["LogInObj"] = serializedData;
                return Redirect("/LoginManager/AdministrativeLogin");
                //return RedirectToAction("AdministrativeLogin", "LoginManager");
            }
            else
            {
                return Redirect("/LoginManager/AdministrativeLogin");
            }
        }

        public ActionResult SetSession(string SectorID)
        {
            sData.CurrSector = Convert.ToInt32(SectorID);
            return Json(cj);
        }

    }
}