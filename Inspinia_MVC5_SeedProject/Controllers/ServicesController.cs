﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using wbEcsc.Models;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using Inspinia_MVC5_SeedProject.Models;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using System.Web;
using System.Text.RegularExpressions;

namespace Inspinia_MVC5_SeedProject.Controllers
{
    [RoutePrefix("api/account")]
    public class ServicesController : ApiController
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlCommand cmd = new SqlCommand();
        ClientJsonResult cr = new ClientJsonResult();

        [HttpGet]
        [Route("AccountDesc")]
        public HttpResponseMessage AccountDesc()
        {
            HttpResponseMessage response = null;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Service", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", "AccountDesc");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            response = Request.CreateResponse(System.Net.HttpStatusCode.OK, cr, "application/json");
            return response;
        }

        [HttpPost]
        [Route("AccountPosting")]
        public HttpResponseMessage AccountPosting(object param)
        {
            HttpResponseMessage response = null;
            SqlConnection dbConn = new SqlConnection(conString);
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();

            try
            {
                if (param != null)
                {
                    //string ss = JsonConvert.SerializeObject(param, Formatting.Indented);
                    var data = new JavaScriptSerializer();
                    Data<List<Services_MD>> Subdetail = data.Deserialize<Data<List<Services_MD>>>(param.ToString());

                    DataTable DetailIncome = new DataTable();
                    DataTable DetailExpense = new DataTable();
                    DataTable Detail = new DataTable();

                    DetailIncome.Columns.Add("WayBillID", typeof(int));
                    DetailIncome.Columns.Add("WayBillNo", typeof(string));
                    DetailIncome.Columns.Add("WayBillDate", typeof(string));
                    DetailIncome.Columns.Add("AccountGroup", typeof(string));
                    DetailIncome.Columns.Add("AccountCode", typeof(string));
                    //DetailIncome.Columns.Add("AccountDescription", typeof(string));
                    DetailIncome.Columns.Add("DRCR", typeof(string));
                    DetailIncome.Columns.Add("VoucherTypeID", typeof(int));
                    DetailIncome.Columns.Add("TicketFare", typeof(decimal));
                    DetailIncome.Columns.Add("SectorID", typeof(int));
                    DetailIncome.Columns.Add("CenterId", typeof(int));
                    DetailIncome.Columns.Add("InsertedBy", typeof(int));

                    DetailExpense.Columns.Add("WayBillID", typeof(int));
                    DetailExpense.Columns.Add("WayBillNo", typeof(string));
                    DetailExpense.Columns.Add("WayBillDate", typeof(string));
                    DetailExpense.Columns.Add("AccountGroup", typeof(string));
                    DetailExpense.Columns.Add("AccountCode", typeof(string));
                    //DetailExpense.Columns.Add("AccountDescription", typeof(string));
                    DetailExpense.Columns.Add("DRCR", typeof(string));
                    DetailExpense.Columns.Add("VoucherTypeID", typeof(int));
                    DetailExpense.Columns.Add("TicketFare", typeof(decimal));
                    DetailExpense.Columns.Add("SectorID", typeof(int));
                    DetailExpense.Columns.Add("CenterId", typeof(int));
                    DetailExpense.Columns.Add("InsertedBy", typeof(int));

                    decimal TotalIncome = 0; decimal TotalExpense = 0;
                    for (int k = 0; k < Subdetail.detail.Count; k++)
                    {
                        string AccountGroup = Subdetail.detail[k].AccountGroup;
                        if (AccountGroup == "I")
                        {
                            DataRow dr = DetailIncome.NewRow();
                            dr["WayBillID"] = Subdetail.detail[k].WayBillID;
                            dr["WayBillNo"] = Subdetail.detail[k].WayBillNo;
                            dr["WayBillDate"] = Subdetail.detail[k].WayBillDate;
                            dr["AccountCode"] = Regex.Replace(Subdetail.detail[k].AccountCode, @"\s", ""); //(Subdetail.detail[k].AccountCode).Replace(" ", "");
                            dr["AccountGroup"] = (Subdetail.detail[k].AccountGroup).Replace(" ", "");
                            //dr["AccountDescription"] = Subdetail.detail[k].AccountDescription;
                            dr["DRCR"] = (Subdetail.detail[k].DRCR).Replace(" ", "");
                            dr["VoucherTypeID"] = 3;
                            dr["TicketFare"] = Subdetail.detail[k].TicketFare;
                            dr["SectorID"] = Subdetail.detail[k].SectorID;
                            dr["CenterId"] = Subdetail.detail[k].CenterId;
                            dr["InsertedBy"] = Subdetail.detail[k].InsertedBy;
                            TotalIncome = TotalIncome + Subdetail.detail[k].TicketFare;
                            DetailIncome.Rows.Add(dr);
                        }
                        if (AccountGroup == "E")
                        {
                            DataRow dr = DetailExpense.NewRow();
                            dr["WayBillID"] = Subdetail.detail[k].WayBillID;
                            dr["WayBillNo"] = Subdetail.detail[k].WayBillNo;
                            dr["WayBillDate"] = Subdetail.detail[k].WayBillDate;
                            dr["AccountCode"] = Regex.Replace(Subdetail.detail[k].AccountCode, @"\s", ""); //(Subdetail.detail[k].AccountCode).Replace(" ", "");
                            dr["AccountGroup"] = (Subdetail.detail[k].AccountGroup).Replace(" ", "");
                            //dr["AccountDescription"] = Subdetail.detail[k].AccountDescription;
                            dr["DRCR"] = (Subdetail.detail[k].DRCR).Replace(" ", "");
                            dr["VoucherTypeID"] = 4;
                            dr["TicketFare"] = Subdetail.detail[k].TicketFare;
                            dr["SectorID"] = Subdetail.detail[k].SectorID;
                            dr["CenterId"] = Subdetail.detail[k].CenterId;
                            dr["InsertedBy"] = Subdetail.detail[k].InsertedBy;
                            TotalExpense = TotalExpense + Subdetail.detail[k].TicketFare;
                            DetailExpense.Rows.Add(dr);
                        }
                    }

                    int IncomeVchNo = 0; int ExpenseVchNo = 0;
                    if (DetailIncome.Rows.Count > 0)
                    {
                        IncomeVchNo = new Services_BLL().SaveVoucher(DetailIncome, dbConn, transaction, TotalIncome);
                    }
                    if (DetailExpense.Rows.Count > 0)
                    {
                        ExpenseVchNo = new Services_BLL().SaveVoucher(DetailExpense, dbConn, transaction, TotalExpense);
                    }

                    if (IncomeVchNo > 0 && ExpenseVchNo > 0)
                    {
                        transaction.Commit();
                    }

                    cr.Data = "success";
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0}", "Account Posting done Successfully !!");
                }
                else
                {
                    cr.Data = "fail";
                    cr.Status = ResponseStatus.INVALID_DATA;
                    cr.Message = string.Format("{0}", "There is some Problem !!");
                }
            }
            catch (SqlException sqlError)
            {
                transaction.Rollback();
                cr.Data = "fail";
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = sqlError.Message;
            }
            finally
            {
                dbConn.Close();
            }
            response = Request.CreateResponse(System.Net.HttpStatusCode.OK, cr, "application/json");
            return response;
        }
    }


}
