﻿using System;
using System.Configuration;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions;
using CrystalDecisions.Shared;
using System.Web.Services;
using wbEcsc.App_Codes;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using Newtonsoft.Json;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace wbEcsc.Views.OpenReport
{
    public partial class OpenReport : System.Web.UI.Page
    {
        ExportFormatType formatType = ExportFormatType.NoFormat;
        CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        Dictionary<string, string> Params;
        IInternalUser user;
      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!SessionContext.IsAuthenticated)
            {
                Response.Redirect("/Error/Error401/Index", true);
                return;
            }

            user = SessionContext.CurrentUser as IInternalUser;

            var keys = Request.QueryString["ReportName"];
            if (keys != null)
            {
                JObject obj = JObject.Parse(keys);
                var master = (JArray)obj.SelectToken("Master");
                var detal = (JArray)obj.SelectToken("Detail");

                GenerateReport(master, detal);
            }
        }
        private void GenerateReport(JArray master, JArray detal)
        {
            string reportName = master[0]["ReportName"].ToString();
            string FileName = master[0]["FileName"].ToString();

            crystalReport.Load(Server.MapPath("~/Reports/" + reportName));
            crystalReport.Refresh();

            string server = Connection_Details.ServerName();
            string database = Connection_Details.DatabaseName();
            string userid = Connection_Details.UserID();
            string password = Connection_Details.Password();

            crystalReport.DataSourceConnections[0].SetConnection(server, database, userid, password);
            crystalReport.DataSourceConnections[0].IntegratedSecurity = false;
            crystalReport.SetDatabaseLogon(server, database, userid, password);

            for (int i = 0; i < crystalReport.Subreports.Count; i++)
            {
                crystalReport.Subreports[i].DataSourceConnections[0].SetConnection(server, database, userid, password);
                crystalReport.Subreports[i].DataSourceConnections[0].IntegratedSecurity = false;
                crystalReport.Subreports[i].SetDatabaseLogon(server, database, userid, password);
            }
            crystalReport.VerifyDatabase();

            foreach (var item in detal)
            {
                string a = item.ToString();
                string noNewLines = a.Replace("\n", "");
                Params = JsonConvert.DeserializeObject<Dictionary<string, string>>(noNewLines);
            }

            foreach (KeyValuePair<string, string> entry in Params)
            {
                crystalReport.SetParameterValue("@" + entry.Key, entry.Value);
            }

            formatType = ExportFormatType.PortableDocFormat;
            crystalReport.ExportToHttpResponse(formatType, Response, false, FileName);
        }
    }
}