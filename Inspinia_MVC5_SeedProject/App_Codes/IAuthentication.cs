﻿namespace wbEcsc.App_Codes
{
    public interface IAuthentication<T>
    {
       T Authenticate();
    }
}
