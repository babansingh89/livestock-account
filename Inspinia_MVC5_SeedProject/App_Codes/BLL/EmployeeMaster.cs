﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using System.IO;

namespace wbEcsc.App_Codes.BLL
{
    public class EmployeeMaster : BllBase
    {
        public List<Sector> Get_Sector(long UserID)
        {
            SqlCommand cmd = new SqlCommand("account.Get_Sector", cn);

            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Converts(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
		 public List<FinYear> Get_SectorWiseFinYear(int? SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.Get_SectorWiseFinYear", cn);

            cmd.Parameters.AddWithValue("@SectorID", SectorID == null ? DBNull.Value : (object)SectorID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Converts_FinYear(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
		
        #region Converter
        List<Sector> Converts(DataTable dt)
        {
            List<Sector> lst = new List<Sector>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Sector();
                obj.SectorID = t.Field<int?>("SectorID");
                obj.SectorName = t.Field<string>("SectorName");
                return obj;
            }).ToList();
            return lst;
        }
		
		        List<FinYear> Converts_FinYear(DataTable dt)
        {
            List<FinYear> lst = new List<FinYear>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new FinYear();
                obj.SectorID = t.Field<int?>("SectorID");
                obj.FinYr = t.Field<string>("FinYr");
                obj.FinYrStatus = t.Field<string>("FinYrStatus");
                return obj;
            }).ToList();
            return lst;
        }
        #endregion
    }
}






















