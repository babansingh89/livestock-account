﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for VoucherInfo
/// </summary>
public class VoucherInfo
{
    public VoucherMaster VoucherMast { get; set; }
    public List<VoucherDetail> VoucherDetail { get; set; }
    public List<VoucherInstType> tmpVchInstType { get; set; }
    public List<VoucherSubLedgerType> tmpVchSubLedger { get; set; }


    public string SessionExpired { get; set; }
    public string EntryType { get; set; }
    public string SearchResult { get; set; }
    public string Message { get; set; }


    public VoucherInfo()
    {
        VoucherDetail = new List<VoucherDetail>();
        tmpVchInstType = new List<VoucherInstType>();
        tmpVchSubLedger = new List<VoucherSubLedgerType>();
        //
        // TODO: Add constructor logic here
        //
    }
}