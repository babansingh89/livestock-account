﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class VoucherApproval_BLL: BllBase
    {
        public object getAllVoucherType()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.Get_Acc_VchType";
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return convertbank(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        object convertbank(DataTable dt)
        {
            //List<AssmtScoreSheet> lst = new List<AssmtScoreSheet>();
            var lst = dt.AsEnumerable().Select(t => new {
                VoucherTypeID = t.Field<int>("VoucherTypeID"),
                VoucherDescription = t.Field<string>("VoucherDescription")
            }).ToList();
            return lst;
        }

        public object getAllVoucherApproval(string VCH_NO, int VCH_TYPE,string YrMonth,string SectorID,long UserID, string fromdate, string toDate) {
            SqlCommand cmd = new SqlCommand("account.Get_VoucherApprovalSearchInfo", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@AccountDescription", (object)AccountDescription ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@VCH_NO", VCH_NO);
            cmd.Parameters.AddWithValue("@VCH_TYPE", VCH_TYPE);
            cmd.Parameters.AddWithValue("@YrMonth", YrMonth);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@fromdate", fromdate);
            cmd.Parameters.AddWithValue("@toDate", toDate);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            List<string> result = new List<string>();
            try
            {
                cn.Open();
                da.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new
                {
                    VoucherNumber = t.Field<int>("VoucherNumber"),
                    FinYear = t.Field<string>("FinYear"),
                    RefVoucherSlNo = t.Field<string>("RefVoucherSlNo"),
                    VCH_DATE = t.Field<string>("VCH_DATE"),
                    VoucherTypeID = t.Field<int>("VoucherTypeID"),
                    STATUS = t.Field<string>("STATUS"),
                    SectorID = t.Field<int>("SectorID"),
                    NARRATION=t.Field<string>("NARRATION"),
                    VCH_AMOUNT=t.Field<decimal>("VCH_AMOUNT")
                }).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public object getAllVoucherApprovalDetails(string VCH_NO,  string YrMonth, int? SectorID, long UserID)
        {
            SqlCommand cmd = new SqlCommand("account.Get_VoucherApprovalDetails", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@VCH_NO", VCH_NO);
            cmd.Parameters.AddWithValue("@YrMonth", YrMonth);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
           
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            List<string> result = new List<string>();
            try
            {
                cn.Open();
                da.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new
                {
                    VoucherNumber = t.Field<int>("VoucherNumber"),
                    VoucherSerial = t.Field<int>("VoucherSerial"),
                    AccountCode = t.Field<string>("AccountCode"),
                    AccountDescription = t.Field<string>("AccountDescription"),
                    DRCR = t.Field<string>("DRCR"),
                    Amount = t.Field<decimal>("Amount")
                }).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public void updateVoucherApproval(string VCH_NO, int? SecID,long InsertedBy)
        {
            SqlTransaction Transaction = null;
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "account.updateVoucherApproval";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@VCH_NO", !string.IsNullOrEmpty(VCH_NO) ? VCH_NO : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@SectorID", SecID);
                cmd.Parameters.AddWithValue("@UserID", InsertedBy);
                cmd.Transaction = Transaction;
                cmd.ExecuteNonQuery();
                Transaction.Commit();
            }
            catch (Exception)
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

          

    }
}