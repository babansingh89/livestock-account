﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Account;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class AccountMappingBLL: BllBase
    {
        public object getAccountMapType()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.GetAccountMapType";
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return convertbank(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        object convertbank(DataTable dt)
        {
            //List<AssmtScoreSheet> lst = new List<AssmtScoreSheet>();
            var lst = dt.AsEnumerable().Select(t => new {
                MId = t.Field<int>("MId"),
                Mdesc = t.Field<string>("Mdesc")
            }).ToList();
            return lst;
        }

        public object getComponentByTypeId(int MapTypeId)
        {
            SqlCommand cmd = new SqlCommand("account.Get_ComponentTypeByMapTypeId", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MapTypeId", MapTypeId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            List<string> result = new List<string>();
            try
            {
                cn.Open();
                da.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new
                {
                    MId = t.Field<int>("MId"),
                    MCompId = t.Field<int>("MCompId"),
                    MCompDesc = t.Field<string>("MCompDesc"),
                    Sectorid = t.Field<int>("Sectorid"),
                    AccountCode = t.Field<string>("AccountCode"),
                    AccountDescription = t.Field<string>("AccountDescription"),
                    OldAccountHead = t.Field<string>("OldAccountHead"),
                    MaintainAccCode=t.Field<string>("MaintainAccCode"),
                    DRCR = t.Field<string>("DRCR"),
                    VoucherTypeID=t.Field<int?>("VoucherTypeID")
                }).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public object GetAuto(string AccountDescription, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.AccountHeaderAuto", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AccountDescription", (object)AccountDescription ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var lst = dt.AsEnumerable().Where(r => r.Field<string>("GroupLedger") == "L").Select(t => new
                {
                    AccountDescription = t.Field<string>("AccountDescription"),
                    AccountCode = t.Field<string>("AccountCode"),
                }).Take(10).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public void InsertAccountMap(List<AccountMapvalue> AccountMapvalues, int? SectorID, long InsertedBy)
        {
            SqlTransaction Transaction = null;
            cn.Open();
            Transaction = cn.BeginTransaction();

            try
            {
                foreach (var item in AccountMapvalues)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Transaction = Transaction;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "account.Insert_AccountMapping";
                    cmd.Parameters.AddWithValue("@Mid", item.Mid);
                    cmd.Parameters.AddWithValue("@MCompId", item.MCompId);
                    cmd.Parameters.AddWithValue("@VoucherTypeID", item.VoucherTypeID);
                    cmd.Parameters.AddWithValue("@acCode", item.acCode);
                    cmd.Parameters.AddWithValue("@drcr", item.drcr);
                    cmd.Parameters.AddWithValue("@SectorID", SectorID);
                    cmd.Parameters.AddWithValue("@InsertedBy", InsertedBy);
                    cmd.ExecuteNonQuery();
                }
                Transaction.Commit();
            }
            catch (Exception)
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
    }


}

   