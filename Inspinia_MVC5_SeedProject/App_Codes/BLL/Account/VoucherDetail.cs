﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for VoucherDetail
/// </summary>
public class VoucherDetail
{
    public int? VoucherDetailID { get; set; }
    public string YearMonth { get; set; }
    public int? VoucherType { get; set; }
    public int? VCHNo { get; set; }
    public int? VchSrl { get; set; }
    public string OLDSLID { get; set; }
    public string GLName { get; set; }
    public string GLID { get; set; }
    public string SLID { get; set; }
    public string SubID { get; set; }
    public string OLDSUBID { get; set; }
    public string SUBDesc { get; set; }
    public string DRCR { get; set; }
    public string GlType { get; set; }
    public double? AMOUNT { get; set; }
    public int? SectorID { get; set; }
    public string IsDelete { get; set; }

    public string AccDeptCode { get; set; }
    public string AccCostCenterCode { get; set; }
    public string AccDept { get; set; }
    public string AccCostCenter { get; set; }
    public decimal? CurrBal { get; set; }


    public List<VoucherInstType> VchInstType { get; set; }
    public List<VoucherSubLedgerType> VchSubLedger { get; set; }

    public VoucherDetail()
    {
        VchInstType = new List<VoucherInstType>();
        VchSubLedger = new List<VoucherSubLedgerType>();
        //
        // TODO: Add constructor logic here
        //
    }
}