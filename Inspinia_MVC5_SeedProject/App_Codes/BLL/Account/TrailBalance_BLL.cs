﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class TrailBalance_BLL:BllBase
    {
        public DataTable Show_Details(string FromDate, string ToDate, string SectorID, string GLSL, string ParentAccountCode, string ischkzero, string ischkonly)
        {
            SqlCommand cmd = new SqlCommand("account.PROC_ACC_TRIAL_BALANCE", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_DT", FromDate);
            cmd.Parameters.AddWithValue("@p_DT1", ToDate);
            cmd.Parameters.AddWithValue("@p_SECTORIDS", SectorID);
            cmd.Parameters.AddWithValue("@GL_SL", GLSL);
            cmd.Parameters.AddWithValue("@PAAccountCode", ParentAccountCode);
            cmd.Parameters.AddWithValue("@ischkzero", ischkzero);
            cmd.Parameters.AddWithValue("@OnlyTrans", ischkonly);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);
                // return Convert_Show_Details(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Show_Exp_Main_1(string AccountCode)
        {
            SqlCommand cmd = new SqlCommand("account.Get_SubAccountDescription", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PAccountCode", AccountCode);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);
                // return Convert_Show_Details(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Show_Exp_Main_2(string AccountCode, string StartDate, string EndDate, string SectorID, string IS_GL)
        {
            SqlCommand cmd = new SqlCommand("account.Acc_Month_Wise_Dtls", cn);  //Get_SubAccountDescriptionByMonth
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Start_dt", StartDate);
            cmd.Parameters.AddWithValue("@End_dt", EndDate);
            cmd.Parameters.AddWithValue("@PAccountCode", AccountCode);
            cmd.Parameters.AddWithValue("@Sectorids", SectorID);
            cmd.Parameters.AddWithValue("@IS_GL", IS_GL);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                cmd.CommandTimeout = 0;

                da.Fill(dt);
                // return Convert_Show_Details(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Show_Exp_Main_3(string AccountCode, string VoucherStartDate, string VoucherEndDate, string IS_GL, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.Get_SubAccountDescriptionForVoucher", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PVoucherCode", AccountCode);
            cmd.Parameters.AddWithValue("@StartDate", VoucherStartDate);
            cmd.Parameters.AddWithValue("@EndDate", VoucherEndDate);
            cmd.Parameters.AddWithValue("@IS_GL", IS_GL);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);
                // return Convert_Show_Details(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Show_Exp(string FinYear, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.Get_Accounts_Expendetures", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FinYear", FinYear);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);
                // return Convert_Show_Details(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Sector> Get_UserWiseSector(long? UserID)
        {
            SqlCommand cmd = new SqlCommand("account.Get_UserPermission", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", "UserSector");
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);
                return Converts(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        #region Converter
        List<TrailBalance_MD> Convert_Show_Details(DataTable dt)
        {
            List<TrailBalance_MD> lst = new List<TrailBalance_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new TrailBalance_MD();
                obj.AccountCode = t.Field<string>("AccountCode");
                obj.OldAccountHead = t.Field<string>("OldAccountHead");
                obj.AccountDescription = t.Field<string>("AccountDescription");
                obj.OpeningBalDR = t.Field<decimal?>("OpeningBalDR");
                obj.OpeningBalCR = t.Field<decimal?>("OpeningBalCR");
                obj.TransactionBalDR = t.Field<decimal?>("TransactionBalDR");
                obj.TransactionBalCR = t.Field<decimal?>("TransactionBalCR");
                obj.ClosingBalDR = t.Field<decimal?>("ClosingBalDR");
                obj.ClosingBalCR = t.Field<decimal?>("ClosingBalCR");
                return obj;
            }).ToList();
            return lst;
        }

        List<Sector> Converts(DataTable dt)
        {
            List<Sector> lst = new List<Sector>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Sector();
                obj.SectorID = t.Field<int?>("SectorID");
                obj.SectorName = t.Field<string>("SectorName");
                return obj;
            }).ToList();
            return lst;
        }

        #endregion

    }
}