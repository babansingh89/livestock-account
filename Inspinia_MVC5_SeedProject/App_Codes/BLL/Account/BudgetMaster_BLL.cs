﻿using wbEcsc.Models.Account;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.App_Codes.BLL;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class BudgetMaster_BLL: BllBase
    {
          public List<BudgetMaster_MD> Get_FundType()
        {
            SqlCommand cmd = new SqlCommand("account.BudgetMaster", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", "FundType");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_Get_FundType(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        #region Converter
        List<BudgetMaster_MD> Convert_Get_FundType(DataTable dt)
        {
            List<BudgetMaster_MD> lst = new List<BudgetMaster_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new BudgetMaster_MD();
                obj.FundTypeID = t.Field<int>("FundTypeID");
                obj.FundDescription = t.Field<string>("FundDescription");
                return obj;
            }).ToList();
            return lst;
        }
        #endregion
    }
}