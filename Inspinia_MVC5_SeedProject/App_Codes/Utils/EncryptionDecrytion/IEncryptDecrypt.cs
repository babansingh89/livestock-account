﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wbEcsc.Utils.EncryptionDecrytion
{
    interface IEncryptDecrypt
    {
        string Encrypt(string text);
        string Decrypt(string cipherText);
    }
}
