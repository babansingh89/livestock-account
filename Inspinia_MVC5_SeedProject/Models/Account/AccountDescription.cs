﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class Account_Description
    {
        public Account_Description()
        {
            Children = new List<Account_Description>();
            nodes = new List<Account_Description>();
        }
        public string AccountCode { get; set; }
        public string AccountDescription { get; set; }
        public string text { get; set; }
        public string ParentAccountCode { get; set; }
        public List<Account_Description> Children { get; set; }
        public List<Account_Description> nodes { get; set; }
        public string GroupLedger { get; set; }
        public string SubLedger { get; set; }
    }
}