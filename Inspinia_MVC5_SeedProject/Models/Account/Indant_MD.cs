﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class Indant_MD
    {
        public int? IndantID { get; set; }
        public string IndantNo { get; set; }
        public string IndantDate { get; set; }
        public string Remark { get; set; }
        public int? SectionID { get; set; }

        public List<IndantDetail_MD> Itemdetail { get; set; }
        public Indant_MD()
        {
            Itemdetail = new List<IndantDetail_MD>();
        }
    }

    public class IndantDetail_MD
    {
        public int? IndantDetailID { get; set; }
        public int? IndantID { get; set; }
        public int? IndantSlNo { get; set; }
        public string ItemID { get; set; }
        public int? Qty { get; set; }
    }
}