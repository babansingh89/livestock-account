﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class InventoryDetails
    {
        public int Id { get; set; }
        public string InvDate { get; set; }
        public string AccountDescription { get; set; }
        public string AccountCode { get; set; }
        public decimal Amount { get; set; }
        public int? SectorID { get; set; }
        public long InsertedBy { get; set; }

    }
}