﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class AccountMapvalue
    {
        public int Mid { get; set; }
        public int MCompId { get; set; }
        public int? VoucherTypeID { get; set; }
        public string acCode { get; set; }
        public string drcr { get; set; }
    }
}