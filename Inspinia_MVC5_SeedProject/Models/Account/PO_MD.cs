﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class PO_MD
    {
        public long? PoId { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string VendEmail { get; set; }
        public string IndantID { get; set; }
        public string PoStatus { get; set; }
        public string Address { get; set; }
        public string ShipTo { get; set; }
        public string ShipToName { get; set; }
        public string ShipAddr { get; set; }
        public string ShipVia { get; set; }
        public string PoNo { get; set; }
        public string PoDate { get; set; }
        public decimal? CreditTerms { get; set; }
        public string TaxType { get; set; }
        public decimal? GrossAmt { get; set; }
        public string AdjType { get; set; }
        public decimal? AdjAmt { get; set; }
        public string BillAdjHead { get; set; }
        public decimal? RoundOff { get; set; }
        public decimal? NetAmt { get; set; }
        public string Remarks { get; set; }
        public string DocFilePath { get; set; }
        public int? Sectorid { get; set; }
        public List<PODetail_MD> PODetail { get; set; }
        public List<POSub_MD> POSubDetail { get; set; }
        public PO_MD()
        {
            PODetail = new List<PODetail_MD>();
            POSubDetail = new List<POSub_MD>();
        }
    }
    public class PODetail_MD
    {
        public long? PoId { get; set; }
        public int? DatPoId { get; set; }
        public string ItemId { get; set; }
        public string ItemDescription { get; set; }
        public string ProductServiseDesc { get; set; }
        public decimal? Qty { get; set; }
        public decimal? Rate { get; set; }
        public decimal? GrossAmt { get; set; }
        public decimal? Discount { get; set; }
        public int? TaxTypeId { get; set; }
        public string TaxTypeDesc { get; set; }
        public decimal? TaxAmt { get; set; }
        public decimal? NetAmt { get; set; }
        public int? sectorid { get; set; }
    }
    public class POSub_MD
    {
        public long? PoId { get; set; }
        public int? DatPoId { get; set; }
        public int? TaxID { get; set; }
        public int? taxTypeDetID { get; set; }
        public string TaxDes { get; set; }
        public decimal? TaxNetAmount { get; set; }
        public decimal? GrossAmt { get; set; }
    }

    //BILL
    public class Term_MD
    {
        public int? TermID { get; set; }
        public string TermDesc { get; set; }
    }
    //BILL
    public class BillAdjHead_MD
    {
        public string AccountCode { get; set; }
        public string AccountDescription { get; set; }
        public string TransType { get; set; }
    }
}