﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wbEcsc.Models.Account;

namespace wbEcsc.Models.Account
{
    public class AccountBRS : Account_Description
    {
        public string BillID { get; set; }
        public string Cleared { get; set; }
        public string ClearedOn { get; set; }
        public string VchAmount { get; set; }
        public string InFavour { get; set; }

    }
}