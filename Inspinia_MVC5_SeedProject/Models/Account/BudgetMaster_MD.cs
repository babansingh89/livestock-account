﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class BudgetMaster_MD
    {
        public int? BudgetID { get; set; }
        public string BudgetCode { get; set; }
        public decimal? BudgetAmount { get; set; }
        public int? FundTypeID { get; set; }
        public int? SectorID { get; set; }
        public string FundDescription { get; set; }

        public List<BudgetDetail_MD> Budgetdetail { get; set; }
        public BudgetMaster_MD()
        {
            Budgetdetail = new List<BudgetDetail_MD>();
        }
    }

    public class BudgetDetail_MD
    {
        public int? BudgetID { get; set; }
        public int? BudgetSlNo { get; set; }
        public decimal? BudgetAmount { get; set; }
        public string EffectiveDate { get; set; }
        public int? FundTypeID { get; set; }
        public string Remarks { get; set; }
    }
    public class ConcurranceDetail_MD
    {
        public int? BudgetID { get; set; }
        public int? ConcurranceSlNo { get; set; }
        public decimal? ConcurranceAmount { get; set; }
        public string ConcurranceDate { get; set; }
        public string ReferanceNo { get; set; }
        public string Remarks { get; set; }
    }
}