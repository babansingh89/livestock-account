﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.Models.Application
{
    public  class SessionData
    {
        public List<Sector> Sectors { get; set; }
        public int? CurrSector { get; set; }
		 public string CurrSectorName { get; set; }

        public string EmpNoLength { get; set; }

        public long UserID { get; set; }
        public string UserName { get; set; }

        public int UserType { get; set; }

        public string CurFinYear { get; set; }
        public string CurSalFinYear { get; set; }
        public string SalMonth { get; set; }
        public int SalMonthID { get; set; }
        public string Permission { get; set; }

        public SessionData()
        {
            Sectors = new List<Application.Sector>();
        }
    }
}