﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
   
    public class Bank_MD
    {
        public int? BankID { get; set; }
        
        [Required(ErrorMessage ="Please Enter a bank Name")]
        [Display(Name ="Enter Bank Name")]
        [MaxLength(200,ErrorMessage ="Name is too large")]
        public string BankName { get; set; }
    }
}