﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class AllSearch_MD
    {
        public int? VoucherNumber { get; set; }
        public string VchDate { get; set; }
        public string Narration { get; set; }
        public string VchType { get; set; }
        public string VchNo { get; set; }
        public decimal? Debit { get; set; }
        public decimal? Credit { get; set; }
    }
}