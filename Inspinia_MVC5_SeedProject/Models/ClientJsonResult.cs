﻿namespace wbEcsc.Models
{
    public class ClientJsonResult
    {
        public object Data { get; set; }
        public ResponseStatus Status { get; set; }
        public string Message { get; set; }
    }
}