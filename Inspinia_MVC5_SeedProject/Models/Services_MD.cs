﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inspinia_MVC5_SeedProject.Models
{
    public class Services_MD
    {
        public string AccountCode { get; set; }
        public string DRCR { get; set; }
        public string AccountDescription { get; set; }
        public int? WayBillID { get; set; }
        public string WayBillNo { get; set; }
        public string WayBillDate { get; set; }
        public string AccountGroup { get; set; }
        public decimal TicketFare { get; set; }
        public int VoucherTypeID { get; set; }
        public int SectorID { get; set; }
        public int CenterId { get; set; }
        public int InsertedBy { get; set; }
    }
    public class Data<T>
    {
        public T detail { get; set; }
        public Data()
        {

        }

    }
}