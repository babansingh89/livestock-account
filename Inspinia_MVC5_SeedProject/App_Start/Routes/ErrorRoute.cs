﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace wbEcsc.App_Start.Routes
{
    public class ErrorRoute:IRoute
    {
        private RouteCollection _routes { get; set; }
        public ErrorRoute(RouteCollection routes)
        {
            this._routes = routes;
        }
        public void MapRoute()
        {
            _routes.MapRoute(
                name: "ErrorRouter",
                url: "Error/{controller}/{action}"
                //,defaults: new { controller = "Collection", action = "Index", id = UrlParameter.Optional }
                , namespaces: new[] { "wbEcsc.Controllers.Error" }
            );
        }
    }
}